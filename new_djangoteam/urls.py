"""new_djangoteam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib import admin
from django.views.decorators.gzip import gzip_page
from django.views.generic import TemplateView
from apps import contact_us
from apps.accounts import views
from django.contrib.auth import views as auth_views
from apps.faq.views import PermissionView
from apps.site_admin.models import AboutUs
from apps.site_admin.views import AboutUsView
from home import views
from apps.contact_us import views as contact

from django.views.defaults import server_error, page_not_found,permission_denied

page_not_found = 'apps.freelancers.views.handler404'
server_error = 'apps.freelancers.views.handler500'



urlpatterns = [

    url(r'^site_administration/', include(admin.site.urls)),
    url(r'^$', gzip_page(views.IndexView.as_view()), name='IndexView'),
    # url(r'^register',views.RegisterView.as_view(), name='Register'),
    url(r'^accounts/', include('apps.accounts.urls')),
    url(r'^', include('apps.clients.urls')),
    url(r'^blogs/', include('apps.blogs.urls')),
    url(r'^faq/', include('apps.faq.urls')),
    url(r'^profile/', include('apps.profile.urls')),
    url(r'^', include('apps.freelancer.urls')),
    url(r'^about/$',AboutUsView.as_view(),name='about'),
    url(r'^contact/$',contact.ContactView.as_view(), name='contact_us'),
    url(r'^admin/mail/', include('apps.sendbox.urls')),
    url(r'^site_admin/', include('apps.site_admin.urls')),
    url(r'^permission_error/$', PermissionView.as_view(), name='permission_error'),
    url(r'^hire/', include('apps.hire.urls')),


    url(r'^password/reset/$',
        auth_views.password_reset,
        {'post_reset_redirect': 'auth_password_reset_done',
         'email_template_name': 'registration/password_reset_email.html'},
        name='password_reset'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/'
        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {'post_reset_redirect': 'auth_password_reset_complete'},
        name='password_reset_confirm'),
    url(r'^password/reset/complete/$',
        auth_views.password_reset_complete,
        name='auth_password_reset_complete'),
    url(r'^password/reset/done/$',
        auth_views.password_reset_done,
        name='auth_password_reset_done'),

        #####CKEditor #####
    url(r'^ckeditor/', include('ckeditor.urls')),


]


if 'django_comments' in settings.INSTALLED_APPS:
    # Django 1.7/1.8 situation
    COMMENT_URLS = 'django_comments.urls'


urlpatterns += patterns('',
                        url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                            {'document_root': settings.STATIC_ROOT, 'show_indexes': False}),
                        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                            {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
                        url(r'^comments/', include(COMMENT_URLS)),
                        )
urlpatterns += staticfiles_urlpatterns()
