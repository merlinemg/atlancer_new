from django.db import models
from solo.models import SingletonModel


class SiteConfiguration(SingletonModel):
    site_name = models.CharField(max_length=255, default='Site Name')
    domain = models.CharField(max_length=255)
    admin_email = models.EmailField()

    def __unicode__(self):
        return u"Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"


class CommissionRate(SingletonModel):
    """
    Commission rate model...
    """
    freelancer_commission_rate = models.DecimalField(default=10.00, max_digits=5, decimal_places=2,
                                                     help_text="Please enter the commission"
                                                                            " rate in percentage(%)")
    client_commission_rate = models.DecimalField(default=20.00, max_digits=5, decimal_places=2,
                                                              help_text="Please enter the commission"
                                                                        " rate in percentage(%)")

    def __unicode__(self):
        return u"Commission rate settings"

    class Meta:
        verbose_name = "Commission rate settings"


