import logging

from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template import Context
from django.template.loader import get_template

from cities_light.models import Country, Region, City

from apps.general.models import SiteConfiguration
from apps.knowledgebase.models import Skills, Language
from apps.freelancer.models import HourlyRate, WorkCategories, WorkSubCategories, HourlyRate, ExperienceLevel,EmploymentHistory

from apps.profile.models import FreelancerProfile, ClientProfile

logger = logging.getLogger(__name__)


def send_email(template_name=None, from_email=None, to_email=[], context=None, subject=''):
        """
        Sending admin emails.
        :param bind_form:
        :param recievers:
        :return:
        """
        if not from_email:
                from_email = settings.DEFAULT_FROM_EMAIL
        if not subject:
                subject = 'Shortlist notifications'
        config = SiteConfiguration.objects.get()
        site = config.domain
        message = ""
        admin_email = config.admin_email
        if not to_email:
                to_email = admin_email
        template = get_template(template_name)
        context.update({'site': site})
        context = Context(context)
        content = template.render(context)
        msg = EmailMessage(subject, content, from_email, to=to_email,)
        msg.content_subtype = "html"
        msg.send()


def stripe_payment_required(a_view):
        def _wrapped_view(request, *args, **kwargs):
                return redirect('stripe')

        return _wrapped_view


class UtilMixin(object):

    def get_authenticated_user(self, user):
        """
        Get related cities...
        :return:cities
        """
        try:
            try:
                profile = FreelancerProfile.objects.get(user=user)
            except:
                profile = ClientProfile.objects.get(user=user)
        except:
            profile = None
        return profile

    def get_logger(self, email_obj, frm, to):
        """
        Get email_obj.
        :return:email_obj
        """
        try:
            email_obj.send()
        except:
            logger.debug("Email sending from " + frm + " to " + str(to) + " failed")
        return email_obj

    def get_all_employee_history(self):
        """
        getting all employee history...
        :return:employee history
        """
        emp_history = EmploymentHistory.objects.all()
        return emp_history

    def get_all_hourly_rate(self):
        """
        getting all houly rate...
        :return:
        """
        rate = HourlyRate.objects.all()
        return rate

    def get_my_hourly_rate_instance(self):
        """
        getting houly rate...
        :return:
        """
        try:
            hourly_rate = HourlyRate.objects.get(user=self.request.user)
        except HourlyRate.DoesNotExist:
            hourly_rate = None
        return hourly_rate

    def get_all_freelancers(self):
        """
        Get freelancer...
        :return: freelancer
        """
        all_freelancers = FreelancerProfile.objects.all()
        return all_freelancers

    def get_all_skills(self):
        """
        Get skills...
        :return: skills
        """
        skills = Skills.objects.all()
        return skills

    def get_languages(self):
        """
        Get languages...
        :return: languages
        """
        languages = Language.objects.all()
        return languages

    def get_work_categories(self):
        """
        Get main_category...
        :return: main_category
        """
        main_category = WorkCategories.objects.all()
        return main_category

    def get_all_work_sub_categories(self):
        """
        Get sub_category...
        :return: sub_category
        """
        sub_category = WorkSubCategories.objects.all()
        return sub_category

    def get_countries(self):
        """
        Get countries...
        :return: countries
        """
        countries = Country.objects.all()
        return countries

    def get_regions(self):
        """
        Get regions...
        :return: regions
        """
        regions = Region.objects.all()
        return regions

    def get_cities(self):
        """
        Get cities...
        :return: cities
        """
        cities = City.objects.all()
        return cities

    def get_region_by_name(self, region):
        """

        :param region: name of region
        :return: region
        """
        try:
            region = Region.objects.get(name=region)
        except:
            region = None
        return region

    def get_country_by_name(self, country):
        """

        :param country: name of region
        :return: country
        """
        try:
            country = Country.objects.get(name=country)
        except:
            country = None
        return country

    def get_city_by_name(self, city):
        """

        :param city: name of city
        :return: city
        """
        try:
            city = City.objects.get(name=city)
        except:
            city = None
        return city

    def get_skills_by_skill_name_list(self, skills_name_list):
        """

        :param city: skills_list
        :return: skills
        """
        skills = Skills.objects.filter(skill_name__in=skills_name_list)
        return skills

    def get_skills_by_skill_id_list(self, skills_id):
        """

        :param city: skills_list
        :return: skills
        """
        skills = Skills.objects.filter(id__in=skills_id)
        return skills

    def get_language_by_language_title_list(self, language_title_list):
        """

        :param language_title_list: language_title_list
        :return: languages
        """
        languages = Language.objects.filter(language_title__in=language_title_list)
        return languages

    def has_freelancer_profile(self):
        try:
            self.request.user.freelancer_profile
            status = True
        except:
            status = False
        return status

    def active_profile(self):
        """

        :return:
        """
        return self.request.user.get_active_account()

    def update_active_profile(self, account_type):
        """
        Update active account for a user...
        :return:
        """
        self.request.user.active_account = account_type
        self.request.user.save()










