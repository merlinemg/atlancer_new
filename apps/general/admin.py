from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import SiteConfiguration, CommissionRate

admin.site.register(SiteConfiguration, SingletonModelAdmin)
admin.site.register(CommissionRate, SingletonModelAdmin)