from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from django.db import models
from django.contrib.auth.models import (
     AbstractUser
)
from django.conf import settings


class User(AbstractUser):
    """
    Custom user model
    """

    CLIENT = 'CL'
    FREELANCER = 'FLR'
    Site_admin = 'SITE'

    ACCOUNT_TYPES = (
        (CLIENT, 'cl'),
        (FREELANCER, 'flr'),
        (Site_admin, 's_admin'),
    )

    country = models.CharField(_('Country'), max_length=255)
    active_account = models.CharField(max_length=4,
                                      choices=ACCOUNT_TYPES,
                                      default=FREELANCER, verbose_name="Current active account")
    register_account = models.CharField(max_length=4,
                                      choices=ACCOUNT_TYPES, verbose_name="Initial login account")



    REQUIRED_FIELDS = ['email']
    USERNAME_FIELD = 'username'

    def get_absolute_url(self):
        return reverse('activate-user', kwargs={'slug': slugify(self.user_profile.activation_key)})

    def __unicode__(self):
        return self.get_full_name()

    def get_active_account(self):
        """

        :return:
        """
        return self.active_account




