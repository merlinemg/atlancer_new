import json
import logging
import threading
import functools
import warnings

from django.contrib.auth.models import Group
from django.core.mail import send_mail
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.deprecation import RemovedInDjango20Warning
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.conf import settings
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.generic import TemplateView
from rest_framework.authentication import SessionAuthentication

from apps.freelancer.general import anonymous_required
from apps.general.utils import send_email
from apps.profile.models import FreelancerProfile
from apps.sendbox.models import MailSet
from .forms import UserCreationForm, UserEditForm, UsernameEmailForm, UserLoginForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import User
from django.views.generic.edit import UpdateView, FormView
from registration.backends.hmac.views import RegistrationView
from django.views.decorators.cache import never_cache
# login
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.http import is_safe_url
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, authenticate, login
from django.http import HttpResponse
from django.template import Context, Template
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash, )

import threading

logger = logging.getLogger(__name__)


def logout(request, next_page=None,
           template_name='registration/logged_out.html',
           redirect_field_name=REDIRECT_FIELD_NAME,
           current_app=None, extra_context=None):
    """
    Logs out the user and displays 'You are logged out' message.
    """
    print request.user
    if request.user.is_authenticated():
        register_account = request.user.register_account
        active_account = request.user.active_account
        if register_account != active_account:
            user = request.user
            user.active_account = active_account
            user.save()
        auth_logout(request)

        if next_page is not None:
            next_page = resolve_url(next_page)

        if (redirect_field_name in request.POST or
                    redirect_field_name in request.GET):
            next_page = request.POST.get(redirect_field_name,
                                         request.GET.get(redirect_field_name))
            # Security check -- don't allow redirection to a different host.
            if not is_safe_url(url=next_page, host=request.get_host()):
                next_page = request.path

        if next_page:
            # Redirect to this page until the session has been cleared.
            return HttpResponseRedirect(next_page)

        current_site = get_current_site(request)
        context = {
            'site': current_site,
            'site_name': current_site.name,
            'title': _('Logged out')
        }
        if extra_context is not None:
            context.update(extra_context)

        if current_app is not None:
            request.current_app = current_app

        return TemplateResponse(request, template_name, context)
    else:
        return redirect('/')


def retrieve_username(request,
                      template_name='registration/retrieve_username.html',
                      current_app=None, extra_context=None):
    user = None
    context = {}
    if request.method == 'POST':
        form = UsernameEmailForm(request.POST)
        context = {
            'login_url': resolve_url(settings.LOGIN_URL),
            'form': form,
        }
        if form.is_valid():
            email = form.cleaned_data.get('email')
            if email:
                try:
                    user = User.objects.get(email=email)
                except User.DoesNotExist:
                    user = None
                    context.update({'error': 'User with this email does not exists.'})
    if user:
        context = {'user': user}
        send_email(template_name='registration/username_email.html', to_email=[email], context=context,
                   subject="Username retrieval confirmation.")
        return redirect('retrieve_username_success')
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def retrieve_username_success(request):
    return render_to_response('registration/retrieve_username_success.html', {},
                              context_instance=RequestContext(request))


class UserUpdateView(UpdateView):
    model = User
    form_class = UserEditForm
    template_name = "user_update_form.html"
    success_url = '/dashboard/'

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)


class CheckUser(TemplateView):
    template_name = 'account/login.html'

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            if request.user.is_authenticated():
                response_data['status'] = True
            else:
                response_data['status'] = False
            return HttpResponse(json.dumps(response_data), content_type="application/json")


def deprecate_current_app(func):
    """
    Handle deprecation of the current_app parameter of the views.
    """

    @functools.wraps(func)
    def inner(*args, **kwargs):
        if 'current_app' in kwargs:
            warnings.warn(
                "Passing `current_app` as a keyword argument is deprecated. "
                "Instead the caller of `{0}` should set "
                "`request.current_app`.".format(func.__name__),
                RemovedInDjango20Warning
            )
            current_app = kwargs.pop('current_app')
            request = kwargs.get('request', None)
            if request and current_app is not None:
                request.current_app = current_app
        return func(*args, **kwargs)

    return inner


@deprecate_current_app
@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='account/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    if request.method == 'GET' and request.user.is_authenticated():

        if request.user.active_account == "CL":
            return redirect('/client-profile/')
        else:
            return redirect('/freelancer-profile/')

    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    group = ""
    if request.method == "POST":
        next_url = request.POST.get('next')
        settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            user_obj = form.get_user()
            username = user_obj.username
            group = user_obj.active_account
            if group == 'CL':
                user_obj.active_account = 'CL'
                user_obj.save()
                group = Group.objects.get(name="Clients")
                users = group.user_set.all()
                for user in users:
                    if user.username == username:
                        if not is_safe_url(url=redirect_to, host=request.get_host()):
                            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

                        # Okay, security check complete. Log the user in.
                        auth_login(request, form.get_user())
                        if next_url != 'None':
                            redirects = next_url
                        else:
                            redirects = '/client-profile/'
                        return redirect(redirects)
            else:
                user_obj.active_account = 'FLR'
                user_obj.save()
                group = Group.objects.get(name='Freelancers')
                if next_url != 'None':
                    profile_url = next_url
                else:
                    profile_url = '/freelancer-profile/'
                create_profile = '/create-profile/'
                users = group.user_set.all()
                for user in users:
                    if user.username == username:
                        print user.id
                        # Ensure the user-originating redirection url is safe.
                        if not is_safe_url(url=redirect_to, host=request.get_host()):
                            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

                        # Okay, security check complete. Log the user in.
                        auth_login(request, form.get_user())

                        try:
                            is_user = FreelancerProfile.objects.get(user=request.user)
                        except:
                            is_user = None
                        if is_user:
                            freelancer_user = profile_url
                        else:
                            freelancer_user = create_profile
                        return redirect(freelancer_user)
        else:
            logger.debug("Login failed from " + request.META.get('REMOTE_ADDR'))

    else:
        form = authentication_form(request)
    if not group:
        group = "client"

    print group

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
        'group_name': group,
        'next': request.GET.get('next'),
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app
    return TemplateResponse(request, template_name, context)


class SignUpView(RegistrationView):
    """
    Display and perform registration of user.
    Django registration view is inherited by this class.
    """
    form_class = UserCreationForm
    Group.objects.get_or_create(name='Freelancers')
    Group.objects.get_or_create(name='Clients')

    def form_valid(self, form):
        """
         check the validation oi the form
        """
        new_user = self.register(form)
        success_url = self.get_success_url(new_user)

        # success_url may be a simple string, or a tuple providing the
        # full argument set for redirect(). Attempting to unpack it
        # tells us which one it is.
        print " form valid child +++++++++++++++++++"
        try:
            to, args, kwargs = success_url
            return redirect(to, *args, **kwargs)
        except ValueError:
            return redirect(success_url)

    def create_inactive_user(self, form):
        """
        Create the inactive user account and send an email containing
        activation instructions.

        """
        print "create_inactive_user "
        new_user = form.save(commit=True, group_name=self.request.POST['group'])
        new_user.is_active = False
        new_user.save()
        activation_key = self.get_activation_key(new_user)
        context = self.get_email_context(activation_key)

        mail_set = MailSet.objects.get(module_type='reg_success')
        recipient = new_user.email
        template = Template(mail_set.html_content)
        context = Context({'context': context, 'first_name': new_user.first_name})
        html = template.render(context)
        print html

        def Email():
            send_mail(mail_set.subject, mail_set.subject, 'tectdjango@gmail.com',
                      [str(recipient)], html_message=html)

        multyEmail = threading.Thread(name='Email', target=Email)
        multyEmail.start()
        # self.send_activation_email(new_user)

        return new_user

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form()
        response_data = {}
        if form.is_valid():
            print " post valid"
            response_data['success'] = "You email has successfully sent."
            return self.form_valid(form)
        else:
            print " post invalid"
            form = self.form_class(data=self.request.POST)
            response_data['status'] = False
            response_data['errors'] = form.errors
            return HttpResponse(json.dumps(response_data), content_type="application/json")
