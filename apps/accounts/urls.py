from django.conf.urls import url,include
from django.views.decorators.gzip import gzip_page

from registration.backends.hmac.views import ActivationView
from django.views.generic.base import TemplateView

from apps.freelancer.general import anonymous_required
from .views import SignUpView, UserUpdateView, login, logout, CheckUser

urlpatterns = [
    # url(r'^activate-user/(?P<slug>[-\w]+)/$', ActivationView.as_view(), name='activate-user'),
    # url(r'^edit-profile/(?P<pk>[-\d]+)/$', UserUpdateView.as_view(), name='edit-profile'),
    url(r'^register/(?P<type>[^/]+)', gzip_page(anonymous_required(SignUpView.as_view())), name='Register'),
    # url(r'^edit-profile/(?P<pk>[-\d]+)/$', EditProfileView.as_view(), name='edit-profile'),
    # url(r'^dashboard/$', DashboardView.as_view(), name='dashboard'),

    url(r'^login/', gzip_page(login) , name='Login'),
    url(r'^logout/$', logout, {"next_page": '/'}, name="logout"),
    url(r'^is_user/$', CheckUser.as_view(), name="is_user"),


    # ...
    # starting of profile url
    # url(r'^profile/', userProfile, name='profile'),
    # ending of profile url

    # registration urls starting
    url(r'^activate/complete/$',
        anonymous_required(TemplateView.as_view(
            template_name='registration/activation_complete.html'
        )),
        name='registration_activation_complete'),
    url(r'^activate/(?P<activation_key>[\w.@+-:]+)/$',
        ActivationView.as_view(),
        name='registration_activate'),
    url(r'^register/complete/$',
        anonymous_required(TemplateView.as_view(
            template_name='registration/registration_complete.html'
        )),
        name='registration_complete'),
    url(r'^register/closed/$',
        anonymous_required(TemplateView.as_view(
            template_name='registration/registration_closed.html'
        )),
        name='registration_disallowed'),
    url(r'', include('registration.auth_urls')),

    url(r'^signup_success/$',
    anonymous_required(TemplateView.as_view(
            template_name='account/signup_success.html'
        )),
        name='signup_success'),

    # ending of registration url
        ]