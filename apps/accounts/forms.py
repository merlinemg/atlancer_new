from django import forms
from django.forms import PasswordInput,TextInput
from captcha.fields import ReCaptchaField

from .models import User
from .admin import UserCreationForm as BaseUserCreationForm
from registration.forms import RegistrationForm
from django.contrib.auth.models import Group
from apps.profile.models import FreelancerProfile


class UserCreationForm(RegistrationForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput, min_length=8)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput, min_length=8)
    first_name = forms.RegexField(regex=r'[a-zA-Z]',
                                  error_message=(
                                      "Only alphabets is allowed."))
    last_name = forms.RegexField(regex=r'[a-zA-Z]',
                                  error_message=(
                                      "Only alphabets is allowed."))
    # captcha = ReCaptchaField()
    # new_captcha = ReCaptchaField(
    #     public_key='6LcfWxATAAAAAPdOmA4sF5JDY9G4yaoRVQMfdKrJ',
    #     private_key='6LcfWxATAAAAAJKMA7wROCTV3DNBPxTg0bTLnnje',
    #     use_ssl=True
    # )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'inputbox'

    def clean_email(self):
        # Check that the two password entries match
        email = self.cleaned_data.get("email")
        if not email:
            raise forms.ValidationError("This field is required.")
        else:
            if_exist = User.objects.filter(email=email)
            if if_exist:
                raise forms.ValidationError("This Email already exist.")
        return email

    def clean_first_name(self):
        # Check that the two password entries match
        first_name = self.cleaned_data.get("first_name")
        if not first_name:
            raise forms.ValidationError("This field is required.")
        return first_name

    def clean_last_name(self):
        # Check that the two password entries match
        last_name = self.cleaned_data.get("last_name")
        if not last_name:
            raise forms.ValidationError("This field is required.")
        return last_name


    def save(self, group_name,commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        print "******** entered save "
        print "group_name",group_name
        if commit:
            if group_name == "freelancer":
                user.register_account = 'FLR'
                user.active_account = 'FLR'
                user.save()
            else:
                user.register_account = 'CL'
                user.active_account = 'CL'
                user.save()
            if group_name == "freelancer":
                user.groups.add(Group.objects.get(name='Freelancers'))
            else:
                user.groups.add(Group.objects.get(name='Clients'))
        return user


class UserEditForm(forms.ModelForm):
    """
    Form to display user editing . Includes all the required
    fields.
    """
    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'loginfields'

    class Meta:
        model = User
        fields = ['email']


class UsernameEmailForm(forms.Form):
    email = forms.EmailField(label='Email address')


class UserLoginForm(forms.Form):
    """
    Form to display the login page details.
    """
    username = forms.CharField(max_length=50, widget=TextInput(attrs={'class': 'inp-login btn-full-width',}))
    password = forms.CharField(max_length=50, widget=PasswordInput())


