from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django_countries import settings
from django.db import models
from django.core.urlresolvers import reverse

from autoslug import AutoSlugField
from ckeditor.fields import RichTextField

from apps.sendbox.settings import *

from apps.sendbox.settings import EMAIL_SEND_TYPES


class Recipients(models.Model):
    """
    Recipients model.
    """
    recipient_name = models.CharField(_('Recipient name'), max_length=255)
    to_email = models.EmailField(_('Email'), max_length=255, unique=True)

    def __str__(self):
        return self.to_email


class MailSet(models.Model):
    """
    MailSet model.
    """
    template_name = models.CharField(_('Email template name'), max_length=255)
    module_type = models.CharField(_('Email for'), max_length=255, unique=True,
                                   choices=EMAIL_SEND_TYPES,
                                   help_text="Please select the module type to configure mail")
    subject = models.CharField(_('Email subject'), max_length=255)
    from_email = models.EmailField(_('From email'), max_length=255, null=True, blank=True)
    html_content = RichTextField()
    text_content = models.TextField(null=True, blank=True)
    is_publish = models.BooleanField(default=False)
    publish_date = models.DateTimeField(auto_now=True)
    slug = AutoSlugField(populate_from='template_name')

    def __str__(self):
        return self.template_name

    class Meta:
        ordering = ['-publish_date']
        verbose_name = "Mail configuration"
        verbose_name_plural = "Mail configurations"
        default_permissions = ('add', 'change', 'delete', 'view')

    def get_absolute_url(self):
        return reverse('template_update', kwargs={'slug': self.slug})


class Attachment(models.Model):
    """
    Attachment model.
    """
    attach = models.ForeignKey(MailSet, related_name='attachments', verbose_name="Attach file")
    attach_file = models.FileField(upload_to='attachments/')

    def __str__(self):
        return self.attach.module_type


class MailSetCampaign(models.Model):
    """
    MailSetCampaign model.
    """

    mail_set = models.OneToOneField(MailSet, related_name='template')
    to_email = models.ManyToManyField(Recipients, blank=True)
    attach = models.ForeignKey(Attachment, null=True, blank=True)
    is_publish = models.BooleanField(default=False)
    publish_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.mail_set.template_name

    class Meta:
        ordering = ['-publish_date']
        verbose_name = "Mail Campaign"
        verbose_name_plural = "Mail Campaigns"
        default_permissions = ('add', 'change', 'delete', 'view')

