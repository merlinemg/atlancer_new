import json
from django.http import HttpResponseRedirect, HttpResponse
from django.template.response import TemplateResponse
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.template import RequestContext
from django.template.loader import render_to_string

from apps.sendbox.forms import MailSetForm, MailSetCampaignForm, EmailTemplateUpdateForm, MailSetCampaignUpdateForm
from apps.sendbox.models import MailSet, MailSetCampaign
from django.contrib.auth.decorators import login_required, user_passes_test
from functools import wraps


class EmailTemplateUpdateView(ListView):

    model = MailSet
    form = EmailTemplateUpdateForm
    template_name = 'email_template_update.html'

    def get_context_data(self, **kwargs):
        context = super(EmailTemplateUpdateView, self).get_context_data(**kwargs)
        context['active_menu'] = 'email_update'
        context['active_tab'] = 'update_temp'
        try:
            mailSet = MailSet.objects.get(slug=self.kwargs['slug'])
            context['form'] = EmailTemplateUpdateForm(instance=mailSet)
            try:
                template_obj = MailSetCampaign.objects.get(mail_set=mailSet)
                context['user_form'] = MailSetCampaignForm(instance=template_obj)
            except:
                context['user_form'] = MailSetCampaignForm()
        except:
            context['form'] = EmailTemplateUpdateForm()
            context['user_form'] = MailSetCampaignForm()

        return context

    def post(self, request, *args, **kwargs):
        mailSet = MailSet.objects.get(slug=self.kwargs['slug'])
        message = None
        if request.POST.get('Form_type') == 'assign_user':
            active_tab = 'assign_user'
            try:
                mailSet = MailSet.objects.get(slug=self.kwargs['slug'])
                form = EmailTemplateUpdateForm(instance=mailSet)
                template_obj = MailSetCampaign.objects.get(mail_set=mailSet)
                user_form = MailSetCampaignUpdateForm(request.POST, instance=template_obj)
                if user_form.is_valid():
                    users_form = user_form.save(commit=False)
                    users_form.save()
                    user_form.save_m2m()
                    message = 'Update users email successfully'
                else:
                    print "user_form error", user_form.errors
            except:
                user_form = MailSetCampaignForm(request.POST)
                if user_form.is_valid():
                    user_form.save()
                    message = 'Update users email successfully'
                else:
                    print "user_form error", user_form.errors
        else:
            form = self.form(request.POST, instance=mailSet)
            active_tab = 'update_temp'
            if form.is_valid():
                new_form = form.save(mail_obj=mailSet)
                message = 'Update  template successfully'
            else:
                print form.errors
            try:
                mailSet = MailSet.objects.get(slug=new_form.slug)
                template_obj = MailSetCampaign.objects.get(mail_set=mailSet)
                user_form = MailSetCampaignForm(instance=template_obj)
            except:
                user_form = MailSetCampaignForm()
        context = {'errors': form.errors, 'form': form, 'user_form': user_form, 'active_tab': active_tab,
                   'message': message}
        return TemplateResponse(request, self.template_name, context)


class EmailTemplateListView(ListView):

    model = MailSet
    template_name = 'email_template_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(EmailTemplateListView, self).get_context_data(**kwargs)
        context['active_menu'] = 'email_list'
        return context


class EmailCreateView(ListView):
    template_name = "email_create.html"
    form_class = MailSetForm
    model = MailSet

    def get_context_data(self, **kwargs):
        context = super(EmailCreateView, self).get_context_data(**kwargs)
        context['active_menu'] = 'email_create'
        context['active_tab'] = 'update_temp'
        context['form'] = MailSetForm()
        context['user_form'] = MailSetCampaignForm()

        return context

    def post(self, request, *args, **kwargs):
        message = None
        if request.POST.get('Form_type') == 'assign_user':
            active_tab = 'assign_user'
            try:
                mailSet = MailSet.objects.get(id=request.POST.get('mail_set'))
                template_obj = MailSetCampaign.objects.get(mail_set=mailSet)
                user_form = MailSetCampaignUpdateForm(request.POST, instance=template_obj)
                if user_form.is_valid():
                    item_form = user_form.save(commit=False)
                    item_form.save()
                    user_form.save_m2m()
                    message = 'Success'
                else:
                    print "user_form error", user_form.errors
            except:
                user_form = MailSetCampaignForm(request.POST)
                if user_form.is_valid():
                    user_form.save()
                    message = 'data added successfully'
                else:
                    print "user_form error", user_form.errors
            try:
                mail_set_id = request.POST.get('mail_set')
                mail_obj = MailSet.objects.get(template__mail_set=mail_set_id)
                form = self.form_class(instance=mail_obj)
            except:
                form = self.form_class()
        else:
            form = self.form_class(request.POST)
            active_tab = 'update_temp'
            if form.is_valid():
                form.save()
                message = 'data added successfully'
            else:
                print form.errors
            # try:
            #     mail_set_id = form.id
            #     print "IIIII", mail_set_id
            #     mail_obj = MailSetCampaign.objects.get(id=mail_set_id)
            #     print "FFFFFFFFFFFFFFFFFFFF", mail_obj
            #     user_form = MailSetCampaignForm(instance=mail_obj)
            # except:
            #     user_form = MailSetCampaignForm()
            user_form = MailSetCampaignForm()


        context = {'active_menu': 'email_create', 'errors': form.errors, 'form': form, 'user_form': user_form,
                   'message': message, 'active_tab': active_tab}
        return TemplateResponse(request, self.template_name, context)


class StaticCampaignSetUpView(ListView):
    template_name = "set_up_static_campaign.html"
    form = MailSetCampaignForm
    model = MailSetCampaign

    def get_context_data(self, **kwargs):
        data = super(StaticCampaignSetUpView, self).get_context_data(**kwargs)
        data['active_menu'] = 'email_setup'
        data['form'] = MailSetCampaignForm()
        #print "hhhhhhhh", self.request.GET.get('mail_set')
        return data

    def post(self, request, *args, **kwargs):
        message = None
        if request.is_ajax():
            response_data = {}
            response_data['status'] = True
            try:
                mailSet = MailSet.objects.get(id=request.POST.get('mail_set'))
                template_obj = MailSetCampaign.objects.get(mail_set=mailSet)
                form = MailSetCampaignForm(instance=template_obj)
            except:
                form = MailSetCampaignForm()
                response_data['status'] = False
            print "form", form
            response_data['html_content'] = render_to_string('ajax/email_recipients.html',
                                                             {'form': form},
                                                             context_instance=RequestContext(request))
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        else:
            try:
                mailSet = MailSet.objects.get(id=request.POST.get('mail_set'))
                template_obj = MailSetCampaign.objects.get(mail_set=mailSet)
                form = MailSetCampaignUpdateForm(request.POST, instance=template_obj)
                if form.is_valid():
                    item_form = form.save(commit=False)
                    item_form.save()
                    form.save_m2m()
                    message = 'Success'
                else:
                    print "user_form error", form.errors
            except:
                form = MailSetCampaignForm(request.POST)
                if form.is_valid():
                    form.save()
                    message = 'Success'
                else:
                    print "user_form error", form.errors
            context = {'active_menu': 'email_setup',  'form': form, 'errors': form.errors, 'message': message}
            return TemplateResponse(request, self.template_name, context)