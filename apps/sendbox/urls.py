from django.conf.urls import url
from apps.sendbox.views import EmailCreateView, StaticCampaignSetUpView,\
    EmailTemplateListView, EmailTemplateUpdateView

urlpatterns = [
    url(r'^mail-launch/', EmailCreateView.as_view(), name="email_create"),
    url(r'^campaign-setup/', StaticCampaignSetUpView.as_view(), name="campaign_setup"),
    url(r'^template-list/$', EmailTemplateListView.as_view(), name='template_list'),
    url(r'^(?P<slug>[-\w]+)/$', EmailTemplateUpdateView.as_view(), name='template_update'),
]