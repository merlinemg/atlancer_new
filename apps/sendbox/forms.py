from django import forms

from apps.sendbox.models import MailSet, MailSetCampaign


class MailSetForm(forms.ModelForm):
    """
    For to get template content...
    """
    class Meta:
        model = MailSet
        exclude = ['publish_date', 'from_email', ]

    def __init__(self, *args, **kwargs):
        super(MailSetForm, self).__init__(*args, **kwargs)


class MailSetCampaignForm(forms.ModelForm):
    """
    For to get template content...
    """
    class Meta:
        model = MailSetCampaign
        exclude = ['publish_date',]

    def __init__(self, *args, **kwargs):
        super(MailSetCampaignForm, self).__init__(*args, **kwargs)
        self.fields['to_email'].widget.attrs.update({'class': 'form-control'})
        self.fields['to_email'].widget.attrs.update({'multiple': 'multiple'})




class MailSetCampaignUpdateForm(forms.ModelForm):
    """
    For to get template content...
    """
    class Meta:
        model = MailSetCampaign
        exclude = ['publish_date', 'id']

    def __init__(self, *args, **kwargs):
        super(MailSetCampaignUpdateForm, self).__init__(*args, **kwargs)
        self.fields['to_email'].widget.attrs.update({'class': 'form-control'})
        self.fields['to_email'].widget.attrs.update({'multiple': 'multiple'})

    def save(self, commit=True, mail_obj=None):
        mail_form = super(MailSetCampaignUpdateForm, self).save(commit=False)
        if commit:
            mail_form.id = mail_obj.id
            mail_form.mail_set = mail_obj.mail_set
            mail_form.save()
        return mail_form



class EmailTemplateUpdateForm(forms.ModelForm):
    """
    For update email template
    """
    class Meta:
        model = MailSet
        exclude = ['id']
        fields = ['template_name', 'module_type', 'subject', 'html_content', 'text_content', 'is_publish']

    def save(self, commit=True, mail_obj=None):
        email_form = super(EmailTemplateUpdateForm, self).save(commit=False)
        if commit:
            email_form.id = email_form.id
            email_form.save()
        return email_form