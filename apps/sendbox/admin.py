from django.contrib import admin

from apps.sendbox.models import Recipients, MailSet, MailSetCampaign


admin.site.register(MailSetCampaign)
admin.site.register(MailSet)
admin.site.register(Recipients)