from django.http import HttpResponseRedirect


def anonymous_site_admin_required(view_function, redirect_to=None):
    return AnonymousAdminRequired(view_function, redirect_to)


class AnonymousAdminRequired(object):
    def __init__(self, view_function, redirect_to):
        if redirect_to is None:
            redirect_to = '/admin/mail/campaign-setup/'
        self.view_function = view_function
        self.redirect_to = redirect_to

    def __call__(self, request, *args, **kwargs ):
        if request.user.is_anonymous() or request.user.groups.filter(name__in=['Site_admin']).exists() == False:
            return HttpResponseRedirect('/site_admin/')
        return self.view_function(request, *args, **kwargs)