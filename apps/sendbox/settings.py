from __future__ import unicode_literals
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

EMAIL_SEND_TYPES = (('c_2_admin', _('Contact email to admin')),
                    ('c_2_customer', _('Contact reply email to end user')),
                    ('reg_success', _('Registration success')),
                    ('admin_2_InvitingClient', _('Job Inviting notification to client')),
                    ('admin_2_InvitedFreelancer', _('Job Invited notification to freelancer')),
                    ('admin_2_Apply_Job_Client', _('Job Applied notification to Client')),
                    ('admin_2_Apply_Job_Freelancer', _('Job Applied notification to freelancer')),
                    ('admin_2_Rejected_ByClient_Freelancer', _('Job rejected by client notification to freelancer')),
                    ('admin_2_Rejected_ByClient_Client', _('Job rejected by client notification to client')),
                    ('admin_2_declineInvitation_Client', _('Invitation rejected notification to client')),
                    ('admin_2_declineInvitation_Freelancer', _('Invitation rejected notification to freelancer')),
                    ('admin_2_acceptInvitation_Freelancer', _('Invitation accepted notification to freelancer')),
                    ('admin_2_acceptInvitation_Client', _('Invitation accepted notification to client')),
                    ('admin_2_JobExpired_Client', _('Job expired notification to client')),
                    )

