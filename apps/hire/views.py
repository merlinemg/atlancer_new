import json
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.db.models import Count, Min, Sum, Avg
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.http import HttpResponse

# Create your views here.
from apps.clients.mixins import ClientMixin
from apps.clients.models import ClientJobs
from apps.freelancer.mixins import FreelancerMixin
from apps.hire.mixins import ContractMixin
from apps.hire.models import HiredJobsContract, JobsMilestone, HiredJobHistory
from apps.hire.forms import HiredJobsContractForm, JobsMilestoneForm, ChangesMilestoneRateForm
from apps.profile.models import FreelancerProfile, ClientProfile


class ContractListView(ClientMixin, FreelancerMixin, TemplateView):
    template_name = 'contract_list.html'

    @csrf_exempt
    @method_decorator(login_required)
    # @method_decorator(my_decorator('add_applyjob'))
    def dispatch(self, *args, **kwargs):
        return super(ContractListView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):

        contracts = self.hireJobsContract_filter(self.request.user)
        return render(request, 'contract_list.html', {'contracts': contracts,'status':self.get_status_contractlist()})

    def post(self, request, *args, **kwargs):

        if request.is_ajax():
            status = request.POST.get('status')
            startdate = request.POST.get('startdate')
            search =  request.POST.get('search')

            if status and not startdate and not search:
                resturn_data = {}
                object_list =self.hireJobsContract_filter_status(self.request.user,status)

                html = render_to_string('list.html',
                                        {'object_list': object_list})

                resturn_data['html'] = html
                return HttpResponse(json.dumps(resturn_data), content_type='text/html')
            # elif startdate and not status:
            #     resturn_data = {}
            #     object_list = self.hireJobsContract_filter_status(self.request.user, startdate)
            #
            #     html = render_to_string('list.html',
            #                             {'object_list': object_list})
            #
            #     resturn_data['html'] = html
            #     return HttpResponse(json.dumps(resturn_data), content_type='text/html')
            elif search and not startdate and not status:
                resturn_data = {}
                object_list = self.hireJobsContract_filter_search(self.request.user, search)

                html = render_to_string('list.html',
                                        {'object_list': object_list})

                resturn_data['html'] = html
                return HttpResponse(json.dumps(resturn_data), content_type='text/html')
            elif status =='' and startdate =='':

                resturn_data = {}
                contracts = self.hireJobsContract_filter(self.request.user)

                html = render_to_string('list.html',
                                        {'object_list': contracts})

                resturn_data['html'] = html
                return HttpResponse(json.dumps(resturn_data), content_type='text/html')



        else:

            contracts = self.hireJobsContract_filter(self.request.user)
            return render(request, 'contract_list.html',
                          {'contracts': contracts, 'status': self.get_status_contractlist()})




class FreelancerContractView(ClientMixin, FreelancerMixin, TemplateView):
    template_name = 'freelancer_hire_contract.html'

    @csrf_exempt
    @method_decorator(login_required)
    # @method_decorator(my_decorator('add_applyjob'))
    def dispatch(self, *args, **kwargs):
        return super(FreelancerContractView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):

        job_slug = self.kwargs['slug']
        slug1 = self.kwargs['slug1']
        job_details = self.get_current_job(job_slug)
        freelancer = self.get_freelancer_profile_instance()
        try:
            client_user = ClientProfile.objects.get(slug=slug1)
        except:
            client_user =ClientProfile.objects.get(id=slug1)
        freelancer_hire = HiredJobsContract.objects.get(client_job=job_details, freelancer_profile=freelancer,
                                         client_profile=client_user)

        sum = 0
        for milestone in freelancer_hire.milestone.all():
            sum = sum+milestone.rate
        return render(request, 'freelancer_hire_contract.html', {'hire': freelancer_hire, 'job_sum':sum})


class HireView(ClientMixin, FreelancerMixin, TemplateView):
    template_name = 'hire.html'

    @csrf_exempt
    @method_decorator(login_required)
    # @method_decorator(my_decorator('add_applyjob'))
    def dispatch(self, *args, **kwargs):
        return super(HireView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        job_slug = self.kwargs['slug']
        slug1 = self.kwargs['slug1']
        job_details = self.get_current_job(job_slug)
        if self.request.user.active_account == 'CL':
            client_user = self.get_client_profile()
            freelancer = self.get_current_freelancer(slug1)
        else:
            freelancer =self.get_freelancer_profile_instance()
            try:
                client_user = ClientProfile.objects.get(slug=slug1)
            except:
                client_user =ClientProfile.objects.get(id=slug1)

        try:
            hire = HiredJobsContract.objects.get(client_job=job_details, freelancer_profile=freelancer,
                                         client_profile=client_user)
        except:
            hire = HiredJobsContract(client_job=job_details, freelancer_profile=freelancer, client_profile=client_user,
                             job_title=job_details.job_title, job_number=job_details.job_number,
                             job_main_category=job_details.job_main_category,
                             job_description=job_details.job_description, payment_type=job_details.payment_type,
                             payment=job_details.payment, company=client_user.company)
            hire.save()
        sum = 0
        for milestone in hire.milestone.all():
            sum = sum+milestone.rate
        return render(request, 'hire.html', {'hire': hire, 'job_sum': sum})




class HireEditAddView(ClientMixin, FreelancerMixin, ContractMixin, TemplateView):
    template_name = 'hire.html'
    form_class = JobsMilestoneForm

    @csrf_exempt
    @method_decorator(login_required)
    # @method_decorator(my_decorator('add_applyjob'))
    def dispatch(self, *args, **kwargs):
        return super(HireEditAddView, self).dispatch(*args, **kwargs)


    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            hire = HiredJobsContract.objects.get(id=request.POST.get('hire_id'))
            if request.POST.get('action') == 'add':
                form = JobsMilestoneForm(request.POST)
                if form.is_valid():
                    milestone_id = form.save()
                    hire.milestone.add(milestone_id)
                    response_data['status'] = True
                    sum = 0
                    for milestone in hire.milestone.all():
                        sum = sum+milestone.rate
                    response_data['job_sum'] = sum
                else:
                    print "************* Error", form.errors.as_text
                    response_data['status'] = False
                    response_data['errors'] = form.errors
                response_data['html_content'] = render_to_string('ajax/milestone_list.html', {'hire': hire})
            if request.POST.get('action') == 'Delete':
                try:
                    # ########### hold delete milestone due to permission enhancement ################### #
                    # milestone = JobsMilestone.objects.get(id=request.POST.get('id'))
                    # hire.milestone.remove(milestone)
                    # ##########################################
                    response_data['status'] = True
                except:
                    response_data['status'] = False
            if request.POST.get('action') == 'Edit_open':
                milestone = JobsMilestone.objects.get(id=request.POST.get('id'))
                milestone_form = JobsMilestoneForm(instance=milestone)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('ajax/edit_milestone.html',
                                                                 {'hire': hire, 'milestone_form': milestone_form,
                                                                  'milestone': milestone})
            if request.POST.get('action') == 'edit':
                milestone = JobsMilestone.objects.get(id=request.POST.get('milestone_edit_id'))
                milestone_form = JobsMilestoneForm(request.POST, instance=milestone)
                milestone_form.save()
                response_data['status'] = True
                response_data['html_content'] = render_to_string('ajax/edit_milestone.html',
                                                                 {'hire': hire, 'milestone_form': milestone_form})
            if request.POST.get('action') == 'switch':
                client_user = ClientProfile.objects.get(user=self.request.user)
                hire_job = ClientJobs.objects.get(id=request.POST.get('job_id'))
                hire = HiredJobsContract.objects.get(id=request.POST.get('hire_id'), client_profile=client_user,
                                                     client_job=hire_job)
                if hire.payment_type == 'Fixed':
                    for milestone in hire.milestone.all():
                        milestone.delete()
                    hire.milestone.clear()
                    hire.payment_type = 'Hourly'
                    hire.save()
                else:
                    hire.payment_type = 'Fixed'
                    hire.save()
                print hire.payment_type
                response_data['status'] = True

            if request.POST.get('action') == 'payment_check':
                client_user = ClientProfile.objects.get(user=self.request.user)
                hire_job = ClientJobs.objects.get(id=request.POST.get('job_id'))
                hire = HiredJobsContract.objects.get(id=request.POST.get('hire_id'), client_profile=client_user,
                                                     client_job=hire_job)
                if hire.payment_type == 'Fixed':
                    if hire.milestone.all().exists():
                        response_data['status'] = False
                    else:
                        response_data['status'] = True
                else:
                    response_data['status'] = True
                print hire.payment_type
            if request.POST.get('action') == 'job_description_save':
                if request.POST.get('job_descriptions'):
                    hire.job_description = request.POST.get('job_descriptions')
                    hire.save()
                #print hire.job_description
                response_data['status'] = True
                response_data['job_description'] = hire.job_description
            if request.POST.get('action') == 'job_title_save':
                if request.POST.get('job_title'):
                    hire.job_title = request.POST.get('job_title')
                    hire.save()
                #print hire.job_title
                response_data['status'] = True
                response_data['job_title'] = hire.job_title
                response_data['job_number'] = hire.job_number
            if request.POST.get('action') == 'hire_request':
                client_profile = ClientProfile.objects.get(user=hire.client_profile.user)
                freelancer_profile = FreelancerProfile.objects.get(user=hire.freelancer_profile.user)
                hire_history = HiredJobHistory(hire_job=hire, client_user=client_profile,
                                               freelancer_user=freelancer_profile, rate_change=hire.payment)
                hire_history.save()
                hire.change_made_client = True
                hire.save()
                response_data['status'] = True

            if request.POST.get('action') == 'contract_agree':
                query = self.get_contract_query()
                hire = query.get('hire')
                client_profile = query.get('client_profile')
                freelancer_profile = query.get('freelancer_profile')
                hire.status = 'APPROVED'
                hire.change_made_client = False
                hire.save()
                for milestone in hire.milestone.all():
                    milestone.status = 'APPROVED'
                    milestone.save()
                hire_history = HiredJobHistory(hire_job=hire, client_user=client_profile,
                                               freelancer_user=freelancer_profile, rate_change=hire.payment)
                hire_history.save()
                response_data['status'] = True

            if request.POST.get('action') == 'change_request':
                milestone = JobsMilestone.objects.get(id=request.POST.get('id'))
                milestone_form = ChangesMilestoneRateForm(instance=milestone)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('ajax/milestone_change_request.html',
                                                                 {'hire': hire, 'milestone_form': milestone_form,
                                                                  'milestone': milestone})
            if request.POST.get('action') == 'change_request_save':
                milestone = JobsMilestone.objects.get(id=request.POST.get('milestone_edit_id'))
                print request.POST.get('rate_change')
                milestone_form = ChangesMilestoneRateForm(request.POST, instance=milestone)
                if milestone_form.is_valid():
                    milestone_form.save()
                    hire.change_made_client = False
                    hire.save()
                else:
                    print "Erorrrrrrrr", milestone_form.errors
                response_data['status'] = True

            return HttpResponse(json.dumps(response_data), content_type="application/json")