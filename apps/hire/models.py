from django.db import models
from django.core.validators import MinLengthValidator
from django_countries import settings
import datetime
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string
from django.utils.text import slugify
# Create your models here.
from apps.clients.models import ClientJobs
from apps.freelancer.models import WorkCategories, WorkSubCategories
from apps.profile.models import FreelancerProfile, ClientProfile


class JobsMilestone(models.Model):
    """
       Job Milestone model.
    """
    MILESTONE_STATUS = (
        ('APPROVED', 'Approved'),
        ('PENDING', 'Pending'),
        ('HOLD', 'Hold'),
    )
    milestone_title = models.CharField(_('Milestone Title'), max_length=255)
    milestone_description = models.TextField(_('Milestone Description'), validators=[MinLengthValidator(00)])
    start_date = models.DateField(_('Milestone start date'), max_length=255, default=datetime.date.today)
    close_date = models.DateField(_('Milestone close date'), max_length=255, default=datetime.date.today)
    rate = models.IntegerField(_('Hire Payment'), default=0)
    rate_change = models.IntegerField(_('Hire Payment'), default=0, null=True, blank=True)
    status = models.CharField(max_length=10, choices=MILESTONE_STATUS, default='PENDING',
                              verbose_name="Milestone status")
    def __str__(self):
        return self.milestone_title

    class Meta:
        verbose_name = "Milestone"
        verbose_name_plural = "Milestones"
        default_permissions = ('add', 'change', 'delete', 'view')


class HiredJobsContract(models.Model):
    """
    Hire Jobs model.
    """
    STATUS = (
        ('APPROVED', 'Approved'),
        ('PENDING', 'Pending'),
        ('HOLD', 'Hold'),
    )
    PAYMENT_TYPE = (
        ('Fixed', 'Fixed'),
        ('Hourly', 'Hourly'),
    )
    client_job = models.ForeignKey(ClientJobs, related_name='hire_jobs', verbose_name="Client Job")
    freelancer_profile = models.ForeignKey(FreelancerProfile, related_name='hire_freelancer',
                                           verbose_name="Hired Freelancer")
    client_profile = models.ForeignKey(ClientProfile, related_name='hire_client', verbose_name="Hired Client")
    job_title = models.CharField(_('Hire Job title'), max_length=255)
    job_number = models.IntegerField(_('Hire Job Number'), default=0)
    job_main_category = models.ForeignKey(WorkCategories, related_name='hire_job_main_category',
                                          verbose_name="Hire Job Category")
    job_description = models.TextField(_('Hire Job Description'), validators=[MinLengthValidator(00)])
    payment_type = models.CharField(_('Hire PaymentType'), max_length=10, choices=PAYMENT_TYPE, default='Fixed',)
    payment = models.IntegerField(_('Hire Payment'), default=0)

    company = models.CharField(_('Company Name'), null=True, max_length=255, default='')

    milestone = models.ManyToManyField(JobsMilestone, related_name='hire_job_sub_category',
                                       verbose_name="Jobs Milestone", blank=True)

    slug = models.SlugField(max_length=255, unique=True,auto_created=True,editable=False)
    status = models.CharField(max_length=10, choices=STATUS, default='PENDING',
                              verbose_name="contract status")
    change_made_client = models.BooleanField(default=False)
    change_rate_flag = models.BooleanField(default=False)

    def __str__(self):
        return "{0}::{1}".format(self.client_job.user.username, self.freelancer_profile.user.username)

    def save(self, *args, **kwargs):
        super(HiredJobsContract, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.job_title) + "-" + unique_id
            self.save()

    class Meta:
        verbose_name = "HireJob"
        verbose_name_plural = "HireJobs"
        default_permissions = ('add', 'change', 'delete', 'view')


class HiredJobHistory(models.Model):
    HISTORY_STATUS = (
        ('CHANGE_REQUEST', 'Change'),
        ('APPROVED', 'Approved'),
        ('PENDING', 'Pending'),
    )
    hire_job = models.ForeignKey(HiredJobsContract, related_name='hire_job', verbose_name="Hire Job")

    client_user = models.ForeignKey(ClientProfile, related_name='hire_client_user',
                                    verbose_name="client_user")
    freelancer_user = models.ForeignKey(FreelancerProfile, related_name='hire_freelancer_user',
                                        verbose_name="freelancer_user", null=True)

    post_date = models.DateTimeField(verbose_name='Date', auto_now_add=True)
    status = models.CharField(max_length=10, choices=HISTORY_STATUS, default='PENDING',
                              verbose_name="contract status")
    rate_change = models.IntegerField(_('Rate Change'), default=0)
    comment = models.TextField(verbose_name='comment', blank=True, null=True)

    def __str__(self):
        return "{0}::{1}::{2}".format(self.hire_job.job_title, self.client_user.user.username,
                                      self.post_date)

    class Meta:
        verbose_name = "Hired Job History"
        verbose_name_plural = "Hired Job Histories"
        default_permissions = ('add', 'change', 'delete', 'view')