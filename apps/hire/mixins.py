from apps.hire.models import HiredJobsContract
from apps.profile.models import ClientProfile
from apps.profile.models import FreelancerProfile


class ContractMixin(object):
     def get_contract_query(self):
        """
        Get all query sets...
        :return:query
        """
        hire_id = self.request.POST.getlist('shortlist')
        hire = HiredJobsContract.objects.get(id=self.request.POST.get('hire_id'))
        client_profile = ClientProfile.objects.get(user=hire.client_profile.user)
        freelancer_profile = FreelancerProfile.objects.get(user=hire.freelancer_profile.user)
        query = {}
        query['hire'] = hire
        query['client_profile'] = client_profile
        query['freelancer_profile'] = freelancer_profile
        return query