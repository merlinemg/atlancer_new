from ckeditor.widgets import CKEditorWidget
from django import forms
from django.forms import ModelForm
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from apps.hire.models import HiredJobsContract, JobsMilestone


class HiredJobsContractForm(ModelForm):
    """
    Form for hire
    """

    class Meta:
        model = HiredJobsContract
        exclude = ['status']

    def __init__(self, *args, **kwargs):
        super(HiredJobsContractForm, self).__init__(*args, **kwargs)

        # for field in self.fields:
        #     self.fields['first_name'].widget.attrs['readonly'] = 'readonly'
        #     self.fields['last_name'].widget.attrs['readonly'] = 'readonly'
        #     self.fields['email'].widget.attrs['readonly'] = 'readonly'

    def save(self, commit=True):
        hire = super(HiredJobsContractForm, self).save(commit=False)
        if commit:
            hire.save()
        return hire


class JobsMilestoneForm(forms.ModelForm):
    """
    Form to add add milestone
    """

    class Meta:
        model = JobsMilestone
        exclude = ['status']

    def __init__(self, *args, **kwargs):
        super(JobsMilestoneForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs['class'] = 'datepicker milestone_edit_start_date'
        self.fields['close_date'].widget.attrs['class'] = 'datepicker milestone_edit_end_date'
        self.fields['milestone_description'].widget.attrs['rows'] = '3'


    def save(self, commit=True):
        milestone = super(JobsMilestoneForm, self).save(commit=False)
        if commit:
            milestone.save()
        return milestone


class ChangesMilestoneRateForm(forms.ModelForm):
    """
    Form to add add milestone
    """

    class Meta:
        model = JobsMilestone
        exclude = ['status']

    def __init__(self, *args, **kwargs):
        super(ChangesMilestoneRateForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs['readonly'] = 'readonly'
        self.fields['milestone_title'].widget.attrs['readonly'] = 'readonly'
        self.fields['close_date'].widget.attrs['readonly'] = 'readonly'
        self.fields['milestone_description'].widget.attrs['rows'] = '3'
        self.fields['milestone_description'].widget.attrs['readonly'] = 'readonly'
        self.fields['rate'].widget.attrs['readonly'] = 'readonly'

    def save(self, commit=True):
        milestone = super(ChangesMilestoneRateForm, self).save(commit=False)
        if commit:
            milestone.save()
        return milestone