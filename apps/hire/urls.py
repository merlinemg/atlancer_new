from django.conf.urls import url
from apps.hire.views import HireView, HireEditAddView, ContractListView, FreelancerContractView

urlpatterns = [
    url(r'^(?P<slug>[-\w]+)/(?P<slug1>[-\w]+)/$', HireView.as_view(), name='hire'),
    url(r'^hire_add_edit/$', HireEditAddView.as_view(), name='add_edit_hire'),
    url(r'^contracts/$', ContractListView.as_view(), name='contract'),
    url(r'^freelancer_contract/(?P<slug>[-\w]+)/(?P<slug1>[-\w]+)/$', FreelancerContractView.as_view(), name='freelancer_contract'),
]
