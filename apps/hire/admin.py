from django.contrib import admin

# Register your models here.
from django.contrib import admin
from apps.hire.models import JobsMilestone, HiredJobsContract, HiredJobHistory


admin.site.register(HiredJobsContract)
admin.site.register(JobsMilestone)
admin.site.register(HiredJobHistory)