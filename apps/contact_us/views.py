import logging

from django.contrib import messages
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, FormView
from apps.contact_us.forms import ContactForm
from apps.contact_us.models import Category
from django.template.loader import get_template
from django.template import Context

from apps.contact_us.mixins import ContactMixin
from apps.sendbox.models import MailSet, MailSetCampaign

logger = logging.getLogger(__name__)

class ContactView(ContactMixin, FormView):

    template_name = 'contactus.html'
    form_class = ContactForm
    success_url = '/contact/'

    def form_valid(self, form):
        to_s = []
        mail_set = MailSet.objects.get(module_type='c_2_admin')
        user_mail_set = MailSet.objects.get(module_type='c_2_customer')
        mailcampaign = MailSetCampaign.objects.get(mail_set=mail_set)
        for recipient in mailcampaign.to_email.all():
            admin_msg = EmailMessage(mail_set.subject, mail_set.html_content, 'tectdjango@gmail.com',
                               [str(recipient)])
            admin_msg.content_subtype = "html"  # Main content is now text/html
            is_send = self.get_logger(admin_msg,'tectdjango@gmail.com',recipient)

            # to_s.append(recipient.to_email)
        user_msg = EmailMessage(user_mail_set.subject, user_mail_set.html_content, 'tectdjango@gmail.com', [form.cleaned_data['email']])
        user_msg.content_subtype = "html"  # Main content is now text/html
        is_send = self.get_logger(user_msg,'tectdjango@gmail.com',form.cleaned_data['email'])
        print is_send
        if is_send != False:
            form.save()
        messages.success(self.request, "Your mail has been sent! ")
        return super(ContactView, self).form_valid(form)


