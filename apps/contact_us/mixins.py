import logging

from apps.contact_us.models import Category
logger = logging.getLogger(__name__)

class ContactMixin(object):

    def get_logger(self,email_obj,frm,to):
        """
        Get email_obj.
        :return:email_obj
        """
        try:
            email_obj.send()
        except:
            email_obj = False
            logger.debug("Email sending from "+frm+ " to "+str(to)+" failed")
        return email_obj


    def get_category(self,category_obj):
        """
        Get category.
        :return:category object
        """
        category = Category.objects.get(pk=category_obj)
        return category
