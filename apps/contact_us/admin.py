from django.contrib import admin

# Register your models here.
from apps.contact_us.models import ContactUs, Category


class ContactUsAdmin(admin.ModelAdmin):
    list_filter = ['category']
    search_fields = ['category']


admin.site.register(ContactUs, ContactUsAdmin)
admin.site.register(Category)
