from django.db import models


# Create your models here.

from django.db import models


class Category(models.Model):
    contact_category = models.CharField(max_length=200)

    def __str__(self):
        return self.contact_category

    class Meta:
        verbose_name = 'Contact Category'
        verbose_name_plural = 'Contact Categories'

class ContactUs(models.Model):

    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    phone = models.CharField(max_length=20)
    category = models.ForeignKey(Category,related_name='category_list',verbose_name='category')
    message = models.TextField(max_length=1000)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Contact '
        verbose_name_plural = 'Contacts'