from django import forms
from apps.contact_us.models import ContactUs


class ContactForm(forms.ModelForm):
    """
    Form to display the contact page details.
    """

    phone = forms.RegexField(regex=r'^\+?1?\d{5,15}$',
                             error_message=(
                                 "Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."))

    class Meta:
        model = ContactUs
        fields = ('name', 'email', 'phone', 'message', 'category')

    def clean_name(self):
        return self.cleaned_data['name'].strip()

    def clean_area_of_study(self):
        return self.cleaned_data['area_of_study'].strip()

    def clean_message(self):
        return self.cleaned_data['message'].strip()

    def save(self, commit=True):
        form = super(ContactForm, self).save(commit=False)
        if commit:
            form.save()
        return form
