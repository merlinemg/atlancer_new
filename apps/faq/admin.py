from django.contrib import admin

# Register your models here.
from apps.faq.models import FaqCategory, Faq


admin.site.register(Faq)
admin.site.register(FaqCategory)
