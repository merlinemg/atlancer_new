from django.db import models

# Create your models here.

from django.db import models

class FaqCategory(models.Model):
    faq_category = models.CharField(max_length=100,verbose_name='Faq category')

    def __str__(self):
        return self.faq_category

    class Meta:
        verbose_name = "Faq Category"
        verbose_name_plural = "Faq Categories"

class Faq(models.Model):
    category = models.ForeignKey(FaqCategory,related_name='category',verbose_name='Category')
    question = models.CharField(max_length=200,verbose_name='Question')
    answer = models.TextField(max_length=2000,verbose_name='answer')


    def __str__(self):
        return self.question

    class Meta:
        verbose_name = "Faq"
        verbose_name_plural = "Faqs"
        default_permissions = ('add', 'change', 'delete', 'view')