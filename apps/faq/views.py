import json
from django.contrib.auth.decorators import user_passes_test
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from apps.faq.models import Faq, FaqCategory


class FaqView(TemplateView):


    def get(self, request, *args, **kwargs):

        first = []
        faqs = Faq.objects.all()
        if faqs:
            for first in faqs:
                first = first
                break

        categs = FaqCategory.objects.all()
        return render(request,'faq.html',{'faqs':faqs,'categs_all':categs,'category':first})

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        faqs = Faq.objects.all()
        categs = FaqCategory.objects.all()
        categ = request.POST.get('category')
        category_obj = FaqCategory.objects.get(faq_category=categ)
        sort = Faq.objects.filter(Q(category__exact=category_obj.id))
        return render(request,'faq.html',{'faqs':faqs,'category':category_obj,'categs_all':categs})


class PermissionView(TemplateView):
    template_name = 'permission_error.html'