import json

from django.http import HttpResponse

from apps.freelancer.forms import EducationDetailForm, EmploymentHistoryForm, CertificationForm, HourlyRateForm
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from .models import Language,Skills


def get_language(request):

    if request.is_ajax():
        results = Language.objects.all()
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)