from django.conf.urls import url
from .views import get_language

urlpatterns = [
     url(r'^language/$', get_language, name='language'),
]