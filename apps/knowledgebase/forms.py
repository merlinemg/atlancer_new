from django import forms

from apps.knowledgebase.models import Skills


class SkillForm(forms.ModelForm):
    """
    Form to get skills from user.
    """

    def __init__(self, *args, **kwargs):
        super(SkillForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Skills
