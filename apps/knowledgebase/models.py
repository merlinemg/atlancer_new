from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from django.db import models
from apps.freelancer.models import WorkCategories


class Skills(models.Model):
    """
    Skill model
    """
    work_category = models.ForeignKey('freelancer.WorkCategories', related_name='work_Categories',
                                      verbose_name="Work Sub Categories")
    skill_name = models.CharField(_('Skill name'), max_length=255)

    def __unicode__(self):
        return self.skill_name

    class Meta:
        verbose_name = "Skill"
        verbose_name_plural = "Skills"


class Language(models.Model):
    """
    Language model
    """
    language_title = models.CharField(_('Language title'), max_length=255)

    def __unicode__(self):
        return self.language_title

    class Meta:
        verbose_name = "Language"
        verbose_name_plural = "Languages"


class DegreeCourse(models.Model):
    """
    Degree Course model
    """
    degree_name = models.CharField(_('Degree name'), max_length=255)

    def __unicode__(self):
        return self.degree_name

    class Meta:
        verbose_name = "Degree Course"
        verbose_name_plural = "Degree Courses"
