from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import Skills,DegreeCourse,Language


admin.site.register(Skills)
admin.site.register(DegreeCourse)
admin.site.register(Language)