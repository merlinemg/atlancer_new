import json
from decimal import *

from apps.clients.models import ClientJobs
from apps.general.models import CommissionRate
from apps.knowledgebase.models import Skills
from apps.profile.models import FreelancerProfile
from apps.freelancer.models import EmploymentHistory, EducationDetails, Certifications,\
    FreelancerPortfolio, HourlyRate

from apps.freelancer.models import WorkCategories,WorkSubCategories
from django.db.models import Q
from cities_light.models import Country, Region, City

from apps.freelancer.models import Availability

from apps.freelancer.models import Company


class FreelancerMixin(object):
    """
    Get Company...
    :return:company
    """
    def get_freelancer_search_query_list(self):
        """
        Get Invitation .
        :return: Invitation
        """

        key_words = []
        freelancers = FreelancerProfile.objects.all()
        for skill in Skills.objects.all():
            if skill.skill_name not in key_words:
                key_words.append(skill.skill_name)
        for main_category in WorkCategories.objects.all():
            if main_category.category_title not in key_words:
                key_words.append(main_category.category_title)
        for sub_categories in WorkSubCategories.objects.all():
            if sub_categories.sub_category_title not in key_words:
                key_words.append(sub_categories.sub_category_title)
        for freelancer in freelancers:
            if freelancer.user.first_name not in key_words:
                key_words.append(freelancer.user.first_name)
            if freelancer.professional_title not in key_words:
                    key_words.append(freelancer.professional_title)

        return key_words


    def get_company(self, search):
        company = Company.objects.filter(Q(company_name__icontains=search) | Q(company_name__startswith=search))
        results = []
        for company in company:
            company_json = {}
            company_json['value'] = company.id
            company_json['label'] = company.company_name
            results.append(company_json)
        data = json.dumps(results)
        return data

    def get_user_availability(self):
        """
        Get user availability...
        :return:cities
        """
        try:
            available = Availability.objects.get(user=self.request.user)
        except:
            available= None
        return available
    def get_related_city(self,region):
        """
        Get related cities...
        :return:cities
        """
        city = City.objects.filter(region__name=region)
        return city

    def get_related_regions(self,country):
        """
        Get related regions...
        :return:regions
        """
        regions = Region.objects.filter(country__name=country)
        return regions


    def get_freelancer_profile_by_id(self):
        """
        Get freelancer profile by id...
        :return:freelancer profile
        """
        slug = self.kwargs.get('slug')

        instance = FreelancerProfile.objects.get(slug=slug)
        return instance

    def get_work_sub_categories(self,sub_category):
        """
        Get related sub categories...
        :return:sub categories
        """
        sub_categories = WorkSubCategories.objects.filter(sub_category_title__in=sub_category)
        return sub_categories

    def get_categories_skills(self,skills):
        """
        Get related skills...
        :return:skills
        """
        skills = Skills.objects.filter(id__in=skills)
        return skills

    def get_freelancer_query(self):
        """
        Get all query sets...
        :return:query
        """
        shortlist = self.request.POST.getlist('shortlist')
        categories = self.request.POST.getlist('categories')
        experience_level = self.request.POST.getlist('experience_level')
        amountstartInput = self.request.POST.get('amountstartInput')
        amountendInput = self.request.POST.get('amountendInput')
        query={}
        query['shortlist']=shortlist
        query['categories']=categories
        query['experience_level']=experience_level
        query['amountstartInput']=amountstartInput
        query['amountendInput']=amountendInput

        return query

    def get_work_category(self, action):
        """
        Get work_categories...
        :return:sub_category
        """
        try:
            category = WorkCategories.objects.get(category_title=action)
            sub_category = WorkSubCategories.objects.filter(work_category=category)
        except:
            sub_category=[]
        return sub_category

    def get_category(self, action):
        """
        Get work_categories...
        :return:sub_category
        """
        try:
            category = WorkCategories.objects.get(category_title=action)
        except:
            category=[]

        return category


    def get_work_category_skills(self, action):
        """
        Get work_categories...
        :return:sub_category
        """
        try:
            category = WorkCategories.objects.get(category_title=action)
            skills = Skills.objects.filter(work_category=category)
        except:
            skills=[]
        return skills

    def get_current_certificate(self,id):
        """
        Get certificate details...
        :return:
        """
        details = Certifications.objects.get(pk=id)
        return details

    def get_freelancer_portfolio(self,id):
        """
        Get portfolio details...
        :return:
        """
        details = FreelancerPortfolio.objects.get(pk=id)
        return details

    def get_current_employee_history(self,id):
        """
        Get employee histor details...
        :return:
        """
        details = EmploymentHistory.objects.get(pk=id)
        return details

    def get_education_details(self,id):
        """
        Get educational details...
        :return:
        """
        details = EducationDetails.objects.get(pk=id)
        return details

    def experience_sort(self,experience_level):
        all_freealncers = FreelancerProfile.objects.filter(
                    Q(user__experience_level__exp_level_type__in=experience_level) | Q(user__experience_level__exp_level_type__startswith=experience_level))

        return all_freealncers

    def rate_sort(self,amountstartInput,amountendInput,sort_object):
        all_freealncers = sort_object.filter(user__hourly_rate__hourly_rate__lte=amountendInput).filter(
                        user__hourly_rate__hourly_rate__gte=amountstartInput)
        return all_freealncers

    def category_sort(self,categories):
        all_freealncers = FreelancerProfile.objects.filter(profile_category__category_title__in=categories).distinct()
        return all_freealncers

    def get_actual_rate(self, hourly_rate):
        """
        Get actual rate from hourly rate...
        :return:
        """
        hourly_rate = Decimal(Decimal(hourly_rate))
        commission_rate = CommissionRate.objects.get(freelancer_commission_rate = 10)
        rate = hourly_rate * (commission_rate.freelancer_commission_rate / 100)
        actual_rate = hourly_rate - rate
        return round(actual_rate, 2)

    def get_my_hourly_rate_instance(self):
        """
        getting houly rate...
        :return:houly rate
        """
        try:
            hourly_rate = HourlyRate.objects.get(user=self.request.user)
        except HourlyRate.DoesNotExist:
            hourly_rate = None
        return hourly_rate

    def get_freelancer_profile_instance(self):
        """
        Get freelancer profile instance.
        :return:
        """
        try:
            instance = FreelancerProfile.objects.get(user=self.request.user)
        except FreelancerProfile.DoesNotExist:
            instance = None
        return instance

    def get_freelancer_certificates(self):
        """
        Get certificates...
        :return: freelancer_certificates
        """
        freelancer_certificate = Certifications.objects.filter(user=self.request.user).order_by('-id')
        return freelancer_certificate

    def get_edu_info(self):
        """
        Get educational information...
        :return: employee_eduction
        """
        employee_eduction = EducationDetails.objects.filter(user=self.request.user).order_by('-id')
        return employee_eduction

    def get_employee_history(self):
        """
        Get employee history information...
        :return: employee_history
        """
        employee_history = EmploymentHistory.objects.filter(user=self.request.user).order_by('-id')
        return employee_history

    def get_portfolio(self):
        """
        Get employee portfolio information...
        :return: portfolio
        """
        portfolio = FreelancerPortfolio.objects.filter(user=self.request.user).order_by('-id')
        return portfolio

