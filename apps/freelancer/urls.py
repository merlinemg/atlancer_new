from django.conf.urls import url
from django.views.decorators.gzip import gzip_page

from apps.freelancer.general import check_group_required
from .views import AddFreelancerView, AddEmploymentHistoryView, AddEducationDetailView, AddCertificationView, \
    AddHourlyRateView, EditFreelancerProfileView, AddPortfolioForm, AddCompany, EditOverview, EditEducationDetails, \
    EditEmploymentHistory, EditPortfolio, EditCertificationView, EditProfileView, EditProfilePicView, \
    EditProfileSkillsView, FindFreelancers, WorkCategory, FreelancerDetailView, CountrySelectView, RegionSelectView, \
    ClientActiveView, AddAvailability, AddSkillView

urlpatterns = [
    url(r'^company/add/$', gzip_page(AddCompany.as_view()), name='add_company'),
    # starting of freelancer profile
    url(r'^create-profile/$', gzip_page(check_group_required(AddFreelancerView.as_view())), name='add_freelancer_profile'),
    url(r'^employmentHistory/$', AddEmploymentHistoryView.as_view(), name='add_employmentHistory'),
    url(r'^educationDetails/$', AddEducationDetailView.as_view(), name='add_educationDetails'),
    url(r'^certification/$', gzip_page(AddCertificationView.as_view()), name='add_certification'),
    url(r'^create-rate/$', AddHourlyRateView.as_view(), name='add_rate'),
    url(r'^freelancer-profile/$', gzip_page(EditFreelancerProfileView.as_view()), name='freelancer_profile'),
    url(r'^portfolio/$', AddPortfolioForm.as_view(), name='add_portfolio'),
    url(r'^edit-overview/$', EditOverview.as_view(), name='edit_overview'),
    url(r'^edit-education-details/$', EditEducationDetails.as_view(), name='edit_education_details'),
    url(r'^edit-employment/$', EditEmploymentHistory.as_view(), name='edit_employment'),
    url(r'^edit-portfolio/$', EditPortfolio.as_view(), name='edit_portfolio'),
    url(r'^edit-certificate/$', EditCertificationView.as_view(), name='edit_certificate'),
    url(r'^edit/$', EditProfileView.as_view(), name='edit_profile'),
    url(r'^edit-picture/$', EditProfilePicView.as_view(), name='edit_picture'),
    url(r'^edit-skills/$', EditProfileSkillsView.as_view(), name='edit_skills'),
    url(r'^freelancers-list/$', gzip_page(FindFreelancers.as_view()), name='find_freelancer'),
    url(r'^working-category/$', gzip_page(WorkCategory.as_view()), name='working_category'),
    url(r'^freelancer-detail/(?P<slug>[\w-]+)/$', FreelancerDetailView.as_view(), name='freelancer_detail'),
    url(r'^country/$', gzip_page(CountrySelectView.as_view()), name='country_select'),
    url(r'^region/$', RegionSelectView.as_view(), name='region_select'),
    url(r'^availability/$', AddAvailability.as_view(), name='add_availability'),
    url(r'^skill/$', gzip_page(AddSkillView.as_view()), name='skill_add'),
    # end of freelancer profile

    url(r'^freelancer-as-client/', gzip_page(ClientActiveView.as_view()), name='client_active'),
]
