from django.core.exceptions import PermissionDenied

from apps.profile.models import FreelancerProfile
from django.http import HttpResponsePermanentRedirect

profile_creation_url = '/create-profile/'
profile_url = "/freelancer-profile/"


class ProfileRedirectionMiddleware(object):

    def process_request(self, request, *args, **kwargs):
        if request.path == profile_creation_url:
            try:
                is_freelancer = FreelancerProfile.objects.get(user=request.user)
                if is_freelancer:
                    is_filled = self.is_freelancer(is_freelancer,request)
                    if is_filled == True:
                        return HttpResponsePermanentRedirect(profile_url)

            except:
                return None

    def is_freelancer(self, is_freelancer,request):

        try:
            if is_freelancer.professional_title and is_freelancer.skills.all and is_freelancer.profile_category and \
                    is_freelancer.sub_category.all and is_freelancer.user.experience_level and is_freelancer.user.hourly_rate:
                return True
            print "Trueeeee"
        except:
            request.is_all_filled = "False"
            return False


class CreationRedirectMiddleware(object):

    def process_request(self, request, *args, **kwargs):
        if request.path == profile_url:
            try:
                is_freelancer = FreelancerProfile.objects.get(user=request.user)
                if is_freelancer:
                    is_filled = self.is_freelancer(is_freelancer)
                    if is_filled == True:
                        return None

            except:
                request.is_all_filled = "False"
                return HttpResponsePermanentRedirect(profile_creation_url)


    def is_freelancer(self, is_freelancer):
        if is_freelancer.professional_title and is_freelancer.skills.all and is_freelancer.profile_category and \
                is_freelancer.sub_category.all and is_freelancer.user.experience_level \
                and is_freelancer.user.hourly_rate:
            return True

        else:
            return False
