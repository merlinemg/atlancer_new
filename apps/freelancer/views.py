import json

from django.template.response import TemplateResponse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic.edit import FormView
from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.clients.mixins import ClientMixin
from apps.freelancer.general import check_group_required, my_decorator
from new_djangoteam import settings
from apps.freelancer.models import EmploymentHistory, EducationDetails, Certifications, FreelancerPortfolio, \
    Availability
from apps.knowledgebase.models import Skills, Language
from apps.profile.models import FreelancerProfile
from apps.freelancer.forms import EducationDetailForm, EmploymentHistoryForm, CertificationForm, HourlyRateForm, \
    ExperienceLevelForm, FreelancerPortfolioForm, EditEducationDetailsForm, EditEmploymentHistoryForm, \
    EditCertificationForm, EditPortfolioForm, EditLanguageForm
from apps.profile.forms import FreelancerProfileForm, FreelancerOverviewEdit, EditFreelancerProfileForm, \
    EditFreelancerPicForm, EditFreelancerSkillsForm
from apps.freelancer.forms import CompanyForm
from apps.freelancer.models import Company, WorkCategories, WorkSubCategories, HourlyRate, ExperienceLevel
from apps.freelancer.mixins import FreelancerMixin
from apps.general.utils import UtilMixin
from apps.profile.forms import ClientActiveProfileForm
from apps.clients.models import JobStatus
from apps.freelancer.forms import AvalabilityForm, SkillSForm


class AddSkillView(FreelancerMixin, TemplateView):
    form_class = SkillSForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddSkillView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddSkillView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        status = request.POST['status']
        print "****status", status
        print request.POST
        if status == 'open':
            print "*****************"
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_add_skills.html',
                                                             {'status': status})
        if status == 'create':
            main_category = request.POST['main_category']
            print "*******main_category", main_category
            main_category_id = self.get_category(main_category)
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save(main_category_id)
                response_data['status'] = True
                skills = self.get_work_category_skills(main_category)
                print skills
                response_data['html_content'] = render_to_string('profile/ajax/freelancer_add_skills.html',
                                                                 {'skills': skills.order_by('-pk'), 'status': status})

            else:
                response_data['status'] = False
                response_data['errors'] = form.errors
                print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class ClientActiveView(FormView):
    template_name = 'freelancer-client.html'
    form_class = ClientActiveProfileForm

    def form_valid(self, form):
        form.save(self.request.user)
        return redirect('client_profile')


class AddCompany(FreelancerMixin, TemplateView):
    """
    View to add company and to search company name
    """
    form_class = CompanyForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(AddCompany, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddCompany, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        return context

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            search_value = request.GET.get('keyword_value')
            search = str(search_value)
            companies = self.get_company(search.strip())
            response_data['companies'] = companies
        else:
            response_data['status'] = False
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        response_data['status'] = True
        form = self.form_class(request.POST)
        if form.is_valid() and request.is_ajax():
            form.save()
            response_data['status'] = True
            response_data['success'] = " successfully sent."
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddFreelancerView(FreelancerMixin, UtilMixin, TemplateView):
    """
    view to add freelancer profile and display employee history form , education detail form,
    certification form and hourly rate form.
    """
    form_class = FreelancerProfileForm
    employment_history_form = EmploymentHistoryForm
    education_detail_form = EducationDetailForm
    certification_form = CertificationForm
    hourly_rate_form = HourlyRateForm
    experience_level_form = ExperienceLevelForm
    freelancer_portfolio_form = FreelancerPortfolioForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    @method_decorator(my_decorator('add_freelancerprofile'))
    def dispatch(self, *args, **kwargs):
        return super(AddFreelancerView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Collect all context to template...
        :param kwargs: extra params
        :return: context
        """
        context = super(AddFreelancerView, self).get_context_data(**kwargs)
        freelancer_profile_instance = self.get_freelancer_profile_instance()
        if freelancer_profile_instance:
            context['form_profile'] = self.form_class(instance=freelancer_profile_instance)
            context['freelancer_current'] = freelancer_profile_instance
        else:
            context['form_profile'] = self.form_class()
        context['form_employment'] = self.employment_history_form()
        context['form_education'] = self.education_detail_form()
        context['form_certifications'] = self.certification_form()
        hourly_rate_instance = self.get_my_hourly_rate_instance()
        if hourly_rate_instance:
            context['form_Rate'] = HourlyRateForm(instance=hourly_rate_instance)
        else:
            context['form_Rate'] = HourlyRateForm()
        context['form_portfolio'] = self.freelancer_portfolio_form()
        context['employee_history'] = self.get_employee_history()
        context['employee_eduction'] = self.get_edu_info()
        context['freelancer_certificate'] = self.get_freelancer_certificates()
        context['portfolio'] = self.get_portfolio()
        # context['skills'] = self.get_all_skills()
        context['languages'] = self.get_languages()
        context['main_category'] = self.get_work_categories()
        context['sub_category'] = self.get_all_work_sub_categories()
        context['countries'] = self.get_countries()
        context['regions'] = self.get_regions()
        context['cities'] = self.get_cities()
        if freelancer_profile_instance:
            try:
                category = freelancer_profile_instance.profile_category.category_title
                if freelancer_profile_instance.skills.all():
                    for skill in freelancer_profile_instance.skills.all():
                        context['skills'] = self.get_work_category_skills(
                            category).exclude(skill_name=skill.skill_name)
                else:
                    context['skills'] = []
            except:
                context['skills'] = []

            if freelancer_profile_instance.sub_category.all():
                for sub_category in freelancer_profile_instance.sub_category.all():
                    context['sub_categories'] = self.get_work_category(category).exclude(
                        sub_category_title=sub_category.sub_category_title)
            else:
                context['sub_categories'] = []
        if self.get_user_availability():
            context['avalabile_form'] = AvalabilityForm(instance=self.get_user_availability())
        else:
            context['avalabile_form'] = AvalabilityForm()
        context['user_available'] = self.get_user_availability()
        return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        freelancer_profile_instance = self.get_freelancer_profile_instance()
        if freelancer_profile_instance:
            form = self.form_class(request.POST, request.FILES, instance=freelancer_profile_instance)
        else:
            freelancer_profile_instance = None
            form = self.form_class(request.POST, request.FILES)
        level = request.POST['level']
        language_title_list = request.POST.getlist('languages')
        skill_name_list = request.POST.getlist('skills')
        country = request.POST.get('country')
        region = request.POST.get('region')
        city = request.POST.get('city')
        if form.is_valid() and level == "first":
            if freelancer_profile_instance:
                for skill in freelancer_profile_instance.skills.all():
                    freelancer_profile_instance.skills.remove(skill)
                for languages in freelancer_profile_instance.language_proficiency.all():
                    freelancer_profile_instance.language_proficiency.remove(languages)
            form.save(user, self.get_skills_by_skill_name_list(skill_name_list),
                      self.get_language_by_language_title_list(language_title_list), self.get_country_by_name(country),
                      self.get_region_by_name(region),
                      self.get_city_by_name(city))
            response_data['status'] = True
            response_data['success'] = " successfully sent."
        elif request.is_ajax() and level == "second":
            exp_level = request.POST.get('exp_level_type')
            self.update_experience_level(exp_level)
            response_data['status'] = True
        elif request.is_ajax() and level == "finish":
            response_data['available'] = True
            response_data['freelancer'] = True
            response_data['category'] = True
            response_data['experience'] = True
            response_data['rate'] = True
            response_data['status'] = True
            try:
                freelancer = FreelancerProfile.objects.get(user=request.user)
                if freelancer.profile_category is None:
                    response_data['category'] = False
                    response_data['status'] = False
            except:
                response_data['freelancer'] = False
                response_data['status'] = False
            # try:
            #     available = Availability.objects.get(user=request.user)
            # except:
            #     response_data['available'] = False
            #     response_data['status'] = False
            try:
                experience = ExperienceLevel.objects.get(user=request.user)
            except:
                response_data['experience'] = False
                response_data['status'] = False
            try:
                rate = HourlyRate.objects.get(user=request.user)
            except:
                response_data['rate'] = False
                response_data['status'] = False
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def update_experience_level(self, exp_level):
        """

        :param exp_level:
        :return:
        """
        try:
            exp_instance = ExperienceLevel.objects.get(user=self.request.user)
            exp_instance.delete()
        except:
            exp_instance = None

        experience_form = self.experience_level_form(self.request.POST)
        experience_form.save(self.request.user)
        return exp_level


class AddEmploymentHistoryView(FreelancerMixin, TemplateView):
    """
    View to add freelancer employment history
    """
    form_class = EmploymentHistoryForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddEmploymentHistoryView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        context = super(AddEmploymentHistoryView, self).get_context_data(**kwargs)
        context['form_employment'] = self.form_class()
        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            action = request.GET['employment_profile']
            response_data = {}
            if action == 'update_history':
                id = request.GET['history_id']
                employment = self.get_current_employee_history(id)
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/emp_history.html',
                                                                 {'form_employment': self.form_class(
                                                                     instance=employment),
                                                                     'company_name': employment.company.company_name})
            elif action == 'add_history':
                print "1111"
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/emp_history.html',
                                                                 {'form_employment': self.form_class()})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        employment = request.POST['employment_profile']
        response_data = {}
        form = self.form_class(request.POST)
        if form.is_valid() and employment == 'create':
            action = request.POST['employment_action']
            name = request.POST['company_name']
            country_id = request.POST['code']
            form.save(user, country_id, name)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            response_data['html_content'] = render_to_string('profile/ajax/employee_work_history.html',
                                                             {'employee_history': self.get_employee_history()})

        elif employment == 'update':
            id = request.POST['employment_id']
            employment = self.get_current_employee_history(id)
            form = self.form_class(request.POST, instance=employment)
            if form.is_valid():
                name = request.POST['company_name']
                country_id = request.POST['code']
                form.save(user, country_id, name)
                response_data['status'] = True
                response_data['success'] = " successfully sent."
                response_data['html_content'] = render_to_string('profile/ajax/employee_work_history.html',
                                                                 {'employee_history': self.get_employee_history()})
            else:
                response_data['status'] = False
                response_data['errors'] = form.errors
                print form.errors.as_text()
        elif form.is_valid() and employment == 'profile':
            name = request.POST['company_name']
            country_id = request.POST['code']
            form.save(user, country_id, name)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/employment_profile.html',
                                                             {
                                                                 'freelancer_employee_history': self.get_employee_history()})
        elif employment == 'delete':
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/employment_profile.html',
                                                             {
                                                                 'freelancer_employee_history': self.get_employee_history()})
        elif employment == 'employment_add_form':
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/employment_add_form.html',
                                                             {'form_employment': self.form_class()})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddEducationDetailView(FreelancerMixin, TemplateView):
    """
    View to add Education detail of freelancer
    """
    form_class = EducationDetailForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddEducationDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        method to display education form  details
        :param kwargs:
        :return:
        """
        context = super(AddEducationDetailView, self).get_context_data(**kwargs)
        context['form_education'] = self.form_class()
        return context

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = request.GET['employment_profile']
            response_data = {}
            if action == 'update_education':
                id = request.GET['edu_id']
                education_instance = self.get_education_details(id)
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/education_detail.html',
                                                                 {'form_education': self.form_class(
                                                                     instance=education_instance)})
            elif action == 'add_education':
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/education_detail.html',
                                                                 {'form_education': self.form_class()})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        """
        Post request take data from create profile and my profile of freelancer.
        This method add education details and also perform delete .
        :param request:
        :param args:
        :param kwargs:
        :return:It returns a table in the html page.
        """
        user = request.user
        response_data = {}
        form = self.form_class(request.POST)
        profile = request.POST['education_profile']
        if form.is_valid() and profile == 'create':
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            response_data['html_content'] = render_to_string('profile/ajax/employee_education.html',
                                                             {'employee_eduction': self.get_edu_info()})
        elif form.is_valid() and profile == 'profile':
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            response_data['html_content'] = render_to_string('profile/ajax/education_profile.html',
                                                             {'freelancer_education_details': self.get_edu_info()})
        elif form.is_valid() and profile == 'update':
            id = request.POST['education_id']
            education = self.get_education_details(id)
            form_update = self.form_class(request.POST, instance=education)
            form_update.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            response_data['html_content'] = render_to_string('profile/ajax/employee_education.html',
                                                             {'employee_eduction': self.get_edu_info()})

        elif profile == "delete":
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/education_profile.html',
                                                             {'freelancer_education_details': self.get_edu_info()})
        elif profile == 'education_add_form':
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/education_add_form.html',
                                                             {'form_education': self.form_class()})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddCertificationView(FreelancerMixin, TemplateView):
    """
     View to add certification details of freelancer
    """

    form_class = CertificationForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddCertificationView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddCertificationView, self).get_context_data(**kwargs)
        context['form_certifications'] = self.form_class()
        return context

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = request.GET['employment_profile']
            response_data = {}
            if action == 'update_certification':
                id = request.GET['cer_id']
                certificate_instance = self.get_current_certificate(id)
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/certification_form.html',
                                                                 {'form_certifications': self.form_class(
                                                                     instance=certificate_instance),
                                                                     'certificate_instance': certificate_instance})
            elif action == 'add_certification':
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/certification_form.html',
                                                                 {'form_certifications': self.form_class()})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        form = self.form_class(request.POST, request.FILES)
        certificate = request.POST['certificate_profile']
        if form.is_valid() and certificate == 'create':
            form.save(user)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_certification.html',
                                                             {
                                                                 'freelancer_certificate': self.get_freelancer_certificates()})
        elif form.is_valid() and certificate == 'profile':
            form.save(user)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/certificates_profile.html',
                                                             {
                                                                 'freelancer_certification': self.get_freelancer_certificates()})
        elif certificate == 'delete':
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/certificates_profile.html',
                                                             {
                                                                 'freelancer_certification': self.get_freelancer_certificates()})
        elif certificate == 'update':
            id = request.POST.get('certification_id')
            certification = self.get_current_certificate(id)
            form_update = EditCertificationForm(request.POST, request.FILES, instance=certification)
            if form_update.is_valid():
                form_update.save(request.user)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/freelancer_certification.html',
                                                                 {
                                                                     'freelancer_certificate': self.get_freelancer_certificates()})
            else:
                response_data['status'] = False
                response_data['errors'] = form_update.errors
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddHourlyRateView(FreelancerMixin, TemplateView):
    """
    View to add hourly rate of freelancer
.    """
    form_class = HourlyRateForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddHourlyRateView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        hourly_rate_instance = self.get_my_hourly_rate_instance()
        response_data = {}
        context = {}
        if hourly_rate_instance:
            context['form_Rate'] = self.form_class(instance=hourly_rate_instance)
        else:
            context['form_Rate'] = self.form_class()
        if request.GET['data'] == 'open':
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_ProfileRate.html',
                                                             {'form_Rate': context['form_Rate'],
                                                              'type': request.GET['data']})
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        return context

    # def get_context_data(self, **kwargs):
    #     context = super(AddHourlyRateView, self).get_context_data(**kwargs)
    #     hourly_rate_instance = self.get_my_hourly_rate_instance()
    #     if hourly_rate_instance:
    #         context['form_Rate'] = self.form_class(instance=hourly_rate_instance)
    #     else:
    #         context['form_Rate'] = self.form_class()
    #     return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        hourly_rate_instance = self.get_my_hourly_rate_instance()
        if hourly_rate_instance:
            form = self.form_class(request.POST, instance=hourly_rate_instance)
        else:
            form = self.form_class(request.POST)
        if request.is_ajax():
            if request.POST.get('level') == "actual_rate_update":
                hourly_rate = request.POST.get('hourly_rate')
                print "hourly_rate", type(hourly_rate.encode('utf-8')), hourly_rate
                response_data['actual_rate'] = str(self.get_actual_rate(hourly_rate.encode('utf-8')))
                response_data['status'] = True
            else:
                if form.is_valid():
                    print "*****"
                    rate = form.save(user)
                    response_data['status'] = True
                    response_data['success'] = " successfully sent."
                    response_data['html_content'] = render_to_string('profile/ajax/freelancer_ProfileRate.html',
                                                                     {'rate': rate})
                else:
                    response_data['status'] = False
                    response_data['errors'] = form.errors
                    print form.errors.as_text()
            return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddPortfolioForm(FreelancerMixin, UtilMixin, TemplateView):
    """
    View to add freelancer portfolio
    """
    form_class = FreelancerPortfolioForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddPortfolioForm, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddPortfolioForm, self).get_context_data(**kwargs)
        context['form_portfolio'] = self.form_class()
        return context

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = request.GET['employment_profile']
            response_data = {}
            if action == 'update_portfolio':
                id = request.GET['portfolio_id']
                portfolio_instance = self.get_freelancer_portfolio(id)
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/portfolio_form.html',
                                                                 {'form_portfolio': self.form_class(
                                                                     instance=portfolio_instance),
                                                                     'portfolio_instance': portfolio_instance})
            elif action == 'add_portfolio':
                response_data['html_content'] = render_to_string('profile/ajax/Create_profile/portfolio_form.html',
                                                                 {'form_portfolio': self.form_class()})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        form = self.form_class(request.POST)
        portfolio = request.POST['portfolio_profile']
        if form.is_valid() and portfolio == 'create':

            form.save(user, self.get_skills_by_skill_id_list(request.POST.getlist('skills')),
                      self.get_work_sub_categories(request.POST.getlist('sub_category')))
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_portfolio.html',
                                                             {'portfolio': self.get_portfolio()})
            response_data['status'] = True

        elif form.is_valid() and portfolio == 'update':
            id = request.POST.get('portfolio_id')
            portfolio_instance = self.get_freelancer_portfolio(id)
            form_update = EditPortfolioForm(request.POST, instance=portfolio_instance)
            form_update.save(user, self.get_skills_by_skill_id_list(request.POST.getlist('skills')),
                             self.get_work_sub_categories(request.POST.getlist('sub_category')))
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_portfolio.html',
                                                             {'portfolio': self.get_portfolio()})
            response_data['status'] = True

        elif form.is_valid() and portfolio == 'profile':
            form.save(user, self.get_skills_by_skill_name_list(request.POST.getlist('skills')),
                      self.get_work_sub_categories(request.POST.getlist('sub_category')))
            response_data['html_content'] = render_to_string('profile/ajax/portfolio_profile.html',
                                                             {'freelancer_portfolio': self.get_portfolio()})
            response_data['status'] = True
        elif portfolio == 'delete':
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/portfolio_profile.html',
                                                             {'freelancer_portfolio': self.get_portfolio()})

        elif portfolio == 'portfolio_add_form':
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/portfolio_add_form.html',
                                                             {'form_portfolio': self.form_class()})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddAvailability(FreelancerMixin, UtilMixin, TemplateView):
    """
    View to add availability of freelancer
    """

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddAvailability, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            return_data = {}
            is_availability = self.get_user_availability()
            data = request.GET.get('data')
            step = request.GET.get('step')
            if is_availability:
                form = AvalabilityForm(instance=is_availability)
            else:
                form = AvalabilityForm(request.POST)
            return_data['html_content'] = render_to_string('profile/ajax/profile_availability_sections.html',
                                                           {'user_available': self.get_user_availability(),
                                                            'new_form': AvalabilityForm(request.POST), 'form': form,
                                                            'data': data, 'step': step})

            return HttpResponse(json.dumps(return_data), content_type="application/json")

    def post(self, request, *args, **kwargs):

        if request.is_ajax():
            return_data = {}
            status = request.POST.get('status')
            period = request.POST.get('period')
            language_title_list = request.POST.getlist('language_proficiency')
            freelancer_profile_instance = self.get_freelancer_profile_instance()
            for language in language_title_list:
                try:
                    Language.objects.get(language_title=language)
                except:
                    p = Language(language_title=language)
                    p.save()
            languages = self.get_language_by_language_title_list(language_title_list)
            is_availability = self.get_user_availability()
            if is_availability:
                form = AvalabilityForm(request.POST, instance=is_availability)
            else:
                form = AvalabilityForm(request.POST)
            if form.is_valid():
                print "*************"
                form.save(request.user, status, period)
                return_data['status'] = True
            else:
                return_data['status'] = False
                return_data['errors'] = form.errors

            if language_title_list:
                for language in freelancer_profile_instance.language_proficiency.all():
                    freelancer_profile_instance.language_proficiency.remove(language)
                try:
                    freelancer_profile_instance.language_proficiency.add(language_title_list)
                except:
                    for language in languages:
                        freelancer_profile_instance.language_proficiency.add(language)
                return_data['status'] = True
            return_data['html_content'] = render_to_string('profile/ajax/profile_availability.html',
                                                           {'user_available': self.get_user_availability(),'request':request})
            return HttpResponse(json.dumps(return_data), content_type="application/json")


class EditFreelancerProfileView(FreelancerMixin,UtilMixin, TemplateView):
    """
    View to add hourly rate of freelancer
    """
    profile_class = FreelancerProfileForm
    employment_class = EmploymentHistoryForm
    education_class = EducationDetailForm
    certification_class = CertificationForm
    rate_class = HourlyRateForm
    portfolio_class = FreelancerPortfolioForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    @method_decorator(my_decorator('change_freelancerprofile'))
    @method_decorator(my_decorator('view_freelancerprofile'))
    def dispatch(self, *args, **kwargs):
        return super(EditFreelancerProfileView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditFreelancerProfileView, self).get_context_data(**kwargs)
        context['form_profile'] = self.profile_class()
        context['form_employment'] = self.employment_class()
        context['form_education'] = self.education_class()
        context['form_certifications'] = self.certification_class()
        context['form_Rate'] = self.rate_class()
        context['form_portfolio'] = self.portfolio_class()
        freelancer_employee_history = EmploymentHistory.objects.filter(user=self.request.user)
        freelancer_profile, new_freelancer = FreelancerProfile.objects.get_or_create(user=self.request.user)
        freelancer_education_details = EducationDetails.objects.filter(user=self.request.user)
        freelancer_certifiaction = Certifications.objects.filter(user=self.request.user)
        freelancer_portfolio = FreelancerPortfolio.objects.filter(user=self.request.user)
        work_categories = WorkCategories.objects.all()
        # skills = Skills.objects.all()
        context['freelancer_employee_history'] = freelancer_employee_history
        context['freelancer_profile'] = freelancer_profile
        context['freelancer_education_details'] = freelancer_education_details
        context['freelancer_certification'] = freelancer_certifiaction
        context['freelancer_portfolio'] = freelancer_portfolio
        context['work_categories'] = work_categories
        all_languages = self.get_languages()
        # context['skills'] = skills
        for language in self.request.user.freelancer_profile.language_proficiency.all():
            all_languages = all_languages.exclude(language_title=language.language_title)
        context['languages'] = all_languages
        context['profile'] = self.profile_class(instance=freelancer_profile)
        if_available = self.get_user_availability()
        hourly_rate_instance = self.get_my_hourly_rate_instance()
        if if_available:
            context['avalabile_form'] = AvalabilityForm(instance=if_available)
        else:
            context['avalabile_form'] = AvalabilityForm()
        context['user_available'] = if_available
        if hourly_rate_instance:
            context['form_Rate'] = HourlyRateForm(instance=hourly_rate_instance)
        else:
            context['form_Rate'] = HourlyRateForm()
        freelancer_instance = self.get_freelancer_profile_instance()
        if freelancer_instance.about_me:
            overview_form = FreelancerOverviewEdit(instance=freelancer_instance)
        else:
            overview_form = FreelancerOverviewEdit
        context['overview_form']=overview_form
        return context


class EditOverview(FreelancerMixin, UpdateView):
    """
     View to edit freelancer overview
    """
    model = FreelancerProfile
    template_name = "profile/freelancer_profile.html"
    form_class = FreelancerOverviewEdit

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditOverview, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        response_data = {}
        action = request.GET['action']
        if request.is_ajax() and action == 'open':
            profile = self.get_freelancer_profile_instance()
            overview_form = self.form_class(instance=profile)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/overview.html',
                                                             {'form': overview_form, 'action': action})
        if request.is_ajax() and action == 'close':
            profile = self.get_freelancer_profile_instance()
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/overview.html',
                                                             {'form': profile, 'action': action})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        form = self.form_class(request.POST, instance=request.user.freelancer_profile)
        if request.is_ajax() and form.is_valid():
            print request.POST.get('about_me')
            form.save(request.user)
            action = 'close'
            profile = self.get_freelancer_profile_instance()
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/overview.html',
                                                             {'form': profile, 'action': action})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditEducationDetails(FreelancerMixin, UpdateView):
    """
    view to update education details
    """
    model = EducationDetails
    form_class = EditEducationDetailsForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditEducationDetails, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            id = request.GET['id']
            action = request.GET['action']
            response_data = {}
            education = self.get_education_details(id)
            if action == "open":
                freelancer = self.form_class(instance=education)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/edit_education.html',
                                                                 {'freelancer': freelancer, 'edit_id': id,
                                                                  'action': action})
            elif action == "close":
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/edit_education.html',
                                                                 {'freelancer': education, 'edit_id': id,
                                                                  'action': action})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        id = request.POST['id']
        education = self.get_education_details(id)
        form = self.form_class(request.POST, instance=education)
        if request.is_ajax() and form.is_valid():
            user = request.user
            form.save(user)
            response_data['status'] = True
            education = self.get_education_details(id)
            response_data['html_content'] = render_to_string('profile/ajax/edit_education.html',
                                                             {'freelancer': education, 'edit_id': id,
                                                              'action': 'close'})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditEmploymentHistory(FreelancerMixin, UpdateView):
    """
    view to update education details
    """
    model = EmploymentHistory
    form_class = EditEmploymentHistoryForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditEmploymentHistory, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            id = request.GET['id']
            action = request.GET['action']
            response_data = {}
            employment = self.get_current_employee_history(id)
            if action == "open":
                employment_history = self.form_class(instance=employment)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/edit_employment.html',
                                                                 {'employment_history': employment_history,
                                                                  'edit_id': id})
            elif action == "close":
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/close_employment.html',
                                                                 {'employment_history': employment, 'edit_id': id})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        id = request.POST['id']
        employment = self.get_current_employee_history(id)
        form = self.form_class(request.POST, instance=employment)
        if request.is_ajax() and form.is_valid():
            form.save(request.user)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/close_employment.html',
                                                             {'employment_history': employment, 'edit_id': id})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditPortfolio(FreelancerMixin, UtilMixin, UpdateView):
    """
    view to update education details
    """
    model = FreelancerPortfolio
    form_class = EditPortfolioForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditPortfolio, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            id = request.GET['id']
            action = request.GET['action']
            response_data = {}
            portfolio = self.get_freelancer_portfolio(id)
            skills = self.get_all_skills()
            for skill in portfolio.skills.all():
                skills = skills.exclude(skill_name=skill.skill_name)
            if action == "open":
                portfolio = self.form_class(instance=portfolio)
                port = self.get_freelancer_portfolio(id)
                category = WorkCategories.objects.all()
                response_data['html_content'] = render_to_string('profile/ajax/edit_portfolio.html',
                                                                 {'portfolio': portfolio, 'edit_id': id,
                                                                  'action': action,
                                                                  'skills': skills, 'port': port, 'category': category})
            elif action == "close":
                response_data['html_content'] = render_to_string('profile/ajax/edit_portfolio.html',
                                                                 {'portfolio': portfolio, 'edit_id': id,
                                                                  'action': action})
            response_data['status'] = True
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        id = request.POST['id']
        portfolio = self.get_freelancer_portfolio(id)
        print "request.POST", request.POST
        form = self.form_class(request.POST, instance=portfolio)
        if request.is_ajax() and form.is_valid():
            skills_obj = request.POST.getlist('skills')
            sub_category = self.get_work_sub_categories(request.POST.getlist('sub_category'))
            skills = self.get_skills_by_skill_name_list(skills_obj)
            form.save(request.user, skills, sub_category)
            portfolio = self.get_freelancer_portfolio(id)
            action = 'close'
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/edit_portfolio.html',
                                                             {'portfolio': portfolio, 'edit_id': id, 'action': action})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditCertificationView(FreelancerMixin, UpdateView):
    """
    view to update education details
    """
    model = Certifications
    form_class = EditCertificationForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditCertificationView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            id = request.GET['id']
            action = request.GET['action']
            response_data = {}
            certificate = self.get_current_certificate(id)
            if action == "open":
                certificate = self.form_class(instance=certificate)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/edit_certification.html',
                                                                 {'certification': certificate, 'edit_id': id,
                                                                  'action': action})
            elif action == "close":
                response_data['status'] = True
                response_data['html_content'] = render_to_string('profile/ajax/edit_certification.html',
                                                                 {'certification': certificate, 'edit_id': id,
                                                                  'action': action})
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        id = request.POST['id']
        certificate = self.get_current_certificate(id)
        form = self.form_class(request.POST, request.FILES, instance=certificate)
        if request.is_ajax() and form.is_valid():
            action = 'close'
            user = request.user
            form.save(user)
            response_data['status'] = True
            certificate = self.get_current_certificate(id)
            response_data['html_content'] = render_to_string('profile/ajax/edit_certification.html',
                                                             {'certification': certificate, 'edit_id': id,
                                                              'action': action})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditProfileView(UtilMixin, FreelancerMixin, UpdateView):
    """
    view to update Freelancer profile
    """

    model = FreelancerProfile
    form_class = EditFreelancerProfileForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditProfileView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        """
        In this method the the form editing freelancer is open and close code is written.
        From ajax action variable is assigned which gives idea that the form should open or close
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        response_data = {}
        if request.is_ajax():
            action = request.GET['action']
            user = request.user
            profile = self.get_freelancer_profile()
            skills = self.get_all_skills()
            languages = self.get_languages()
            if action == "open":
                profile = self.form_class(instance=profile)
                response_data['html_content'] = render_to_string('profile/ajax/edit_profile_details.html',
                                                                 {'freelancer_profile': profile, 'action': action,
                                                                  'user': user, 'skills': skills,
                                                                  'languages': languages})
            elif action == "close":
                response_data['html_content'] = render_to_string('profile/ajax/edit_profile_details.html',
                                                                 {'freelancer_profile': profile, 'action': action,
                                                                  'user': user, 'skills': skills,
                                                                  'languages': languages})
            response_data['status'] = True
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = 'close'
            profile = self.get_freelancer_profile_instance()
            form = self.form_class(request.POST, request.FILES, instance=profile)
            skills_obj = request.POST.getlist('skills')
            skills = self.get_skills_by_skill_name_list(skills_obj)
            languages_obj = request.POST.getlist('languages')
            languages = self.get_language_by_language_title_list(languages_obj)
            if form.is_valid():
                form.save(request.user, skills, languages)
            else:
                print form.errors.as_text()
            response_data['status'] = True
            profile = self.get_freelancer_profile()
            skills = self.get_all_skills()
            languages = self.get_languages()
            response_data['html_content'] = render_to_string('profile/ajax/edit_profile_details.html',
                                                             {'freelancer_profile': profile, 'action': action,
                                                              'user': request.user, 'skills': skills,
                                                              'languages': languages})
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditProfilePicView(FreelancerMixin, ClientMixin, UpdateView):
    """
    view to update Freelancer profile picture
    """

    model = FreelancerProfile
    form_class = EditFreelancerPicForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditProfilePicView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = request.GET['action']
            user = request.user
            profile = self.get_freelancer_profile_instance()
            if action == "open":
                profile = self.form_class(instance=profile)
                countries = self.get_all_countries()
                regions = None
                cities = None
                try:
                    regions = self.get_related_regions(user.freelancer_profile.country.name)
                    cities = self.get_related_city(user.freelancer_profile.region.name)
                except:
                    pass
                response_data['html_content'] = render_to_string('profile/ajax/profile_picture.html',
                                                                 {'freelancer_profile': profile, 'countries': countries,
                                                                'regions': regions, 'cities': cities, 'action': action,
                                                                  'user': user})
            elif action == "close":
                response_data['html_content'] = render_to_string('profile/ajax/profile_picture.html',
                                                                 {'freelancer_profile': profile, 'action': action,
                                                                  'user': user})
            response_data['status'] = True
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        profile = self.get_freelancer_profile_instance()
        form = self.form_class(request.POST, request.FILES, instance=profile)
        address = self.get_address()
        country = address.get('country')
        region = address.get('region')
        city = address.get('city')

        if request.is_ajax() and form.is_valid():
            action = 'close'
            form.save(request.user,country, region, city)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/profile_picture.html',
                                                             {'freelancer_profile': profile, 'action': action,
                                                              'user': request.user})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditProfileSkillsView(FreelancerMixin, UtilMixin, UpdateView):
    """
    view to update Freelancer profile picture
    """

    model = FreelancerProfile
    form_class = EditFreelancerSkillsForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditProfileSkillsView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = request.GET['action']
            user = request.user
            profile = self.get_freelancer_profile_instance()
            if action == "open":
                category = request.user.freelancer_profile.profile_category.category_title
                skills = self.get_work_category_skills(category)
                for skill in profile.skills.all():
                    skills = skills.exclude(skill_name=skill.skill_name)
                profile = self.form_class(instance=profile)
                response_data['html_content'] = render_to_string('profile/ajax/freelancer_skills.html',
                                                                 {'freelancer_profile': profile, 'action': action,
                                                                  'user': user, 'skills': skills})
            elif action == "close":
                response_data['html_content'] = render_to_string('profile/ajax/freelancer_skills.html',
                                                                 {'freelancer_profile': profile, 'action': action,
                                                                  'user': user})
            response_data['status'] = True
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        profile = self.get_freelancer_profile_instance()
        form = self.form_class(request.POST, instance=profile)
        if request.is_ajax() and form.is_valid():
            action = 'close'
            skills_obj = request.POST.getlist('skills_profile')
            for skill in skills_obj:
                try:
                    Skills.objects.get(skill_name=skill)
                except:
                    p = Skills(skill_name=skill, work_category=request.user.freelancer_profile.profile_category)
                    p.save()
            skills = self.get_skills_by_skill_name_list(skills_obj)
            form.save(request.user, skills)
            response_data['status'] = True
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_skills.html',
                                                             {'freelancer_profile': profile, 'action': action,
                                                              'user': request.user})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class FindFreelancers(FreelancerMixin, UtilMixin, TemplateView):
    def dispatch(self, *args, **kwargs):
        return super(FindFreelancers, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        template_name = "profile/find_freelancers.html"
        all_freelancers = self.get_all_freelancers()
        if request.user.is_authenticated():
            freelancer = self.get_freelancer_profile_instance()
            if freelancer:
                all_freelancers = all_freelancers.exclude(user=request.user)
        search = request.GET.get('search_type')
        if search:
            search = search.split(",")
            print search
            all_freelancers = all_freelancers.filter(
                Q(user__first_name__icontains=search) | Q(user__first_name__in=search) | Q(
                    professional_title__icontains=search) | Q(professional_title__in=search) | Q(
                    about_me__icontains=search) | Q(about_me__in=search) | Q(skills__skill_name__in=search) | Q(
                    skills__skill_name__contains=search) | Q(
                    skills__skill_name__icontains=search)).distinct()

        categories = self.get_work_categories()
        rates = self.get_all_hourly_rate()
        paginator = Paginator(all_freelancers, 20)

        page = request.GET.get('page')
        try:
            freelancer_list = paginator.page(page)
        except PageNotAnInteger:
            freelancer_list = paginator.page(1)
        except EmptyPage:
            freelancer_list = paginator.page(paginator.num_pages)
        context = {'all_freealncers': freelancer_list, 'rates': rates, 'categories': categories,'key_words':self.get_freelancer_search_query_list()}

        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        template_name = "profile/find_freelancers.html"
        rates = HourlyRate.objects.all()

        if request.is_ajax():
            response_data = {}
            query = self.get_freelancer_query()
            shortlist = query.get('shortlist')
            categories = query.get('categories')
            experience_level = query.get('experience_level')
            amountstartInput = query.get('amountstartInput')
            amountendInput = query.get('amountendInput')
            print categories
            if shortlist == [] and categories and experience_level == []:
                all_freealncers = self.category_sort(categories).distinct()
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            elif shortlist == [] and categories == [] and experience_level:
                all_freealncers = self.experience_sort(experience_level)
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            elif shortlist and categories == [] and experience_level == []:
                all_freealncers = FreelancerProfile.objects.filter(applied_user_name__status__in=shortlist,
                                                                   applied_user_name__job__user=request.user).distinct()
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            elif shortlist == [] and categories == [] and experience_level == []:
                all_freealncers = FreelancerProfile.objects.all()
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)
            elif shortlist and categories and experience_level == []:
                categories = self.category_sort(categories).distinct()
                all_freealncers = categories.filter(applied_user_name__status__in=shortlist,
                                                    applied_user_name__job__user=request.user).distinct()
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            elif shortlist and categories == [] and experience_level:
                experience = self.experience_sort(experience_level)
                all_freealncers = experience.filter(applied_user_name__status__in=shortlist,
                                                    applied_user_name__job__user=request.user).distinct()
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            elif shortlist == [] and categories and experience_level:
                categories = self.category_sort(categories).distinct()
                all_freealncers = categories.filter(
                    Q(user__experience_level__exp_level_type__in=experience_level) | Q(
                        user__experience_level__exp_level_type__startswith=experience_level))
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            elif shortlist and categories and experience_level:
                categories = self.category_sort(categories).distinct()
                experience_level = categories.filter(
                    Q(user__experience_level__exp_level_type__in=experience_level) | Q(
                        user__experience_level__exp_level_type__startswith=experience_level))
                all_freealncers = experience_level.filter(applied_user_name__status__in=shortlist,
                                                          applied_user_name__job__user=request.user).distinct()
                if amountstartInput and amountendInput:
                    all_freealncers = self.rate_sort(amountstartInput, amountendInput, all_freealncers)

            else:
                all_freealncers = self.get_all_freelancers()
            if request.user.is_authenticated():
                freelancer = self.get_freelancer_profile_instance()
                if freelancer:
                    all_freealncers = all_freealncers.exclude(user=request.user)

            response_data['html_content'] = render_to_string('profile/ajax/sorted_freelancer.html',
                                                             {'all_freealncers': all_freealncers, 'rates': rates,
                                                              'user': request.user})
            return HttpResponse(json.dumps(response_data), content_type="application/json")


class WorkCategory(FreelancerMixin, TemplateView):
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(WorkCategory, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            action = request.GET['action']
            response_data['status'] = True
            sub_categories = self.get_work_category(action)
            skills = self.get_work_category_skills(action)
            response_data['html_content'] = render_to_string('profile/ajax/subcategory.html',
                                                             {'sub_categories': sub_categories, 'skills': skills,
                                                              'page': request.GET.get('page')})
            return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request, *args, **kwargs):
        response_data = {}
        user = request.user
        FreelancerProfile.objects.get_or_create(user=user)
        if request.is_ajax():
            response_data['status'] = True
            sub_category = request.POST.getlist('sub_category')
            category = self.get_category(request.POST.get('profile_category'))
            if category:
                skills = request.POST.getlist('skills')
                category_object = self.get_work_sub_categories(sub_category)
                skills_category_object = self.get_categories_skills(skills)
                print skills_category_object
                freelancer = self.get_freelancer_profile_instance()
                if sub_category and skills:
                    for skill in freelancer.skills.all():
                        freelancer.skills.remove(skill)
                    for sub_categories in freelancer.sub_category.all():
                        freelancer.sub_category.remove(sub_categories)

                    freelancer.profile_category = category
                    try:
                        freelancer.save()
                        freelancer.skills.add(skills_category_object)
                    except:
                        for skill in skills_category_object:
                            freelancer.save()
                            freelancer.skills.add(skill)
                    try:
                        freelancer.save()
                        freelancer.sub_category.add(category_object)
                    except:
                        for category in category_object:
                            freelancer.save()
                            freelancer.sub_category.add(category)
                else:
                    response_data['status'] = False
                    if not sub_category:
                        response_data['sub_category'] = 'sub_category'
                    if not skills:
                        response_data['skills'] = 'skill'
            else:
                response_data['status'] = False

                response_data['profile_category'] = 'profile_category'
                response_data['sub_category'] = 'sub_category'

                response_data['skills'] = 'skill'

            return HttpResponse(json.dumps(response_data), content_type="application/json")


class FreelancerDetailView(UtilMixin, FreelancerMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        template_name = 'profile/freelancer_detail.html'
        slug = kwargs.get('slug')
        freelancer_certification = Certifications.objects.filter(user__freelancer_profile__slug=slug)
        freelancer_portfolio = FreelancerPortfolio.objects.filter(user__freelancer_profile__slug=slug)
        context = {'freelancer': self.get_freelancer_profile_by_id(), 'workhistory': self.get_all_employee_history(),
                   'freelancer_certification': freelancer_certification, 'freelancer_portfolio': freelancer_portfolio}
        return TemplateResponse(request, template_name, context)


class CountrySelectView(FreelancerMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            response_data = {}
            response_data['status'] = True
            country = request.GET['action']
            user = request.GET['user']
            regions = self.get_related_regions(country)
            if user == 'freelancer':
                response_data['html_content'] = render_to_string('profile/ajax/region.html',
                                                                     {'regions': regions})
            else:
                try:
                    status = request.GET['status']
                    response_data['html_content'] = render_to_string('ajax/client_region.html',
                                                                     {'regions': regions,  'status':status})
                except:
                    response_data['html_content'] = render_to_string('ajax/client_region.html',
                                                                     {'regions': regions})
            return HttpResponse(json.dumps(response_data), content_type="application/json")


class RegionSelectView(FreelancerMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            response_data = {}
            response_data['status'] = True
            region = request.GET['action']
            cities = self.get_related_city(region)
            user = request.GET['user']
            if user == 'freelancer':
                response_data['html_content'] = render_to_string('profile/ajax/city.html',
                                                                 {'cities': cities})
            else:
                try:
                    status = request.GET['status']
                    response_data['html_content'] = render_to_string('ajax/client_city.html',
                                                                 {'cities': cities, 'status': status})
                except:

                    response_data['html_content'] = render_to_string('ajax/client_city.html',
                                                                     {'cities': cities})
            return HttpResponse(json.dumps(response_data), content_type="application/json")


def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response
