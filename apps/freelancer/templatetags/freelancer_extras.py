from django import template
from django.utils.timesince import timesince
from apps.profile.models import FreelancerProfile, ClientProfile
from django.contrib.auth.models import Group

register = template.Library()


@register.simple_tag
def date_difference(start_date, end_date):
    days = abs((end_date-start_date).days)
    return timesince(start_date, end_date)

@register.filter(name='get_user_freelancer_profile')
def get_user_freelancer_profile(user):
    try:
        profile = FreelancerProfile.objects.get(user=user)
    except FreelancerProfile.DoesNotExist:
        profile = None

    return profile


@register.filter(name='get_user_client_profile')
def get_user_client_profile(user):
    try:
        profile = ClientProfile.objects.get(user=user)
    except ClientProfile.DoesNotExist:
        profile = None

    return profile


@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()

@register.filter(name='has_group_site_admin_only')
def has_group_site_admin_only(user):
    if user.groups.filter(name='Site_admin').exists():
        if ((user.groups.filter(name='Freelancers').exists()) or (user.groups.filter(name='Clients').exists())):
            status = False
        else:
            status = True
    else:
        status = False
    return status