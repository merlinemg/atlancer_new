from django.contrib import admin
from .models import Company, EmploymentHistory, EducationDetails, HourlyRate, Certifications, ExperienceLevel, FreelancerPortfolio, \
    WorkCategories, WorkSubCategories,Availability

admin.site.register(Company)
admin.site.register(EmploymentHistory)
admin.site.register(EducationDetails)
admin.site.register(HourlyRate)
admin.site.register(Certifications)
admin.site.register(ExperienceLevel)
admin.site.register(FreelancerPortfolio)
admin.site.register(WorkCategories)
admin.site.register(WorkSubCategories)
admin.site.register(Availability)