from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect, Http404
from django.core.exceptions import PermissionDenied
from functools import wraps
from django_countries import settings
from django.views.decorators.csrf import csrf_exempt, csrf_protect


def anonymous_required( view_function, redirect_to = None ):
    return AnonymousRequired( view_function, redirect_to )

class AnonymousRequired( object ):
    def __init__( self, view_function, redirect_to ):
        if redirect_to is None:
            from django.conf import settings
            redirect_to = settings.LOGIN_REDIRECT_URL
        self.view_function = view_function
        self.redirect_to = redirect_to

    def __call__( self, request, *args, **kwargs ):
        if request.user is not None and request.user.is_authenticated():
            return HttpResponseRedirect( self.redirect_to )
        return self.view_function( request, *args, **kwargs )

#
# def check_group_freelancer_required(view_function, redirect_to=None):
#     return check_group_freelancer(view_function, redirect_to)

#
# class check_group_freelancer(object):
#
#     def __init__(self, view_function, redirect_to):
#         if redirect_to is None:
#             redirect_to = settings.LOGIN_REDIRECT_URL
#         self.view_function = view_function
#         self.redirect_to = redirect_to
#
#     def __call__(self, request, *args, **kwargs):
#         if (not(request.user.groups.filter(name='Freelancers').exists())):
#             raise Http404("Group not found")
#         return self.view_function(request, *args, **kwargs)
#
#
# def check_group_client_required(view_function, redirect_to=None):
#     return check_group_client(view_function, redirect_to)
#
#
# class check_group_client(object):
#
#     def __init__(self, view_function, redirect_to):
#         if redirect_to is None:
#             redirect_to = settings.LOGIN_REDIRECT_URL
#         self.view_function = view_function
#         self.redirect_to = redirect_to
#
#     def __call__(self, request, *args, **kwargs):
#         if (not(request.user.groups.filter(name='Clients').exists())):
#             raise Http404("Group not found")
#         return self.view_function(request, *args, **kwargs)

@csrf_exempt
def check_group_required(view_function, redirect_to=None):
    """
    function for checking group permissions

    """
    return check_group(view_function, redirect_to)


class check_group(object):

    def __init__(self, view_function, redirect_to):
        if redirect_to is None:
            redirect_to = settings.LOGIN_REDIRECT_URL
        self.view_function = view_function
        self.redirect_to = redirect_to

    def __call__(self, request, *args, **kwargs):
        if request.user.groups.filter(name='Site_admin').exists() and request.user.is_superuser:
            if ((request.user.groups.filter(name='Freelancers').exists()) or (request.user.groups.filter(name='Clients').exists())):
                 pass
            else:
                raise Http404("Group not found")
        elif request.user.groups.filter(name='Site_admin').exists():
            if ((request.user.groups.filter(name='Freelancers').exists()) or (request.user.groups.filter(name='Clients').exists())):
                 pass
            # else:
            #     raise Http404("Group not found")
        elif request.user.is_superuser:
            raise Http404("Group not found")
        return self.view_function(request, *args, **kwargs)


def my_decorator(permission):
    """

    :param permission:
    :return:decorator for user permission checking
    """
    def decorator(a_view):
        def _wrapped_view(request, *args, **kwargs):
            # print 'h', request.user.groups.filter(name='Freelancers').exists()
            # print request.user.active_account
            if request.user.is_anonymous():
                return a_view(request, *args, **kwargs)
            else:
                User = get_user_model()
                get_user = User.objects.get(username=request.user.username)
                if request.user.active_account == 'FLR' and request.user.groups.filter(name='Freelancers').exists():
                    try:
                        group = Group.objects.get(name='Freelancers')
                        group.permissions.get(codename=permission)
                        return a_view(request, *args, **kwargs)
                    except:
                        if request.user.groups.filter(name='Clients').exists():
                            try:
                                group = Group.objects.get(name='Clients')
                                group.permissions.get(codename=permission)
                                get_user.active_account = 'CL'
                                request.user.active_account = 'CL'
                                get_user.save()
                                return a_view(request, *args, **kwargs)

                            except:
                                #return HttpResponseRedirect('/permission_error/')
                                raise PermissionDenied
                        else:
                            #return HttpResponseRedirect('/permission_error/')
                            raise PermissionDenied
                elif request.user.active_account == 'CL' and request.user.groups.filter(name='Clients').exists():

                    try:
                        group = Group.objects.get(name='Clients')
                        group.permissions.get(codename=permission)
                        return a_view(request, *args, **kwargs)
                    except:
                        if request.user.groups.filter(name='Freelancers').exists():
                            try:
                                group = Group.objects.get(name='Freelancers')
                                group.permissions.get(codename=permission)
                                get_user.active_account = 'FLR'
                                request.user.active_account = 'FLR'
                                get_user.save()
                                return a_view(request, *args, **kwargs)

                            except:
                                #return HttpResponseRedirect('/permission_error/')
                                raise PermissionDenied
                        else:
                            #return HttpResponseRedirect('/permission_error/')
                            raise PermissionDenied
                else:
                    #return HttpResponseRedirect('/permission_error/')
                    raise PermissionDenied
        return _wrapped_view
    return decorator