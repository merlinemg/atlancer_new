from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from django.db import models


from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill


class Company(models.Model):
    """
    Freelancer Company Name model
    """
    company_name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.company_name


class WorkCategories(models.Model):
    """
    Freelancer Work Categories model.
    """
    category_title = models.CharField(_('Work Category name'), max_length=255)

    def __unicode__(self):
        return self.category_title

    class Meta:
        verbose_name = "Work Category"
        verbose_name_plural = "Work Categories"


class WorkSubCategories(models.Model):
    """
    Freelancer Work SubCategories model
    """
    work_category = models.ForeignKey(WorkCategories,related_name='work_sub_categories',
                                      verbose_name="Work Sub Categories")
    sub_category_title = models.CharField(_('Sub category title'), max_length=255)

    def __unicode__(self):
        return self.sub_category_title

    class Meta:
        verbose_name = "Work Sub Category"
        verbose_name_plural = "Work Sub Categories"


class ExperienceLevel(models.Model):
    """
    Freelancer Experience Level model
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='experience_level',
                             verbose_name="Experience Level")
    exp_level_type = models.CharField(_('Experience Level title'), max_length=255)

    def __unicode__(self):
        return self.exp_level_type

    class Meta:
        verbose_name = "Experience Level"
        verbose_name_plural = "Experience Levels"


class EmploymentHistory(models.Model):
    """
    Freelancer Employment History
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='employment_history',
                             verbose_name="Employment history")
    company = models.ForeignKey(Company,)
    location = models.CharField(_('Location'), max_length=255)
    role = models.CharField(_('Role'), max_length=255)
    start_date = models.DateField(_('Start date'), max_length=255)
    end_date = models.DateField(_('End date'), max_length=255)
    description = models.TextField(_('Description'))

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Employment History"
        verbose_name_plural = "Employment Histories"


class EducationDetails(models.Model):
    """
     Freelancer Education Details model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='education_details',
                             verbose_name="Education details")
    institution_name = models.CharField(_('Institution Name'), max_length=255)
    degree = models.ForeignKey('knowledgebase.DegreeCourse', verbose_name='Degree')
    area_of_study = models.CharField(_('Area of study'), max_length=255)
    enrollment_date = models.DateField(_('Enrollment Date'), max_length=255)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Education Detail"
        verbose_name_plural = "Education Details"


class Certifications(models.Model):
    """
     Freelancer Certifications model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='cerifications',
                             verbose_name="Certifications")
    certificate_name = models.CharField(_('Certificate name'), max_length=255)
    issued_by = models.CharField(_('Issued authority'), max_length=255, blank=True, null=True)
    certificate = models.ImageField(_('Certificate'), upload_to='certificates_upload')
    certificate_thumbnail = ImageSpecField(source='certificate',processors=[ResizeToFill(370, 200)], format='JPEG',options={'quality': 60})


    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Certifications"
        verbose_name_plural = "Certifications"


class HourlyRate(models.Model):
    """
    Freelancer Hourly Rate model
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='hourly_rate',
                                verbose_name="Hourly Rate")
    hourly_rate = models.DecimalField(_('Hourly rate'),max_digits=5, decimal_places=2)
    actual_rate = models.DecimalField(_('Actual rate'),  max_digits=5, decimal_places=2)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Hourly rate"
        verbose_name_plural = "Hourly rates"


class FreelancerPortfolio(models.Model):
    """
    Freelancer Portfolio model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='portfolio',
                                verbose_name="Portfolio")
    project_name = models.CharField(_('Project name'), max_length=255)
    description = models.TextField(_('Description'))
    project_url = models.URLField(_('Project URL'))
    main_category = models.ForeignKey(WorkCategories, related_name='portfolio_work_categories',
                                      verbose_name="Work Categories")
    sub_category = models.ManyToManyField(WorkSubCategories,verbose_name='Work Sub Category',related_name='portfolio_sub_category')
    skills = models.ManyToManyField('knowledgebase.Skills', verbose_name='Skills', related_name='portfolio_skills')

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Freelancer portfolio"
        verbose_name_plural = "Freelancer portfolios"


class Availability(models.Model):
    """
    Freelancer Availability model
    """
    periods = (
        ('full_time', 'full_time'),
        ('weekly', 'weekly'),
    )
    statusChoice = (
        ('available', 'available'),
        ('not_available', 'not_available'),
    )


    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='availability_user',
                                verbose_name="Avalability user")
    status = models.CharField(_('Status'),max_length=100,choices=statusChoice)
    period = models.CharField(_('Period'),max_length=100,choices=periods,null=True,blank=True)
    avalable_hours = models.CharField(_('avalable hours'),null=True,blank=True,max_length=3)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Freelancer Avalability"
        verbose_name_plural = "Freelancer Avalabilities"