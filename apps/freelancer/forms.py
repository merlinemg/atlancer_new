from django import forms
from django import forms
import re

from apps.freelancer import models
from .models import Certifications, EducationDetails, EmploymentHistory, HourlyRate, ExperienceLevel, \
    FreelancerPortfolio, Availability
from .models import Company, WorkSubCategories
from django.shortcuts import get_object_or_404
from apps.profile.models import FreelancerProfile
from apps.knowledgebase.models import Skills



class EditLanguageForm(forms.ModelForm):

    class Meta:
        model = FreelancerProfile
        fields = ['language_proficiency']



    def clean_language_title(self):
        language = self.cleaned_data['language_proficiency']
        if not language:
            return forms.ValidationError("This field is required")
        return self.cleaned_data['language_proficiency']

    def save(self,commit=True):
        language_form = super(EditLanguageForm, self).save(commit=False)
        if commit:
            language_form.save()
        return language_form

class EmploymentHistoryForm(forms.ModelForm):
    """
    Form for adding Employment History of freelancer
    """

    def __init__(self, *args, **kwargs):
        super(EmploymentHistoryForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs.update({'class': 'datepicker'})
        self.fields['end_date'].widget.attrs.update({'class': 'datepicker1'})
        self.fields['description'].widget.attrs.update({'rows': '5'})

    class Meta:
        model = EmploymentHistory
        exclude = ['user', 'company']

    def clean_location(self):
        return self.cleaned_data['location'].strip()

    def clean_company(self):
        return self.cleaned_data['company'].strip()

    def clean_role(self):
        return self.cleaned_data['role'].strip()

    def clean_description(self):
        return self.cleaned_data['description'].strip()


    def save(self, user, company_id, name, commit=True):
        try:
            company = Company.objects.get(pk=int(company_id))
        except Exception as e:
            company = Company(company_name=name)
            company.save()
        employmentHistory = super(EmploymentHistoryForm, self).save(commit=False)
        if commit:
            employmentHistory.user = user
            employmentHistory.company = company
            employmentHistory.save()
        return employmentHistory

    def delete(self, user_id):
        employment_history = EmploymentHistory.objects.get(pk=user_id)
        employment_history.delete()
        return 'deleted'


class EducationDetailForm(forms.ModelForm):
    """
    Form for adding Education Detail of freelancer
    """
    institution_name = forms.RegexField(regex=r'[a-zA-Z]+$',
                                  error_message=(
                                      "Only alphabets is allowed."))

    def __init__(self, *args, **kwargs):
        super(EducationDetailForm, self).__init__(*args, **kwargs)
        self.fields['enrollment_date'].widget.attrs.update({'class': 'datepicker2'})

    class Meta:
        model = EducationDetails
        exclude = ['user']

    def clean_institution_name(self):
        return self.cleaned_data['institution_name'].strip()

    def clean_area_of_study(self):
        return self.cleaned_data['area_of_study'].strip()

    def save(self, user, commit=True):
        education_detail = super(EducationDetailForm, self).save(commit=False)
        if commit:
            education_detail.user = user
            education_detail.save()
        return user

    def delete(self, user_id):
        education_details = EducationDetails.objects.get(pk=user_id)
        education_details.delete()
        return 'deleted'


class CertificationForm(forms.ModelForm):
    """
    Form for adding Certification of freelancer
    """
    certificate_name = forms.RegexField(regex=r'[a-zA-Z]+$',
                                  error_message=(
                                      "Only alphabets is allowed."))

    class Meta:
        model = Certifications
        exclude = ['user']

    def clean_certificate_name(self):
        return self.cleaned_data['certificate_name'].strip()

    def clean_issued_by(self):
        return self.cleaned_data['issued_by'].strip()

    def save(self, user, commit=True):
        certification = super(CertificationForm, self).save(commit=False)
        if commit:
            certification.user = user
            certification.save()
        return certification

    def delete(self, user_id):
        print "entered form delete certificate **************** "
        certificate = get_object_or_404(Certifications, pk=user_id)
        print "certificate", certificate
        certificate.delete()
        return 'deleted'


class HourlyRateForm(forms.ModelForm):
    """
    Form for adding Hourly Rate of freelancer
    """

    hourly_rate = forms.RegexField(regex=r'^[0-9]\d*(\.\d+)?$',
                                 error_message=(
                                     "Enter a valid rate."))

    def __init__(self, *args, **kwargs):
        super(HourlyRateForm, self).__init__(*args, **kwargs)
        self.fields['actual_rate'].widget.attrs['readonly'] = True

    class Meta:
        model = HourlyRate
        exclude = ['user']

    def save(self, user, commit=True):
        hourlyRate = super(HourlyRateForm, self).save(commit=False)
        if commit:
            hourlyRate.user = user
            hourlyRate.save()
        return hourlyRate


class CompanyForm(forms.ModelForm):
    """
    form for adding company
    """

    class Meta:
        model = Company
        fields = ['company_name']

    def save(self, commit=True):
        company = super(CompanyForm, self).save(commit=False)
        if commit:
            company.save()
        return company


class ExperienceLevelForm(forms.ModelForm):
    """
    form for adding Experience Level of freelancer
    """

    class Meta:
        model = ExperienceLevel
        exclude = ['user']

    def save(self, user,commit=True):

        experience_level = super(ExperienceLevelForm, self).save(commit=False)
        if commit:
            experience_level.user = user
            experience_level.save()
            print " Entered save........................"
        return experience_level


class FreelancerPortfolioForm(forms.ModelForm):
    """
     Form for adding Portfolio of freelancer
    """
    project_name = forms.RegexField(regex=r'[a-zA-Z]+$',
                                  error_message=(
                                      "Only alphabets is allowed."))

    def __init__(self, *args, **kwargs):
        super(FreelancerPortfolioForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget.attrs.update({'rows': '5'})

    class Meta:
        model = FreelancerPortfolio
        exclude = ['user','sub_category','skills']

    def clean_project_name(self):
        return self.cleaned_data['project_name'].strip()

    def clean_description(self):
        description = self.cleaned_data['description'].strip()
        reg = re.compile('[a-zA-Z]+$')
        if not reg.match(description):
            raise forms.ValidationError("Only alphabets is allowed.")
        return description

    def save(self, user, skills,sub_categories, commit=True):
        print" Entered save ***************"
        portfolio = super(FreelancerPortfolioForm, self).save(commit=False)
        if commit:
            print "entered commit ******************"
            portfolio.user = user
            portfolio.save()
            try:
                portfolio.sub_category.add(sub_categories)
                portfolio.skills.add(skills)
            except:
                for skill in skills:
                    portfolio.skills.add(skill)
                for sub_category in sub_categories:
                    portfolio.sub_category.add(sub_category)

        return portfolio

    def delete(self, user_id):
        print "entered form delete **************** "
        portfolio = FreelancerPortfolio.objects.get(pk=user_id)
        portfolio.delete()
        print "data deleted"
        return 'deleted'


# class CategoryForm(forms.ModelForm):
#     class Meta:
#         model = FreelancerProfile
#         exclude = ['user', 'professional_title', 'skills', 'language_proficiency', 'address', 'profile_pic', 'about_me']
#
#     def save(self,sub_category, commit=True):
#         form = super(CategoryForm, self).save(commit=False)
#         print "CategoryForm",sub_category
#         if commit:
#             form.save()
#         return form


class EditEducationDetailsForm(forms.ModelForm):
    institution_name = forms.RegexField(regex=r'[a-zA-Z]+$',
                                  error_message=(
                                      "Only alphabets is allowed."))
    def __init__(self, *args, **kwargs):
        super(EditEducationDetailsForm, self).__init__(*args, **kwargs)
        self.fields['enrollment_date'].widget.attrs.update({'class': 'datepicker2'})

    class Meta:
        model = EducationDetails
        exclude = ['user']

    def clean_institution_name(self):
        return self.cleaned_data['institution_name'].strip()

    def clean_area_of_study(self):
        return self.cleaned_data['area_of_study'].strip()

    def save(self, user, commit=True):
        education_details = super(EditEducationDetailsForm, self).save(commit=False)
        if commit:
            education_details.user = user
            education_details.save()
        return user


class EditEmploymentHistoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditEmploymentHistoryForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs.update({'class': 'datepicker'})
        self.fields['end_date'].widget.attrs.update({'class': 'datepicker1'})

    class Meta:
        model = EmploymentHistory
        exclude = ['user']

    def clean_location(self):
        return self.cleaned_data['location'].strip()

    def clean_role(self):
        return self.cleaned_data['role'].strip()

    def clean_description(self):
        return self.cleaned_data['description'].strip()

    def save(self, user, commit=True):
        employment_history = super(EditEmploymentHistoryForm, self).save(commit=False)
        if commit:
            employment_history.user = user
            employment_history.save()
        return user


class EditPortfolioForm(forms.ModelForm):
    project_name = forms.RegexField(regex=r'[a-zA-Z]+$',
                                  error_message=(
                                      "Only alphabets is allowed."))

    class Meta:
        model = FreelancerPortfolio
        exclude = ['user', 'sub_category', 'skills']

    def clean_project_name(self):
        return self.cleaned_data['project_name'].strip()

    def clean_description(self):
        description = self.cleaned_data['description'].strip()
        reg = re.compile('[a-zA-Z]+$')
        if not reg.match(description):
            raise forms.ValidationError("Only alphabets is allowed.")
        return description

    def save(self, user, skills,sub_categories, commit=True):
        portfolio = super(EditPortfolioForm, self).save(commit=False)
        print skills,sub_categories
        if commit:
            portfolio.user = user
            portfolio.save()
            for skill in portfolio.skills.all():
                portfolio.skills.remove(skill)
            for sub_category in portfolio.sub_category.all():
                portfolio.sub_category.remove(sub_category)

            try:
                portfolio.skills.add(skills)
                portfolio.sub_category.add(sub_categories)
            except:
                for skill in skills:
                    portfolio.skills.add(skill)
                for sub_category in sub_categories:
                    portfolio.sub_category.add(sub_category)
            print "edited portfolio"
        return user


class EditCertificationForm(forms.ModelForm):
    certificate_name = forms.RegexField(regex=r'[a-zA-Z]+$',
                                  error_message=(
                                      "Only alphabets is allowed."))
    class Meta:
        model = Certifications
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(EditCertificationForm, self).__init__(*args, **kwargs)
        self.fields['certificate'].widget.attrs.update({'id': 'edit_certificate'})

    def clean_certificate_name(self):
        return self.cleaned_data['certificate_name'].strip()

    def clean_issued_by(self):
        return self.cleaned_data['issued_by'].strip()

    def save(self, user, commit=True):
        certificate = super(EditCertificationForm, self).save(commit=False)
        if commit:
            certificate.user = user
            certificate.save()
        return user

class AvalabilityForm(forms.ModelForm):

    avalable_hours = forms.RegexField(regex=r'^\+?1?\d{1,3}$',
                                      error_message=(
                                           "Invalid Entry"),required=False)
    def __init__(self, *args, **kwargs):
        super(AvalabilityForm, self).__init__(*args, **kwargs)
        self.fields['period'].widget.attrs['class'] = 'profile_peroid_dropdown'
        self.fields['period'].widget.attrs['onchange'] = "period_type();"
        self.fields['status'].widget.attrs['type'] = "checkbox"
        self.fields['status'].widget.attrs['class'] = "check_box_style"
        self.fields['status'].widget.attrs['onclick'] = "available_func();"

    def clean_period(self):
        period = self.cleaned_data.get('period')
        status = self.cleaned_data.get('status')
        if period == None and status != 'not_available':
            raise forms.ValidationError("Please select a valid choice")
        return period


    def clean_avalable_hours(self):
        status = self.cleaned_data.get('status')
        data = self.cleaned_data.get('avalable_hours')
        period = self.cleaned_data.get('period')
        if data:
            if data > '168' and not data and status != 'not_available':
                raise forms.ValidationError("Please enter hours in range 1-168")
        else:
            if period == 'full_time' or status == 'not_available':
                data = '168'
            else:
                raise forms.ValidationError("This Field is required")
        return data

    class Meta:
        model = Availability
        exclude = ['user']

    def save(self, user,status=None,period=None,commit=True):
        form = super(AvalabilityForm, self).save(commit=False)
        if commit:
            if status == 'not_available':
                form.period = 'full_time'
                form.avalable_hours = ''
            if period == 'full_time':
                form.avalable_hours='168'
            form.user = user
            form.save()
        return user


class SkillSForm(forms.ModelForm):
    class Meta:
        model = Skills
        exclude = ['work_category']

    def save(self, main_category_id=None, commit=True):
        form = super(SkillSForm, self).save(commit=False)
        if commit:
            form.work_category = main_category_id
            form.save()
        return main_category_id

