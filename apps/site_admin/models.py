from ckeditor.fields import RichTextField
from django.db import models

# Create your models here.

class AboutUs(models.Model):
    """
    AboutUs model
    """
    about_us = RichTextField()

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else True
