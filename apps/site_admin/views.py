import json
from django.shortcuts import render
from django.views.generic import TemplateView, FormView
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, authenticate, login
from django.contrib.auth.models import Group
from django.contrib.auth import logout
from django.shortcuts import redirect

# Create your views here.
from apps.accounts.forms import UserLoginForm
from apps.accounts.models import User
from apps.general.utils import UtilMixin
from apps.site_admin.models import AboutUs


class siteAdminView(TemplateView, UtilMixin):

    template_name = 'account/admin_login.html'
    form_class = UserLoginForm
    model = User

    def get_context_data(self, **kwargs):
        context = super(siteAdminView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        response_data = {}
        form = UserLoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.

        try:
            user = authenticate(username=username, password=password)
            user.is_authenticated()
            if user is not None:
                if user.groups.filter(name='Site_admin').exists():
                    login(request, user)
                    self.update_active_profile('SITE')
                    if request.POST.has_key('remember_me'):
                            request.session.set_expiry(1209600)
                    return HttpResponseRedirect('/admin/mail/template-list/')
                else:
                    form.add_error(None, "Not a site admin.")
        except:
            form.add_error(None, "Please enter a correct username and password. Note that both fields may be case-sensitive.")
        response_data['errors'] = form.errors
        return render(request, self.template_name, {'form': form})


def siteAdminLogOutView(request):
    logout(request)
    return HttpResponseRedirect('/site_admin/')

class AboutUsView(TemplateView):

    template_name='about_us.html'

    def get_context_data(self, **kwargs):
        context = super(AboutUsView, self).get_context_data(**kwargs)
        context['about'] = AboutUs.objects.all()
        return context
