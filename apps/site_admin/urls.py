from django.conf.urls import url
from .views import siteAdminView, siteAdminLogOutView
from . import views


urlpatterns = [
    url(r'^$', siteAdminView.as_view(), name='site_admin_login'),
    url(r'^log_out$', views.siteAdminLogOutView, name='admin_log_out'),
]
