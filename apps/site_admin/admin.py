from django.contrib import admin

from apps.site_admin.models import AboutUs

admin.site.register(AboutUs)