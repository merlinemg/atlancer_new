from django.contrib import admin
from .models import FreelancerProfile, ClientProfile


admin.site.register(FreelancerProfile)
admin.site.register(ClientProfile)
