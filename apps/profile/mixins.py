from decimal import *

from apps.general.models import CommissionRate
from apps.profile.models import FreelancerProfile
from apps.freelancer.models import EmploymentHistory, EducationDetails, Certifications,\
    FreelancerPortfolio, HourlyRate

from apps.freelancer.models import WorkCategories,WorkSubCategories
from django.db.models import Q
from cities_light.models import Country, Region, City


class ProfileMixin(object):

    def get_related_city(self,region):
        """
        Get related cities...
        :return:cities
        """
        city = City.objects.filter(region__name=region)
        return city
