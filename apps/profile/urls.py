from django.conf.urls import url

from apps.profile.views import AddFreelancerView, AddEmploymentHistoryView, AddEducationDetailView, AddCertificationView, \
        AddHourlyRateView, EditFreelancerProfileView, AddPortfolioForm, EditOverview, ProfileSwitchingView, CommonProfileView



urlpatterns = [
    # starting of freelancer profile
    url(r'^freelancer/add/$', AddFreelancerView.as_view(), name='add_freelancer_profile'),
    url(r'^freelancer/add/employmentHistory/$', AddEmploymentHistoryView.as_view(), name='add_employmentHistory'),
    url(r'^freelancer/add/educationDetails/$', AddEducationDetailView.as_view(), name='add_educationDetails'),
    url(r'^freelancer/add/certification/$', AddCertificationView.as_view(), name='add_certification'),
    url(r'^freelancer/add/rate/$', AddHourlyRateView.as_view(), name='add_rate'),
    url(r'^freelancer/profile/$', EditFreelancerProfileView.as_view(), name='freelancer_profile'),
    url(r'^freelancer/add/portfolio/$', AddPortfolioForm.as_view(), name='add_portfolio'),
    url(r'^freelancer/edit/overview/$', EditOverview.as_view(), name='edit_overview'),
    url(r'^profile-switching/$', ProfileSwitchingView.as_view(), name='profile_switching'),
    url(r'^$', CommonProfileView.as_view(), name='common_profile'),
    # end of freelancer profile
    ]