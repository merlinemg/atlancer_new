import json

from django.utils.crypto import get_random_string
from django.utils.text import slugify
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import redirect

from apps.profile.forms import FreelancerProfileForm, FreelancerOverviewEdit, NameChangeForm, ResetPasswordForm
from apps.freelancer.forms import EducationDetailForm, EmploymentHistoryForm, CertificationForm, HourlyRateForm, \
    ExperienceLevelForm, FreelancerPortfolioForm
from apps.freelancer.models import EmploymentHistory, EducationDetails, Certifications, FreelancerPortfolio
from apps.profile.models import FreelancerProfile
from apps.general.utils import UtilMixin

from apps.freelancer.mixins import FreelancerMixin


class ProfileSwitchingView(UtilMixin, View):
    greeting = "Good Day"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileSwitchingView, self).dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        """
        get to switching account....
        :param args:
        :param kwargs:
        :return:
        """
        if self.has_freelancer_profile() and self.active_profile() == 'CL':
            self.update_active_profile('FLR')
            return redirect('freelancer_profile')
        self.update_active_profile('CL')
        return redirect('client_profile')


class AddFreelancerView(FreelancerMixin,TemplateView):
    """
    view to add freelancer profile and display employee history form , education detail form,
    certification form and hourly rate form.
    """
    form_class = FreelancerProfileForm
    second_form_class = EmploymentHistoryForm
    third_form_class = EducationDetailForm
    forth_form_class = CertificationForm
    fifth_form_class = HourlyRateForm
    sixth_form_class = ExperienceLevelForm
    seven_form_class = FreelancerPortfolioForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddFreelancerView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddFreelancerView, self).get_context_data(**kwargs)
        context['form_profile'] = self.form_class()
        context['form_employment'] = self.second_form_class()
        context['form_education'] = self.third_form_class()
        context['form_certifications'] = self.forth_form_class()
        context['form_Rate'] = self.fifth_form_class()
        context['form_portfolio'] = self.seven_form_class()
        freelancer_certificate = self.get_freelancer_certificates()
        employee_eduction = self.get_edu_info()
        employee_history = self.get_employee_history()
        portfolio = self.get_portfolio()
        context['employee_history'] = employee_history
        context['employee_eduction'] = employee_eduction
        context['freelancer_certificate'] = freelancer_certificate
        context['portfolio'] = portfolio
        return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        form = self.form_class(request.POST, request.FILES)
        level = request.POST['level']
        if form.is_valid() and request.is_ajax():
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
        elif request.is_ajax() and level == "second":
            exp_level = request.POST['exp_level_type']
            self.update_experience_level(exp_level)
            response_data['status'] = True
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def update_experience_level(self, exp_level):
        """

        :param exp_level:
        :return:
        """
        experience_form = self.sixth_form_class(self.request.POST)
        experience_form.save(self.request.user)
        return exp_level


class AddEmploymentHistoryView(FreelancerMixin,TemplateView):
    """
    View to add freelancer employment history
    """
    form_class = EmploymentHistoryForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddEmploymentHistoryView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        context = super(AddEmploymentHistoryView, self).get_context_data(**kwargs)
        context['form_employment'] = self.form_class()
        return context

    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        employment = request.POST['employment_profile']
        response_data = {}
        form = self.form_class(request.POST)
        if form.is_valid() and employment == 'create':
            name = request.POST['company_name']
            country_id = request.POST['code']
            form.save(user, country_id, name)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            employee_history = self.get_employee_history()
            response_data['html_content'] = render_to_string('profile/ajax/employee_work_history.html',
                                                             {'employee_history': employee_history})
        elif form.is_valid() and employment == 'profile':
            name = request.POST['company_name']
            country_id = request.POST['code']
            form.save(user, country_id, name)
            response_data['status'] = True
            employee_history = self.get_employee_history()
            response_data['html_content'] = render_to_string('profile/ajax/employment_profile.html',
                                                             {'freelancer_employee_history': employee_history})
        elif employment == 'delete':
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            employee_history = self.get_employee_history()
            response_data['html_content'] = render_to_string('profile/ajax/employment_profile.html',
                                                             {'freelancer_employee_history': employee_history})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddEducationDetailView(FreelancerMixin,TemplateView):
    """
    View to add Education detail of freelancer
    """
    form_class = EducationDetailForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddEducationDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        method to display education form  details
        :param kwargs:
        :return:
        """
        context = super(AddEducationDetailView, self).get_context_data(**kwargs)
        context['form_education'] = self.form_class()
        return context

    def post(self, request, *args, **kwargs):
        """
        Post request take data from create profile and my profile of freelancer.
        This method add education details and also perform delete .
        :param request:
        :param args:
        :param kwargs:
        :return:It returns a table in the html page.
        """
        user = request.user
        response_data = {}
        form = self.form_class(request.POST)
        profile = request.POST['education_profile']
        if form.is_valid() and profile == 'create':
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            employee_eduction = self.get_edu_info()
            response_data['html_content'] = render_to_string('profile/ajax/employee_education.html',
                                                             {'employee_eduction': employee_eduction})
        elif form.is_valid() and profile == 'profile':
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            freelancer_eduction = self.get_edu_info()
            response_data['html_content'] = render_to_string('profile/ajax/education_profile.html',
                                                             {'freelancer_education_details': freelancer_eduction})
        elif profile == "delete":
            user_id = request.POST['user_id']
            print"user_id ", user_id
            form.delete(user_id)
            response_data['status'] = True
            freelancer_eduction = self.get_edu_info()
            response_data['html_content'] = render_to_string('profile/ajax/education_profile.html',
                                                             {'freelancer_education_details': freelancer_eduction})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddCertificationView(FreelancerMixin,TemplateView):
    """
     View to add certification details of freelancer
    """
    form_class = CertificationForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddCertificationView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddCertificationView, self).get_context_data(**kwargs)
        context['form_certifications'] = self.form_class()
        return context

    def post(self, request, *args, **kwargs):
        print "entered post"
        print "request.POST", request.POST
        user = request.user
        response_data = {}
        form = self.form_class(request.POST, request.FILES)
        certificate = request.POST['certificate_profile']
        if form.is_valid() and certificate == 'create':
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            freelancer_certificate = self.get_freelancer_certificates()
            response_data['html_content'] = render_to_string('profile/ajax/freelancer_certification.html',
                                                             {'freelancer_certificate': freelancer_certificate})
        elif form.is_valid() and certificate == 'profile':
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
            freelancer_certificate = self.get_freelancer_certificates()
            response_data['html_content'] = render_to_string('profile/ajax/certificates_profile.html',
                                                             {'freelancer_certification': freelancer_certificate})
        elif certificate == 'delete':
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            freelancer_certificate = self.get_freelancer_certificates()
            response_data['html_content'] = render_to_string('profile/ajax/certificates_profile.html',
                                                             {'freelancer_certification': freelancer_certificate})
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddHourlyRateView(TemplateView):
    """
    View to add hourly rate of freelancer
    """
    form_class = HourlyRateForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddHourlyRateView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddHourlyRateView, self).get_context_data(**kwargs)
        context['form_Rate'] = self.form_class()
        return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        form = self.form_class(request.POST)
        if form.is_valid() and request.is_ajax():
            form.save(user)
            response_data['status'] = True
            response_data['success'] = " successfully sent."
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class AddPortfolioForm(FreelancerMixin,TemplateView):
    form_class = FreelancerPortfolioForm
    template_name = "profile/createProfile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddPortfolioForm, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddPortfolioForm, self).get_context_data(**kwargs)
        context['form_portfolio'] = self.form_class()
        return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        form = self.form_class(request.POST)
        portfolio = request.POST['portfolio_profile']
        if form.is_valid() and portfolio == 'create' or portfolio == 'profile':
            form.save(user)
            if portfolio == 'create':
                portfolio = self.get_portfolio()
                response_data['html_content'] = render_to_string('profile/ajax/freelancer_portfolio.html',
                                                                 {'portfolio': portfolio})
            elif portfolio == 'profile':
                portfolio = self.get_portfolio()
                response_data['html_content'] = render_to_string('profile/ajax/portfolio_profile.html',
                                                                 {'freelancer_portfolio': portfolio})
            response_data['status'] = True
        elif portfolio == 'delete':
            user_id = request.POST['user_id']
            form.delete(user_id)
            response_data['status'] = True
            portfolio = self.get_portfolio()
            response_data['html_content'] = render_to_string('profile/ajax/portfolio_profile.html',
                                                             {'freelancer_portfolio': portfolio})
        else:
            print "error **************"
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class EditFreelancerProfileView(FreelancerMixin,TemplateView):
    """
    View to add hourly rate of freelancer
    """
    profile_class = FreelancerProfileForm
    employment_class = EmploymentHistoryForm
    education_class = EducationDetailForm
    certification_class = CertificationForm
    rate_class = HourlyRateForm
    portfolio_class = FreelancerPortfolioForm
    template_name = "profile/freelancer_profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditFreelancerProfileView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditFreelancerProfileView, self).get_context_data(**kwargs)
        print"entered get context"
        context['form_profile'] = self.profile_class()
        context['form_employment'] = self.employment_class()
        context['form_education'] = self.education_class()
        context['form_certifications'] = self.certification_class()
        context['form_Rate'] = self.rate_class()
        context['form_portfolio'] = self.portfolio_class()
        freelancer_employee_history = self.get_employee_history()
        freelancer_profile = self.get_freelancer_profile_instance()
        freelancer_education_details = self.get_edu_info()
        freelancer_certifiaction = self.get_freelancer_certificates()
        freelancer_portfolio = self.get_portfolio()
        context['freelancer_employee_history'] = freelancer_employee_history
        context['freelancer_profile'] = freelancer_profile
        context['freelancer_education_details'] = freelancer_education_details
        context['freelancer_certification'] = freelancer_certifiaction
        context['freelancer_portfolio'] = freelancer_portfolio
        return context


class EditOverview(FreelancerMixin,UpdateView):
    """
     View to edit freelancer overview
    """
    model = FreelancerProfile
    template_name = "profile/freelancer_profile.html"
    form_class = FreelancerOverviewEdit

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditOverview, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            profile = self.get_freelancer_profile_instance()
            overview_form = self.form_class(instance=profile)
            response_data = {}
            response_data['status'] = True
            print "overview_form", overview_form
            response_data['html_content'] = render_to_string('profile/ajax/overview.html', {'form': overview_form})
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class CommonProfileView(UtilMixin,TemplateView):
    # model = FreelancerProfile
    template_name = "profile/profile.html"

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CommonProfileView, self).dispatch(*args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            response_data={}
            password = request.POST.get('password')
            is_password = request.POST.get('is_password')
            name_form = NameChangeForm(request.POST,instance=request.user)
            password_form=ResetPasswordForm(request.POST,instance=request.user)
            print is_password,password
            if name_form.is_valid():
                name_form.save()
                profile = self.get_authenticated_user(request.user)
                unique_id = get_random_string(length=15)
                slug = slugify(profile.user.get_full_name()) + "-" + unique_id
                profile.slug = slug
                profile.save()
                response_data['status']='name'
            else:
                response_data['form_error']=name_form.errors
                response_data['status'] = 'error'
                print name_form.errors

            if is_password == 'true':
                if password_form.is_valid():
                    password_form.save(password)
                    response_data['status']='password'
                else:
                    response_data['status']='error'
                    response_data['form_error']=password_form.errors
                    print "password_form.errors",password_form.errors
            return HttpResponse(json.dumps(response_data), content_type="application/json")


