from __future__ import unicode_literals

import uuid
from datetime import date, datetime

from ckeditor.fields import RichTextField
from django.core.validators import RegexValidator, MaxLengthValidator, MinLengthValidator
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from django.db import models
from django.utils.crypto import get_random_string
from apps.knowledgebase.models import Skills, Language
from apps.freelancer.models import WorkSubCategories
from cities_light.models import Country, Region, City
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill

from apps.freelancer.models import WorkCategories


class ClientProfile(models.Model):
    """
    Client model
    """
    country_name = (
        ('USA', 'USA'),
        ('UK', 'UK'),
        ('India', 'India'),
    )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='client_profile', verbose_name="Client Profile")
    profile_pic = models.ImageField(upload_to='profile_picture',null=True, max_length=100)
    profile_pic_thumbnail = ImageSpecField(source='profile_pic',processors=[ResizeToFill(170, 170)],format='JPEG',options={'quality': 60})
    company = models.CharField(_('Company Name'), null=True, max_length=255, default='')
    about_company = RichTextField(validators=[MinLengthValidator(300)])
    area_of_work = models.CharField(_('Area of Work'), null=True, default='', max_length=255)
    vat_id = models.CharField(_('Vat Id'),max_length=100,null=True,default='',blank=True)
    country = models.ForeignKey(Country,verbose_name='Country',related_name='client_country',null=True,blank=True)
    region = models.ForeignKey(Region,verbose_name='Region',related_name='client_region',null=True,blank=True)
    city = models.ForeignKey(City,verbose_name='City',related_name='client_city',null=True,blank=True)
    company_address = models.TextField(_('Company Address'), null=True, default='')
    owner_name = models.CharField(_('Owner Name'),null=True, default='', max_length=200,blank=True)
    birthday = models.DateField(_("Date"), null=True,blank=True)
    company_phone_number = models.CharField(_('Company Phone Number'), max_length=255, null=True, default='')
    phone_number = models.CharField(_('Phone Number'), max_length=12, null=True, default='',blank=True)
    website = models.URLField(_('Website'),max_length=500,null=True, default='')
    slug = models.SlugField(max_length=255, unique=True,auto_created=True,editable=False)

    def save(self, *args, **kwargs):
        super(ClientProfile, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.user.get_full_name()) + "-" + unique_id
            self.save()

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Client Profile"
        verbose_name_plural = "Client Profiles"
        default_permissions = ('add', 'change', 'delete', 'view')


class FreelancerProfile(models.Model):
    """
    Freelancer model
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='freelancer_profile',
                                verbose_name="Freelancer Profile")
    profile_pic = models.ImageField(upload_to='profile_picture', max_length=100,null=True,blank=True)
    profile_pic_thumbnail = ImageSpecField(source='profile_pic',processors=[ResizeToFill(170, 170)], format='JPEG',options={'quality': 60})
    professional_title = models.CharField(_('Professional Title'), max_length=255)
    about_me = RichTextField(validators=[MinLengthValidator(300)])
    skills = models.ManyToManyField(Skills, verbose_name='Skills', related_name='skills')
    language_proficiency = models.ManyToManyField(Language, verbose_name='Language proficiency',
                                                  related_name='languages')
    profile_category = models.ForeignKey(WorkCategories, related_name='freelancer_work_categories',
                                      verbose_name="Work Category",null=True,blank=True)

    sub_category = models.ManyToManyField(WorkSubCategories,verbose_name='Work Sub Categories',related_name='sub_category')
    country = models.ForeignKey(Country,verbose_name='Country',related_name='country',null=True,blank=True)
    region = models.ForeignKey(Region,verbose_name='Region',related_name='region',null=True,blank=True)
    city = models.ForeignKey(City,verbose_name='City',related_name='city',null=True,blank=True)
    slug = models.SlugField(max_length=255, unique=True,auto_created=True,editable=False)

    def save(self, *args, **kwargs):
        super(FreelancerProfile, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug =  slugify(self.user.get_full_name()) + "-" + unique_id
            self.save()


    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Freelancer Profile"
        verbose_name_plural = "Freelancer Profiles"
        default_permissions = ('add', 'change', 'delete', 'view')
