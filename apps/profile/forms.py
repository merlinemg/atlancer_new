import re

from ckeditor.widgets import CKEditorWidget
from django.forms import ModelForm
from django import forms
from django.forms import PasswordInput,TextInput
from apps.freelancer.models import WorkCategories
from apps.accounts.models import User
from apps.profile.models import FreelancerProfile, ClientProfile
from HTMLParser import HTMLParser
from django.contrib.auth.models import Group

class ResetPasswordForm(forms.ModelForm):
    """
    Form to reset password of client
    """
    password = forms.CharField(widget=forms.PasswordInput, label="Password", required=True)
    password2 = forms.CharField(widget=forms.PasswordInput, label="Confirm Password", required=True)

    class Meta:
        model = User
        fields = ['password']

    def clean_password2(self):
        password1 = self.cleaned_data['password']
        password2 = self.cleaned_data['password2']
        if password1 != password2:
            raise forms.ValidationError("Password do not match.")
        return password2

    def save(self, password, commit=True):
        user = super(ResetPasswordForm, self).save(commit=False)
        if commit:
            user.set_password(password)
            user.save()
        return user

class NameChangeForm(forms.ModelForm):
    """
    Form for changing name
    """
    last_name = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."))
    class Meta:
        model = User
        fields = ['first_name', 'last_name']

    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        if not re.match(r'[a-zA-Z]', first_name) and first_name:
            raise forms.ValidationError("Only alphabets is allowed")
        return first_name

    # def clean_last_name(self):
    #     last_name = self.cleaned_data['last_name']
    #     if not last_name.isalpha() and last_name:
    #         raise forms.ValidationError("Only alphabets is allowed")
    #     return last_name

    def save(self,commit=True):
        form = super(NameChangeForm, self).save(commit=False)
        if commit:
            form.save()




class ClientActiveProfileForm(forms.ModelForm):
    """
    Form for adding profile of freelancer
    """

    company = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."))
    class Meta:
        model = ClientProfile
        fields = ['company', 'company_address', 'phone_number', 'website']

    def clean_company_address(self):
        return self.cleaned_data['company_address'].strip()

    def save(self, user, commit=True):
        profile = super(ClientActiveProfileForm, self).save(commit=False)
        if commit:
            profile.user = user
            g = Group.objects.get(name='Clients')
            g.user_set.add(user)
            profile.save()
            user.active_account = 'CL'
            user.save()
        return user


class FreelancerProfileForm(forms.ModelForm):
    """
    Form for adding profile of freelancer
    """

    class Meta:
        model = FreelancerProfile
        exclude = ['user', 'language_proficiency', 'skills','sub_category','country','region','city','profile_category','slug']

    def clean_company(self):
        company = self.cleaned_data.get("company").strip()
        if company == "":
            raise forms.ValidationError("This field is required.")
        return company

    def clean_company_address(self):
        company_address = self.cleaned_data.get("company_address").strip()
        if company_address == "":
            raise forms.ValidationError("This field is required.")
        return company_address

    def clean_website(self):
        website = self.cleaned_data.get("website").strip()
        if website == "":
            raise forms.ValidationError("This field is required.")
        return website

    def save(self, user, skills, languages,country,region,city, commit=True):
        profile = super(FreelancerProfileForm, self).save(commit=False)
        print "******** entered save "
        if commit:
            profile.user = user
            profile.country = country
            profile.region = region
            profile.city = city
            profile.save()
            try:
                profile.language_proficiency.add(languages)
            except:
                for language in languages:
                    profile.language_proficiency.add(language)
            try:
                profile.skills.add(skills)
            except:
                for skill in skills:
                    profile.skills.add(skill)
        return user


class FreelancerActiveProfileForm(forms.ModelForm):
    """
    Form for adding profile of freelancer
    """
    professional_title = forms.CharField(max_length=30)


    class Meta:
        model = FreelancerProfile
        exclude = ['user', 'language_proficiency', 'skills', 'sub_category', 'address', 'profile_category','slug']

    def clean_professional_title(self):
        professional_title = self.cleaned_data.get("professional_title")
        professional_title = professional_title.strip()
        if professional_title == "":
            raise forms.ValidationError("This field is required.")
        return professional_title

    def clean_about_me(self):
        about_me = self.cleaned_data.get("about_me")
        about_me = about_me.strip()
        if about_me == "":
            raise forms.ValidationError("This field is required.")
        return about_me

    def save(self, user, skills, sub_category_object, categories,  commit=True):
        profile = super(FreelancerActiveProfileForm, self).save(commit=False)
        print "******** entered save "
        if commit:
            profile.user = user
            profile.profile_category = categories
            profile.save()
            user.active_account = 'FLR'
            g = Group.objects.get(name='Freelancers')
            g.user_set.add(user)
            user.save()
            for sub_category in sub_category_object:
                print '*****1', sub_category
                profile.sub_category.add(sub_category)
            try:
                profile.skills.add(skills)
            except:
                for skill in skills:
                    profile.skills.add(skill)
        return user


class FreelancerOverviewEdit(forms.ModelForm):
    """
     form to edit overview  of freelancer profile
    """
    about_me = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = FreelancerProfile
        exclude = ['user', 'professional_title', 'skills', 'language_proficiency', 'address', 'profile_pic',
                   'sub_category', 'profile_category','country','region','city','slug']

    def __init__(self, *args, **kwargs):
        super(FreelancerOverviewEdit, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['about_me'].widget.attrs['cols'] = '15'
            self.fields['about_me'].widget.attrs['class'] = 'overview_text_area'



    # def clean_about_me(self):
    #     about_me = self.cleaned_data.get("about_me")
    #     about_me = about_me.strip()
    #     print "about_me::", len(str(about_me))
    #     class MyHTMLParser(HTMLParser):
    #         # def handle_starttag(self, tag, attrs):
    #         #     print "Encountered a start tag:", tag
    #         #
    #         # def handle_endtag(self, tag):
    #         #     print "Encountered an end tag :", tag
    #
    #         def handle_data(self, data):
    #             data_length =  len(str(data))
    #             print data_length
    #
    #             # if data_length < 100:
    #             #     raise forms.ValidationError("100 words minumum required")
    #
    #     parser = MyHTMLParser()
    #     data_length =parser.feed(about_me)
    #     print "parseer", data_length
    #     return about_me

    def save(self, user, commit=True):
        profile_overview = super(FreelancerOverviewEdit, self).save(commit=False)
        print "******** entered save "
        if commit:
            print "****************** overview commit"
            profile_overview.user = user
            profile_overview.save()
        return user


class EditFreelancerPicForm(forms.ModelForm):
    """
    Form for Edit profile of freelancer
    """
    class Meta:
        model = FreelancerProfile
        exclude = ['user', 'slug', 'about_me', 'skills', 'language_proficiency', 'address','sub_category',
                   'profile_category', 'country', 'region', 'city']

    def save(self, user, country, region, city, commit=True):
        profile = super(EditFreelancerPicForm, self).save(commit=False)
        if commit:
            profile.user = user
            profile.country = country
            profile.region = region
            profile.city = city
            profile.save()
            print "saved"
        return user


class EditFreelancerSkillsForm(forms.ModelForm):
    """
    Form for Edit profile of freelancer
    """

    class Meta:
        model = FreelancerProfile
        exclude = ['user','slug', 'about_me', 'language_proficiency', 'address', 'profile_pic', 'professional_title', 'skills','sub_category','profile_category']

    def save(self, user, skills, commit=True):
        profile = super(EditFreelancerSkillsForm, self).save(commit=False)
        if commit:
            profile.user = user
            profile.save()
            for skill in profile.skills.all():
                profile.skills.remove(skill)
            print "commit"
            try:
                profile.skills.add(skills)
            except:
                for skill in skills:
                    profile.skills.add(skill)
            print "saved"
        return user


class EditFreelancerProfileForm(forms.ModelForm):
    """
    Form for Edit profile of freelancer
    """

    class Meta:
        model = FreelancerProfile
        exclude = ['user','slug', 'about_me', 'skills', 'language_proficiency','profile_category']

    def save(self, user, skills, languages, commit=True):
        profile = super(EditFreelancerProfileForm, self).save(commit=False)
        if commit:
            profile.user = user
            profile.save()
            try:
                profile.language_proficiency.add(languages)
            except:
                for language in languages:
                    profile.language_proficiency.add(language)
            try:
                profile.skills.add(skills)
            except:
                for skill in skills:
                    profile.skills.add(skill)
        return user




