from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models

from ckeditor.fields import RichTextField


class MailList(models.Model):
    """
    UserMailingListName model...
    """
    email = models.EmailField(unique=True)


class UserMailList(models.Model):
    """
    UserMailingList model.
    """
    mailing_name = models.CharField(_('Email template name'), max_length=255)
    mail_list = models.ManyToManyField(MailList, related_name='user_mailing_list')

    def __str__(self):
        return self.mailing_name

    class Meta:
        ordering = ['-id']
        verbose_name = "User mail list"
        verbose_name_plural = "User mail lists"


class EmailTemplate(models.Model):
    """
    EmailTemplate model.
    """
    template_name = models.CharField(_('Email template name'), max_length=255)
    subject = models.CharField(_('Email subject'), max_length=255)
    html_content = RichTextField()
    text_content = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.template_name

    class Meta:
        ordering = ['-publish_date']
        verbose_name = "Email template"
        verbose_name_plural = "Email templates"


class NewsletterCampaign(models.Model):
    """
    MailSetCampaign model.
    """
    SEND_OPTIONS = (('send', 'send'),
                    ('sent', 'sent'),)
    user_list = models.ForeignKey(UserMailList)
    template = models.ForeignKey(EmailTemplate)
    is_publish = models.BooleanField(default=False)
    publish_date = models.DateTimeField(auto_now=True)
    is_sent = models.CharField(_('Is sent'), max_length=255, choices=SEND_OPTIONS, default='send')
    sent_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user_list.mailing_name

    class Meta:
        ordering = ['-publish_date']
        verbose_name = "Newsletter campaign"
        verbose_name_plural = "Newsletter campaigns"
