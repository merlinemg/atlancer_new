from apps.blogs.models import Blogs, BlogCategory
from django.db.models import Q


class BlogMixin(object):


    def get_related_blog(self,slug):
        """
        Get related blog.
        :return:blog object
        """
        blog = Blogs.objects.get(slug=slug)
        return blog


    def get_blog_query_set(self):
        """
        Get  blogs query.
        :return:query string
        """
        category = self.request.POST.get('categ')
        all = self.request.POST.get('all')
        query = self.request.POST.get('search')
        all_query = {}
        all_query['category'] = category
        all_query['all'] = all
        all_query['query'] = query
        return all_query

    def get_related_bolg_on_category(self, category):
        """
        Get related blogs.
        :return:blogs object
        """
        try:
            obj = BlogCategory.objects.get(category=category)
            blogs = Blogs.objects.filter(blog_category__exact=obj)
        except:
            if category == 'all':
                blogs = Blogs.objects.all()
            else:
                blogs = []
        return blogs

    def get_all_blogs(self):
        """
        Get all blogs.
        :return:blogs object
        """
        all_blogs = Blogs.objects.all()
        return all_blogs

    def get_all_blogs_category(self):
        """
        Get all category.
        :return:category object
        """
        categs = BlogCategory.objects.all()
        return categs

    def get_related_blogs(self, blogs, query):
        """
        Get related blogs.
        :return:blogs object
        """
        sort = blogs.filter(
            Q(blog_title__icontains=query) | Q(blog_title__contains=query) | Q(description__startswith=query) | Q(
                description__contains=query) | Q(blog_category__category__contains=query) | Q(
                blog_category__category__icontains=query) | Q(
                blog_title__contains=query))
        return sort
