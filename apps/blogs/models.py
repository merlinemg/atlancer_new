from ckeditor.fields import RichTextField
from django.db import models

# Create your models here.
from django.db import models
from django.conf import settings
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill


class BlogCategory(models.Model):
    category = models.CharField(max_length=200,verbose_name='Blog Category')

    def __str__(self):
        return self.category

class Blogs(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='blog_user',
                             verbose_name="User")
    blog_title = models.CharField(verbose_name='Blog Title',max_length=200)
    blog_category = models.ForeignKey(BlogCategory,related_name='categories',verbose_name='category')
    no_comments = models.PositiveIntegerField(verbose_name='Number of Comments',default=0)
    blog_date = models.DateField(verbose_name='Date', auto_now_add=True)
    description = RichTextField()
    blog_pic = models.ImageField(upload_to='blog_picture', max_length=100)
    blog_pic_thumbnail = ImageSpecField(source='blog_pic',processors=[ResizeToFill(700, 300)], format='JPEG',options={'quality': 500})
    slug = models.SlugField(max_length=255, unique=True,auto_created=True,editable=False)

    def save(self, *args, **kwargs):
        super(Blogs, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.blog_title) + "-" + str(unique_id)
            self.save()

    def __str__(self):
        return self.blog_title

    class Meta:
        verbose_name = "Blog"
        verbose_name_plural = "Blogs"



