from django.contrib import admin

# Register your models here.
from django.contrib import admin
from apps.blogs.models import Blogs, BlogCategory

admin.site.register(Blogs)
admin.site.register(BlogCategory)

