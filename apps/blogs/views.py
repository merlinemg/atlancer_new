import json

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template.defaultfilters import truncatechars
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from apps.blogs.models import Blogs, BlogCategory

from apps.blogs.mixins import BlogMixin



class BlogView(BlogMixin,TemplateView):
    template_name = 'blog.html'

    def get(self, request, *args, **kwargs):
        flag=1
        latest_blog = self.get_all_blogs().order_by('-id')[:4]
        categs = self.get_all_blogs_category()

        return render(request,'blog.html',{'blogs':self.get_all_blogs(),'categs':categs,'blog_latest':latest_blog,'flag':flag})

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        blogs = self.get_related_bolg_on_category(request.POST.get('data'))
        if request.POST.get('search'):
            blogs = self.get_related_blogs(blogs,request.POST.get('search'))


        response_data={}
        response_data['status'] = True
        response_data['html_content'] = render_to_string('ajax/sorted_blog.html',
                                                         {'blogs': blogs})
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class BlogDetailView(BlogMixin,TemplateView):
    template_name = 'blog_detail.html'

    def get(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        blog = self.get_related_blog(slug)
        latest_blog = self.get_all_blogs().order_by('-id')[:4]
        return render(request, 'blog_detail.html',{'blog': blog,'latest_blog':latest_blog})

