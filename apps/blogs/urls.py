from django.conf.urls import url
from django.views.decorators.gzip import gzip_page

from apps.blogs.views import BlogView,BlogDetailView

urlpatterns = [
    url(r'^$', gzip_page(BlogView.as_view()), name='blogs'),
    url(r'^blogs_detail/(?P<slug>[\w-]+)/$',  gzip_page(BlogDetailView.as_view()), name='blogs_detail'),

]