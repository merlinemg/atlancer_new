from __future__ import unicode_literals
from __future__ import unicode_literals

import json

from channels import Group
import random

from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django_countries import settings
from django.db import models

from apps.accounts.models import User
from apps.clients.settings import MSG_TYPE_MESSAGE
from apps.knowledgebase.models import Skills
from apps.profile.models import ClientProfile, FreelancerProfile
import datetime

from apps.freelancer.models import WorkCategories

from apps.freelancer.models import WorkSubCategories



class ClientJobs(models.Model):
    """
    Client Jobs model.
    """

    JOB_STATUS = (
        ('OPEN', 'Open'),
        ('CLOSE', 'Close'),
        ('DRAFT', 'Draft'),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='client_jobs',
                             verbose_name="User")
    job_number = models.IntegerField(_('Job Number'),default=0)
    job_date = models.DateTimeField(verbose_name='Date', auto_now_add=True)
    job_title = models.CharField(_('Job title'), max_length=255)
    job_main_category = models.ForeignKey(WorkCategories, related_name='job_main_category',
                                          verbose_name="Job Category")
    job_sub_category = models.ManyToManyField(WorkSubCategories, related_name='job_sub_category',
                                              verbose_name="Job sub Category")
    job_description = models.TextField(_('Job Description'),validators=[MinLengthValidator(00)])
    payment_type = models.CharField(_('PaymentType'), max_length=255)
    payment = models.IntegerField(_('Payment'), default=0)
    duration_in_hour = models.IntegerField(_('Duration In Hour'), default=0, blank=True, null=True)
    duration_in_weak = models.IntegerField(_('Duration In Weak'), default=0, blank=True, null=True)
    duration_in_month = models.IntegerField(_('Duration In Month'), default=0, blank=True, null=True)
    exp_level = models.CharField(_('Desired Experience Level'), max_length=255, blank=True, null=True)
    no_of_hires = models.IntegerField(_('Number of Hires'), default=0)
    job_skills = models.ManyToManyField(Skills, related_name='job_skills',
                                        verbose_name="Job Skill")
    qualifications = models.CharField(_('Qualifications Required'), max_length=255, blank=True, null=True)
    job_close_date = models.DateField(_('Job close date'), max_length=255, default=datetime.date.today)
    status = models.CharField(max_length=6,
                              choices=JOB_STATUS,
                              default='OPEN', verbose_name="Job status")
    slug = models.SlugField(max_length=255, unique=True,auto_created=True,editable=False)

    def __str__(self):
        return "{0}::{1}".format(self.user.username, self.job_title)

    def save(self, *args, **kwargs):
        super(ClientJobs, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.job_title) + "-" + unique_id
            self.save()

    class Meta:
        ordering = ['-job_date']
        verbose_name = "Client Job"
        verbose_name_plural = "Client Jobs"
        default_permissions = ('add', 'change', 'delete', 'view')

    @property
    def websocket_group(self):
        """
        Returns the Channels Group that sockets should subscribe to to get sent
        messages as they are generated.
        """
        return Group("room-%s" % self.id)

    def send_message(self, message, user,date,msg_type=MSG_TYPE_MESSAGE):
        """
        Called to send a message to the room on behalf of a user.
        """
        user_details = User.objects.get(id=user.id)

        if user.active_account == 'CL':
            clientimage = ClientProfile.objects.get(user=user_details)
        else:
            clientimage = FreelancerProfile.objects.get(user=user_details)
        if clientimage.profile_pic:
            url = clientimage.profile_pic.url
        else:
            url ='/static/images/no_image.png'
        final_msg = {'room': str(self.id), 'message': message,'date':date, 'username': user.username,'msg_type': msg_type,'image':url}

        # Send out the message to everyone in the room
        self.websocket_group.send(
            {"text": json.dumps(final_msg)}
        )


class MyJobs(models.Model):
    """
    Client Jobs model.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='my_jobs',
                             verbose_name="Employment history")
    job = models.ForeignKey(ClientJobs, related_name='jobs',
                            verbose_name="Client Job")

    class Meta:
        verbose_name = "MyJob"
        verbose_name_plural = "MyJobs"
        default_permissions = ('add', 'change', 'delete', 'view')


class ApplyJob(models.Model):
    """
    Client Apply Jobs model.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='applied_user',
                             verbose_name="User")

    job = models.ForeignKey(ClientJobs, related_name='applied_jobs',
                            verbose_name="Job")

    description = models.TextField(verbose_name='Description', blank=True, null=True)
    estimated_cost = models.CharField(_('Estimated cost'), max_length=255, blank=True, null=True)
    resume = models.FileField(upload_to='resume_upload', max_length=100, blank=True, null=True)

    def __str__(self):
        return "{0}::{1}".format(self.user.username, self.job.job_title)

    class Meta:
        verbose_name = "ApplyJob"
        verbose_name_plural = "ApplyJobs"



class ApplyJobCancel(models.Model):
    """
    Cancel applied job
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='cancel_applied_user',
                             verbose_name="User")

    job = models.ForeignKey(ClientJobs, related_name='cancel_applied_jobs',
                            verbose_name="Job")

    cancel_message = models.TextField(verbose_name='Cancel_message', blank=True, null=True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "ApplyJobCancel"
        verbose_name_plural = "ApplyJobCancels"



class JobStatus(models.Model):
    """
    Client Job approved status.
    """
    status = (
        ('Shortlist', 'Shortlist'),
        ('Hire', 'Hire'),
        ('Declined', 'Declined'),
    )
    user = models.ForeignKey(FreelancerProfile, related_name='applied_user_name',
                             verbose_name="User")

    job = models.ForeignKey(ClientJobs, related_name='applied_jobs_name',
                            verbose_name="Job")
    status = models.CharField(max_length=255, choices=status, null=True)

    def __str__(self):
        return self.user.user.username

    class Meta:
        verbose_name = "JobStatus"
        verbose_name_plural = "JobStatus"



class JobInvitation(models.Model):
    """
    Client Job invitation details.
    """

    user = models.ForeignKey(FreelancerProfile, related_name='invited_user_name',
                             verbose_name="User", null=True)
    job = models.ForeignKey(ClientJobs, related_name='invited_jobs_name', verbose_name="Job")
    invite = models.BooleanField(default=False)
    is_accepted = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    is_rejected_by_client = models.BooleanField(default=False)
    messege = models.TextField(blank=True, null=True)
    reject_messege = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user.user.username

    class Meta:
        verbose_name = "JobInvitation"
        verbose_name_plural = "JobInvitations"



class JobMesseges(models.Model):
    """
    Job releted comment post section
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comment_user',
                             verbose_name="User")
    messege_to = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comment_to_user',
                                   verbose_name="User_to", null=True)
    job = models.ForeignKey(ClientJobs, related_name='comment_jobs_name', verbose_name="Job")
    post_date = models.DateTimeField(verbose_name='Date', auto_now_add=True)
    messege = models.TextField(verbose_name='messege', blank=True, null=True)
    attachment_file = models.FileField(upload_to='communiction_attach', null=True, blank=True)
    read_status = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Job message"
        verbose_name_plural = "Job messages"


    def __str__(self):
        return self.user.username

