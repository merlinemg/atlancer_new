import json
import datetime
import os
import random
import re
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from django.db.models import Q, Max, Min
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.generic import TemplateView, View
from django.shortcuts import render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import update_session_auth_hash
from django.http import HttpResponseRedirect

from apps.accounts.models import User
from apps.clients.forms import PostJobForm, ClientProfileSecondForm, ClientProfileForm, CompanyDescriptionForm, \
    ResetPasswordForm, CompanyDetailsForm, UpdateJobWithdrawnForm
from apps.clients.models import ClientJobs, ApplyJob, JobStatus, JobInvitation, JobMesseges
from apps.freelancer.general import check_group_required, my_decorator
from apps.freelancer.models import HourlyRate
from apps.knowledgebase.models import Skills
from apps.freelancer.models import WorkCategories
from apps.freelancer.models import WorkSubCategories
from apps.clients.forms import ApplyJobForm, ApplyJobUpdateForm, ApplyJobCancelForm, PostMessege
from apps.clients.models import ApplyJob
from cities_light.models import Country, Region, City
from apps.profile.models import FreelancerProfile, ClientProfile
from apps.freelancer.forms import HourlyRateForm, ExperienceLevelForm

from apps.clients.mixins import ClientMixin
from apps.profile.forms import FreelancerActiveProfileForm

from apps.freelancer.forms import ExperienceLevelForm, HourlyRateForm
from apps.general.utils import UtilMixin


class FetchJobDetailsView(ClientMixin, TemplateView):
    @csrf_exempt
    def post(self, request, *args, **kwargs):
        if self.request.is_ajax():
            response_data = {}
            if request.POST.get('job_details') == '0':
                freelancers_users = ''
                all_jobs = self.get_my_jobs()
                main_category = self.get_all_workCategoty()
                sub_category = self.get_all_sub_category()
                form = PostJobForm()
                context = {'main_category': main_category, 'sub_category': sub_category,
                           'skills': self.get_all_skills(),
                           'freelancers_users': freelancers_users, 'all_jobs': all_jobs, 'form': form}
                response_data['html_content'] = render_to_string('ajax/postJob_details.html', context)
                response_data['status'] = True
            else:
                job = self.get_current_job(request.POST.get('job_details'))
                form = PostJobForm(instance=job)
                skills = self.get_all_skills()
                sub_category = self.get_all_sub_category()
                main_category = WorkCategories.objects.all()
                for skill in job.job_skills.all():
                    skills = skills.exclude(skill_name=skill.skill_name)
                for category in job.job_sub_category.all():
                    sub_category = job.job_main_category.work_sub_categories.exclude(
                        sub_category_title=category.sub_category_title)
                response_data['status'] = True
                response_data['html_content'] = render_to_string('ajax/postJob_details.html',
                                                                 {'main_category': main_category,
                                                                  'sub_category': sub_category, 'skills': skills,
                                                                  'form': form, 'job': job})

            return HttpResponse(json.dumps(response_data), content_type="application/json")


class FreelancerActiveView(ClientMixin, TemplateView):
    template_name = 'client-freelancer.html'
    form_class = FreelancerActiveProfileForm
    exp_level_form_class = ExperienceLevelForm
    hourly_rate_form = HourlyRateForm

    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FreelancerActiveView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FreelancerActiveView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        context['exp_level_form'] = self.exp_level_form_class()
        context['main_categories'] = self.get_all_workCategoty()
        context['skills'] = self.get_all_skills()
        return context

    def get(self, request, *args, **kwargs):
        if self.has_freelancer_profile():
            return redirect('freelancer_profile')
        if self.request.is_ajax():
            return HttpResponse(self.get_category_wise_subcat(template="ajax/freelancer_active_subcategory.html"),
                                content_type="application/json")
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            response_data = {}
            print "*******request.POST", request.POST
            form = self.form_class(request.POST, request.FILES)
            exp_level_form = self.exp_level_form_class(request.POST)
            sub_categories = request.POST.getlist('sub_category')
            print "sub_categories*******", sub_categories
            sub_category_object = WorkSubCategories.objects.filter(id__in=sub_categories)
            hourly_rate_form = self.hourly_rate_form(request.POST)
            context = self.get_context_data(**kwargs)
            category = self.get_category(request.POST.get('profile_category'))
            print "sub_category_object*******", sub_category_object
            if exp_level_form.is_valid() and hourly_rate_form.is_valid() and form.is_valid():
                form.save(request.user, self.get_selected_skill(request.POST.getlist('skills')), sub_category_object,
                          category)
                hourly_rate_form.save(request.user)
                exp_level_form.save(request.user)
                response_data['status'] = True
            else:
                print "form", form.errors.as_text()
                context['form'] = form
                response_data['status'] = False
                response_data['errors'] = form.errors
            return HttpResponse(json.dumps(response_data), content_type="application/json")


class PostJobView(ClientMixin, UtilMixin, TemplateView):
    @method_decorator(login_required)
    @method_decorator(my_decorator('add_clientjobs'))
    def dispatch(self, *args, **kwargs):
        return super(PostJobView, self).dispatch(*args, **kwargs)

    def get(self, request):
        freelancers_users = ''

        all_jobs = self.get_my_jobs()
        if request.is_ajax():
            return HttpResponse(self.get_category_wise_subcat(template="ajax/sub_category_post_job.html"),
                                content_type="application/json")
        if request.GET.get("invite_id"):
            invited_id = request.GET.get("invite_id")
            freelancers_users = self.get_current_freelancer(invited_id)
            if freelancers_users.user == request.user:
                return redirect('/')
                # all_jobs = self.get_my_jobs()

        template_name = 'postJob.html'
        main_category = self.get_all_workCategoty()
        sub_category = self.get_all_sub_category()
        context = {'main_category': main_category, 'sub_category': sub_category, 'skills': self.get_all_skills(),
                   'freelancers_users': freelancers_users, 'all_jobs': all_jobs}
        return TemplateResponse(request, template_name, context)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            response_data = {}
            form = PostJobForm(request.POST)
            query = self.get_query_set()
            if form.is_valid():
                try:
                    job_category = self.get_category(query['job_main_category'])
                    job_sub_category = self.get_selected_sub_catrories(query['job_sub_category'])
                    skills = self.get_selected_job_skill(query['job_skills_obj'])
                    job_id = form.save(request.user, job_category, job_sub_category, skills)
                    if request.POST.get('invite_form'):
                        invite_query = self.get_query_set()
                        freelancer_id = invite_query['freelancer_id']
                        messege = invite_query['messege']
                        job_details = self.get_current_job(job_id.id)
                        freelancers_users = self.get_current_freelancer(freelancer_id)
                    if not self.if_job_invitation(freelancers_users, job_details):
                        job_invitation = self.send_new_invitation(freelancers_users, job_details, messege)
                    response_data['status'] = True
                except:
                    pass
            else:
                print form.errors
                response_data['errors'] = form.errors
                response_data['status'] = False
            return HttpResponse(json.dumps(response_data), content_type="application/json")


class FindDetailsView(ClientMixin, UtilMixin, TemplateView):
    """
    Displaying list of client Job Details and proposals

    """

    @csrf_exempt
    @method_decorator(login_required)
    @method_decorator(my_decorator('change_clientjobs'))
    def get(self, request, **kwargs):
        applied_freelancer_array = []
        template_name = 'jobDetails.html'
        job = self.get_current_job(kwargs.get("slug"))
        job_status = self.get_all_job_status()
        job_invitation = self.get_all_job_invitation()
        main_category = self.get_all_workCategoty()
        sub_category = self.get_all_sub_category()
        message_form = PostMessege()
        messegeDetails_arrray = []
        messegeDetailsInvite_arrray = []
        cancelledUser_arrray = []
        avalibilty_hour_array = []
        skills = self.get_all_skills()
        for skill in job.job_skills.all():
            skills = skills.exclude(skill_name=skill.skill_name)
        for category in job.job_sub_category.all():
            sub_category = job.job_main_category.work_sub_categories.exclude(
                sub_category_title=category.sub_category_title)
        applied_jobs = self.get_user_applied_jobs()
        invited_jobs = self.get_client_invited_jobs(job)
        for inv_job in invited_jobs:
            avalibilty_hour_array.append(self.get_freelancer_hourly_rate(user=inv_job.user.user.id))
        hired_jobs = self.get_client_hired_jobs(job)

        if request.is_ajax():
            return HttpResponse(self.get_category_wise_subcat(template="ajax/sub_category_post_job.html"),
                                content_type="application/json")

        applied_user_list = self.get_related_applyjob(job.id)
        job_cancelled_list = self.get_cancelled_applyjob(job.id)
        for cancel_user in job_cancelled_list:
            cancelledUser_arrray.append(int(cancel_user.user.id))

        for user in applied_user_list:
            applied_freelancer_array.append(self.get_related_freelancers(user.user))
        for user_id in applied_freelancer_array:
            for users in user_id:
                messegeDetails_arrray.append(self.get_messeges_userlist(kwargs.get("slug"), users.user.id))
        for users_in in invited_jobs:
            messegeDetailsInvite_arrray.append(self.get_messeges_userlist(kwargs.get("slug"), users_in.user.user.id))
        print "avalibilty_hour", avalibilty_hour_array
        for i in avalibilty_hour_array:
            print type(i)
            print i.values()
        context = {'jobs': job, 'applied_freelancer': applied_freelancer_array, 'job_status': job_status,
                   'job_invitation': job_invitation, 'main_category': main_category, 'sub_category': sub_category,
                   'applied_user_list': applied_user_list, 'skills': skills, 'invited_jobs': invited_jobs,
                   'hired_jobs': hired_jobs, 'message_form': message_form, 'messegeDetails': messegeDetails_arrray,
                   "messageDetailsInvite": messegeDetailsInvite_arrray,
                   'avalibilty_hour': avalibilty_hour_array, 'cancelledUser_arrray': cancelledUser_arrray}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.is_ajax():
            try:
                postFormType = self.request.POST['form_type']
            except:
                postFormType = self.request.POST['postFormType']
            if postFormType == 'status_update':
                job_details = self.get_current_job(kwargs.get("slug"))
                query = self.get_status_query_set()
                if query['job_id']:
                    freelancers_users = FreelancerProfile.objects.filter(pk__in=query['freelancers_id'])
                    for freelancers in freelancers_users:
                        job = self.if_job_status(freelancers, job_details)
                        if job:
                            self.create_job_status(freelancers, query['status'], job_details, job.id)
                        else:
                            self.create_job_status(freelancers, query['status'], job_details)
                            response_data = {'status': True}
            elif postFormType == 'invitaion_cancel':
                response_data = {}
                query = self.get_invitation_reject_query()
                self.reject_invitation(query['invitaion_id'], query['invitaion_messege'])
                response_data = {'status': True}


            elif postFormType == 'message_send':
                print "*************", "\n"
                response_data = {}
                user_send = request.user
                message_from = request.POST.get('message_from')
                job = self.get_current_job(request.POST.get('job_id'))
                form = PostMessege(request.POST, request.FILES)
                query = self.request.POST.get('freelancer_id')
                freelancer = FreelancerProfile.objects.get(id=query)
                if message_from == 'freelancer':
                    messege_to = self.get_user_details(job.user.id)
                else:
                    messege_to = self.get_user_details(freelancer.user.id)

                if form.is_valid():
                    form.save(user_send, job, messege_to)
                query = self.get_message_ajax_query()
                job_id = query.get('job_id')
                job = self.get_current_job(job_id)
                freelancer_id = request.POST.get('freelancer_id')
                freelancer = FreelancerProfile.objects.get(id=freelancer_id)
                if request.POST.get('message_type') == 'freelancer_client':
                    job_message = self.get_messeges(job.id, freelancer.user)
                else:
                    job_message = self.get_messages_client_freelancer(job.slug, freelancer)
                # job_message = self.get_messeges(job.id)
                message_form = PostMessege()
                jobs = self.get_current_job(request.POST.get('job_id'))
                response_data = {'status': True}
                response_data['html_content'] = render_to_string('ajax/job_message_ajax.html',
                                                                 {'job_message': job_message,
                                                                  'applied_job_details': jobs,
                                                                  'message_form': message_form,
                                                                  'freelancer': freelancer},
                                                                 context_instance=RequestContext(request))

                return HttpResponse(json.dumps(response_data), content_type="application/json")
            else:
                job_details = self.get_current_job(kwargs.get("slug"))
                form = PostJobForm(request.POST, instance=job_details)
                query = self.get_status_query_set()
                job_main_category = query.get('job_main_category')
                sub_category = query.get('sub_category')
                skills_obj = query.get('job_skills_obj')
                if form.is_valid():
                    try:
                        for skill in job_details.job_skills.all():
                            job_details.job_skills.remove(skill)
                        for category in job_details.job_sub_category.all():
                            job_details.job_sub_category.remove(category)
                        job_category = self.get_category(job_main_category)
                        job_sub_category = self.get_selected_sub_catrories(sub_category)
                        skills = self.get_selected_job_skill(skills_obj)
                        job_instance = form.save(request.user, job_category, job_sub_category, skills)
                        response_data['status'] = True
                        response_data['job_slug'] = job_instance.slug
                    except:
                        message = "sorry!"
                else:
                    response_data['errors'] = form.errors
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        else:
            postFormType = self.request.POST['postFormType']
            slug = self.kwargs['slug']
            job = self.get_current_job(slug)
            user_send = request.user
            applied_freelancer_array = []
            template_name = 'jobDetails.html'
            job = self.get_current_job(kwargs.get("slug"))
            job_status = self.get_all_job_status()
            job_invitation = self.get_all_job_invitation()
            main_category = self.get_all_workCategoty()
            sub_category = self.get_all_sub_category()
            message_form = PostMessege()
            messegeDetails_arrray = []
            messegeDetailsInvite_arrray = []
            avalibilty_hour_array = []
            cancelledUser_arrray = []

            applied_jobs = self.get_user_applied_jobs()
            invited_jobs = self.get_client_invited_jobs(job)
            hired_jobs = self.get_client_hired_jobs(job)
            for inv_job in invited_jobs:
                avalibilty_hour_array.append(self.get_availability_hour(user=inv_job.user.user.id))
            if postFormType == 'message_send':
                form = PostMessege(request.POST, request.FILES)
                query = self.get_messege_query_set()
                messege_to = self.get_user_details(query['messege_to'])
                if form.is_valid():
                    form.save(user_send, job, messege_to)
                    return HttpResponseRedirect(self.request.get_full_path())

            applied_user_list = self.get_related_applyjob_slug(kwargs.get("slug"))
            job_cancelled_list = self.get_cancelled_applyjob_slug(kwargs.get("slug"))
            for cancel_user in job_cancelled_list:
                cancelledUser_arrray.append(int(cancel_user.user.id))
            for user in applied_user_list:
                applied_freelancer_array.append(self.get_related_freelancers(user.user))
            for user_id in applied_freelancer_array:
                for users in user_id:
                    messegeDetails_arrray.append(self.get_messeges_userlist(kwargs.get("slug"), users.user.id))

            for users_in in invited_jobs:
                messegeDetailsInvite_arrray.append(
                    self.get_messeges_userlist(kwargs.get("slug"), users_in.user.user.id))

            context = {'jobs': job, 'applied_freelancer': applied_freelancer_array, 'job_status': job_status,
                       'job_invitation': job_invitation, 'main_category': main_category, 'sub_category': sub_category,
                       'applied_user_list': applied_user_list, 'skills': Skills.objects.all(),
                       'invited_jobs': invited_jobs,
                       'hired_jobs': hired_jobs, 'message_form': message_form, 'messegeDetails': messegeDetails_arrray,
                       "messageDetailsInvite": messegeDetailsInvite_arrray, 'avalibilty_hour': avalibilty_hour_array,
                       'cancelledUser_arrray': cancelledUser_arrray}
            return TemplateResponse(request, template_name, context)

        applied_user_list = self.get_related_applyjob(kwargs.get("job_id"))
        for user in applied_user_list:
            applied_freelancer_array.append(self.get_related_freelancers(user.user))
        context = {'jobs': job, 'applied_freelancer': applied_freelancer_array, 'job_status': job_status,
                   'job_invitation': job_invitation, 'main_category': main_category, 'sub_category': sub_category,
                   'applied_user_list': applied_user_list, 'skills': Skills.objects.all(), 'invited_jobs': invited_jobs,
                   'hired_jobs': hired_jobs, 'message_form': message_form}
        return TemplateResponse(request, template_name, context)




        # def Usermail(self, request, choose_job, messege):
        #     choose_job = choose_job
        #     messege = messege

        # reset1 = get_object_or_404(User, pk=user_id)
        # subject = reset1.username
        # from_email = reset1.email
        # to = reset1.email
        # temp = get_template('logon/mailtemplate.html')
        # html_content = temp.render(subject)
        # if subject and from_email:
        #     try:
        #         msg = EmailMessage(subject, html_content, from_email,[to])
        #         msg.attach_file('/home/spericorn/package/loginapp/logon/123.png')
        #         msg.content_subtype = "html"
        #         msg.send()
        #     except:
        #         return "Attachment error"
        #     return HttpResponseRedirect('/logon/result')
        # else:
        #     return HttpResponse('Make sure all fields are entered and valid.')


class FindJobView(ClientMixin, TemplateView):
    """
    Displaying list of freelancers

    """
    ajax_paginate_count = 2

    # @csrf_exempt
    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super(FindJobView, self).dispatch(*args, **kwargs)
    @csrf_exempt
    def get(self, request):
        try:
            job_amt = ClientJobs.objects.earliest('payment')
            amountStart = job_amt.payment
        except:
            amountStart = 0
        try:
            job_amts = ClientJobs.objects.latest('payment')
            amountEnd = job_amts.payment
        except:
            amountEnd = 1000
        amt_array = [int(amountStart), int(amountEnd)]
        template_name = 'find_job.html'
        if request.user.is_superuser:
            all_jobs = self.get_all_jobs()
        else:
            if request.user.is_authenticated() and request.user.active_account == "CL":
                all_jobs = self.get_my_jobs()
            elif request.user.is_authenticated() and request.user.active_account == "FLR":
                all_jobs = self.get_all_jobs().exclude(user=request.user)
            elif request.user.is_authenticated() and request.user.active_account == "SITE":
                all_jobs = self.get_all_jobs()
            else:
                all_jobs = self.get_all_jobs()
        closed_status = ['DRAFT', 'CLOSE']
        check_status = self.check_job_status()
        all_jobs = all_jobs.exclude(status__in=closed_status)
        categories = self.get_all_workCategoty()
        search = request.GET.get('search_type')
        all_jobs = all_jobs.order_by('-id')
        sorted_job = []
        if search:
            search = search.split(',')
            for search_string in search:
                print search_string
                if search_string:
                    search_string = str(search_string)
                    jobs = all_jobs.filter(
                        Q(job_title__in=search_string) | Q(job_title=search_string) | Q(
                            job_title__icontains=search_string) | Q(
                            job_title__contains=search_string) | Q(
                            job_description__icontains=search_string) | Q(job_description__contains=search_string) | Q(
                            job_description__in=search_string) | Q(
                            job_skills__skill_name__icontains=search_string) | Q(
                            job_skills__skill_name=search_string) | Q(
                            job_skills__skill_name__contains=search_string) | Q(
                            job_skills__skill_name__in=search_string)).distinct()
                    for job in jobs:
                        if job not in sorted_job:
                            sorted_job.append(job)
            all_jobs = sorted_job
        if request.is_ajax():
            closed_status = ['DRAFT', 'CLOSE']
            all_jobs = self.get_all_jobs()
            visible_jobs = all_jobs.exclude(status__in=closed_status)
            action = request.GET.get('action')
            keyword_value = request.GET.get('keyword_value')
            collected_list = []
            key_words = self.get_job_search_query_list()
            get_list = []
            if action == 'query_related':
                if keyword_value:
                    get_list = []
                    for query in key_words:
                        query_1 = query.upper()
                        keyword_value = keyword_value.upper()
                        if query_1 == keyword_value or keyword_value in query_1:
                            if query not in get_list:
                                get_list.append(query)
                response_data = {}
                if len(get_list) > 10:
                    get_list = get_list[:10]
                response_data['html_content'] = render_to_string('ajax/get_related_list.html', {'lists': get_list})
                return HttpResponse(json.dumps(response_data), content_type="application/json")
        # paginator = Paginator(all_jobs, 5)
        # page = request.GET.get('page')
        # try:
        #     job_list = paginator.page(page)
        # except PageNotAnInteger:
        #     job_list = paginator.page(1)
        # except EmptyPage:
        #     job_list = paginator.page(paginator.num_pages)

        context = {'all_jobs': all_jobs, 'categories': categories, 'key_words': self.get_job_search_query_list(),
                   'amt_array': amt_array, 'amountEnd': amountEnd}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        if request.is_ajax():
            response_data = {}
            query = self.get_sort_query_set()
            categories = query.get('categories')
            job_type = query.get('job_type')
            experience = query.get('experience')
            amountstartInput = query.get('amountstartInput')
            amountendInput = query.get('amountendInput')
            job_tabs = request.POST.get('job_tabs')
            job_status = request.POST.get('job_status')
            quote_array = []
            quotes_submit = []
            # c = request.POST.getlist('durations')
            if request.user.is_superuser:
                all_jobs = self.get_all_jobs()
            else:
                if request.user.is_authenticated() and request.user.active_account == "CL":
                    all_jobs = self.get_my_jobs()
                elif request.user.is_authenticated() and request.user.active_account == "FLR":
                    quotes_submit = self.get_quotes_submit_status()
                    all_jobs = self.get_all_jobs().exclude(user=request.user)
                elif request.user.is_authenticated() and request.user.active_account == "SITE":
                    all_jobs = self.get_all_jobs()
                else:
                    all_jobs = self.get_all_jobs()

            # categories only
            closed_status = ['DRAFT', 'CLOSE']
            if job_status:
                if job_status == 'CLOSE' or job_status == 'DRAFT':
                    all_jobs = all_jobs.filter(status=job_status)
                else:
                    all_jobs = all_jobs.exclude(status__in=closed_status)
                    all_jobs = all_jobs.filter(status=job_status)

            elif not job_status and job_tabs == 'applied-job' or job_tabs == 'invited-job' or job_tabs == 'hire-job':
                all_jobs = all_jobs
            else:
                all_jobs = all_jobs.exclude(status__in=closed_status)

            if categories and experience == [] and job_type == []:
                category_obj = self.get_realated_category(categories)
                all_jobs = all_jobs.filter(job_main_category__in=category_obj)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)
            # job_type only
            elif categories == [] and experience == [] and job_type:
                all_jobs = all_jobs.filter(payment_type__in=job_type)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)
                print all_jobs
            # experience only
            elif categories == [] and experience and job_type == []:
                all_jobs = all_jobs.filter(exp_level__in=experience)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)
                print all_jobs
                # categories and  job_type
            elif categories and not experience and job_type:
                category_obj = WorkCategories.objects.filter(pk__in=categories)
                all_jobs = all_jobs.filter(job_main_category__in=category_obj).filter(
                    payment_type__in=job_type)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)
                print all_jobs
            # categories and experience
            elif categories and experience and not job_type:
                category_obj = WorkCategories.objects.filter(id__in=categories)
                all_jobs = all_jobs.filter(job_main_category__in=category_obj).filter(
                    exp_level__in=experience)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)
                print all_jobs
            # experience and job_type
            elif not categories and experience and job_type:
                all_jobs = all_jobs.filter(payment_type__in=job_type).filter(
                    exp_level__in=experience)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)
                print all_jobs
            # all
            elif categories and experience and job_type:
                category_obj = WorkCategories.objects.filter(id__in=categories)
                all_jobs = all_jobs.filter(job_main_category__in=category_obj).filter(
                    exp_level__in=experience).filter(payment_type__in=job_type)
                if amountstartInput or amountendInput:
                    all_jobs = self.amount_filter(amountendInput, amountstartInput, all_jobs)

                print all_jobs

            elif amountstartInput or amountendInput:
                if categories == [] and experience == [] and job_type == []:
                    amountendInput = int(amountendInput)
                    amountstartInput = int(amountstartInput)
                    sorted_jobs = []
                    for job in all_jobs:
                        if job.payment <= amountendInput and job.payment >= amountstartInput:
                            sorted_jobs.append(job.id)
                    all_jobs = all_jobs.filter(id__in=sorted_jobs)

            else:
                all_jobs = all_jobs

            if job_tabs == 'applied-job':
                all_jobs = ApplyJob.objects.filter(user=request.user).filter(job__in=all_jobs).order_by('-id')
                ajax_template = 'ajax/sorted_applyJob_list.html'

            elif job_tabs == 'invited-job':
                all_jobs = JobInvitation.objects.filter(user__user=request.user).filter(job__in=all_jobs).order_by(
                    '-id')
                ajax_template = 'ajax/sorted_inviteJob_list.html'
                if quotes_submit:
                    for quote in quotes_submit:
                        quote_array.append(int(quote.job.id))

            else:
                ajax_template = 'ajax/sorted_list.html'

            if not request.user.is_authenticated() or request.user.is_anonymous():
                user = None
            else:
                user = True
            # print all_jobs
            response_data['html_content'] = render_to_string(ajax_template,
                                                             {'all_jobs': all_jobs,
                                                              'request': request, 'sorted_jobs': all_jobs,
                                                              'user': request.user, 'quote_array': quote_array,
                                                              'quotes_submit': quotes_submit})

            return HttpResponse(json.dumps(response_data), content_type="application/json")

        else:
            template_name = 'find_job.html'
            skill = request.POST.get('skill')
            experiance = request.POST.get('experiance')
            skills = Skills.objects.filter(Q(skill_name__icontains=skill) | Q(skill_name__startswith=skill))

            if skill == 'Skill' and experiance != 'none':
                all_jobs = ClientJobs.objects.filter(
                    Q(exp_level__icontains=experiance) | Q(exp_level__startswith=experiance))
                message = self.create_message(all_jobs, experiance)

            elif experiance == 'none' and skill != 'Skill':
                all_jobs = ClientJobs.objects.filter(skills=skills)
                message = self.create_message(all_jobs, skill)

            elif experiance != 'none' and skill != 'Skill':
                all_jobs = ClientJobs.objects.filter(
                    Q(exp_level__icontains=experiance) | Q(exp_level__startswith=experiance)).filter(skills=skills)
                query = skill + " and " + experiance
                message = self.create_message(all_jobs, query)

            else:
                all_jobs = ClientJobs.objects.all()
                message = ""

            paginator = Paginator(all_jobs, 20)

            page = request.GET.get('page')
            try:
                job_list = paginator.page(page)
            except PageNotAnInteger:
                job_list = paginator.page(1)
            except EmptyPage:
                job_list = paginator.page(paginator.num_pages)

            context = {'all_jobs': job_list, 'message': message}
            return TemplateResponse(request, template_name, context)


class ClientDetailList(ClientMixin, TemplateView):
    """
    Displaying list of client Job Details and proposals

    """

    @csrf_exempt
    @method_decorator(login_required)
    def get(self, request):
        template_name = 'clientDetails.html'
        all_jobs = self.get_my_jobs()

        categories = self.get_all_workCategoty()

        all_freelancers = self.get_all_Freelancers()
        freelan_categories = self.get_all_workCategoty()
        rates = self.get_hourly_rate()

        if request.is_ajax():
            response_data = {}
            slug = request.GET.get('user_id')
            freelancer_details = FreelancerProfile.objects.select_related('user').get(slug=slug)
            all_jobs = ClientJobs.objects.select_related('user').filter(user=request.user)
            response_data['html_content'] = render_to_string('ajax/invitemodal.html',
                                                             {'freelancer_details': freelancer_details,
                                                              'all_jobs': all_jobs},
                                                             context_instance=RequestContext(request))
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        context = {'all_jobs': all_jobs, 'categories': categories, 'all_freelancers': all_freelancers,
                   'freelan_categories': freelan_categories, 'rates': rates}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get('invite_form'):
                response_data = {}
                query = self.get_query_set()
                messege = query.get('messege')
                choose_job = query.get('choose_job')
                freelancer_id = query.get('freelancer_id')
                job_details = self.get_current_job(choose_job)
                freelancers_users = self.get_current_freelancer(freelancer_id)
                if not self.if_job_invitation(freelancers_users, job_details):
                    self.send_new_invitation(freelancers_users, job_details, messege)
                    response_data = {'status': True}
                else:
                    response_data = {'status': False}
                return HttpResponse(json.dumps(response_data), content_type="application/json")

        else:
            template_name = 'clientDetails.html'
            all_jobs = self.get_my_jobs()
            message = ""
            context = {'all_jobs': all_jobs, 'message': message}
            return TemplateResponse(request, template_name, context)

    def create_message(self, all_jobs, query):
        count = 0
        for i in all_jobs:
            count = count + 1
        message = str(count) + "  result on  " + query
        return message

    def amount_filter(self, amountendInput, amountstartInput, sort_object):
        try:
            sort_object = sort_object.filter(payment__lte=amountendInput).filter(payment__gte=amountstartInput)
        except:
            pass
        return sort_object


class ClientProfileView(ClientMixin, TemplateView):
    @csrf_exempt
    @method_decorator(login_required)
    # @method_decorator(user_passes_test(lambda u: u.has_perm('profile.view_clientprofile'), login_url='/permission_error/'))
    @method_decorator(my_decorator('view_clientprofile'))
    @method_decorator(my_decorator('change_clientprofile'))
    def get(self, request, *args, **kwargs):
        if self.get_active_profile() == 'FLR':
            return redirect('add_freelancer_profile')
        template_name = 'ClientProfile.html'
        user = request.user
        countries = self.get_all_countries()
        client = self.get_client_profile()
        regions = None
        cities = None
        if client:
            try:
                regions = self.get_related_regions(user.client_profile.country.name)
                cities = self.get_related_city(user.client_profile.region.name)
            except:
                pass
            client_profile_form = ClientProfileForm(instance=user)
            client_profile_second_form = ClientProfileSecondForm(instance=client)
            company_details_form = CompanyDetailsForm(instance=client)
            company_description_form = CompanyDescriptionForm(instance=client)
        else:
            client = ClientProfile.objects.get_or_create(user=user)
            client_profile_form = ClientProfileForm(instance=user)
            client_profile_second_form = ClientProfileSecondForm()
            company_details_form = CompanyDetailsForm()
            company_description_form = CompanyDescriptionForm()
        reset_password_form = ResetPasswordForm()

        context = {'user': user, 'countries': countries, 'regions': regions, 'cities': cities, 'client': client,
                   'company_description_form': company_description_form,
                   'company_details_form': company_details_form, 'client_profile_form': client_profile_form,
                   'reset_password_form': reset_password_form, 'client_profile_second_form': client_profile_second_form}
        return TemplateResponse(request, template_name, context)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        response_data = {}
        print "@@@@@@", request.POST
        user = request.user
        client = self.get_client_profile()
        client_profile_second_form = ClientProfileSecondForm(request.POST, request.FILES, instance=client)
        client_profile_form = ClientProfileForm(request.POST, instance=user)
        company_details_form = CompanyDetailsForm(request.POST, instance=client)
        company_description_form = CompanyDescriptionForm(request.POST, instance=client)
        reset_password_form = ResetPasswordForm(request.POST, instance=user)
        address = self.get_address()
        country = address.get('country')
        region = address.get('region')
        city = address.get('city')

        if company_details_form.is_valid():
            company_details_form.save()
            response_data['company_detail'] = True
            response_data['html_company'] = render_to_string('ajax/company_details.html',
                                                             {'user': user, 'client': client})

        else:
            response_data['company_errors'] = company_details_form.errors
            response_data['company_detail'] = False

        if company_description_form.is_valid():
            company_description_form.save()
            response_data['company_description'] = True
            response_data['html_overview'] = render_to_string('ajax/overview.html', {'user': user, 'client': client})
        else:
            response_data['company_description'] = False
            response_data['description_error'] = company_description_form.errors

        if reset_password_form.is_valid():
            response_data['valid'] = True
            password = reset_password_form.cleaned_data['password']
            user = reset_password_form.save(password)
            update_session_auth_hash(request, user)
        else:
            response_data['valid'] = False
            response_data['errors'] = reset_password_form.errors

        if client_profile_second_form.is_valid():
            if client_profile_form.is_valid():
                flag = client_profile_form.cleaned_data['profile_form_id']
                if flag == "profile_form_id":
                    response_data['client_profile'] = True
                    client_profile_form.save()
                    client_profile_second_form.save(country, region, city)
                    response_data['html_profile'] = render_to_string('ajax/client_profile.html',
                                                                     {'user': user, 'client': client})
            else:
                response_data['errors'] = client_profile_form.errors
        else:
            response_data['errors'] = client_profile_second_form.errors
            response_data['client_profile'] = False
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class JobDetailView(ClientMixin, TemplateView):
    template_name = 'job-detailedview.html'

    # @method_decorator(my_decorator('add_applyjob'))
    @method_decorator(my_decorator('add_applyjob'))
    @method_decorator(my_decorator('change_applyjob'))
    @method_decorator(my_decorator('add_applyjobcancel'))
    def get(self, request, *args, **kwargs):
        job_details = self.get_current_job(self.kwargs['slug'])
        if job_details.user == request.user:
            return redirect('/permission_error/')
        form = ApplyJobForm()
        user_apply_job = self.if_job_applied(request.user, job_details)
        form_cancel_job = ApplyJobCancelForm()
        if user_apply_job:
            form_update = ApplyJobForm(instance=user_apply_job)
        else:
            form_update = ApplyJobForm()
        applied_freelancer = self.get_all_Freelancers()
        try:
            applied_jobs = self.get_related_applyjob(job_details)
        except:
            applied_jobs = []
        try:
            job_applied = self.if_job_applied(request.user, job_details)
        except:
            job_applied = []
        job_cancel = self.if_job_applied_cancel(request.user, job_details)
        context = {'job_details': job_details, 'form': form, 'applied_jobs': applied_jobs,
                   'form_cancel_job': form_cancel_job, 'applied_freelancer': applied_freelancer,
                   'job_applied': job_applied, 'form_update': form_update, 'job_cancel': job_cancel}
        return TemplateResponse(request, self.template_name, context)

    @method_decorator(my_decorator('change_applyjob'))
    def post(self, request, *args, **kwargs):

        slug = self.kwargs['slug']
        print "slug", slug
        job_details = self.get_current_job(slug)
        applied_freelancer = self.get_all_Freelancers()
        job_applied = []
        is_freelancer = ''
        user = request.user
        username = user.username
        group = self.get_freelancer_group()
        users = group.user_set.all()
        for user1 in users:
            if user1.username == username:
                is_freelancer = True
        is_user = self.if_job_applied(user, job_details)
        applied_jobs = self.get_related_applyjob(job_details)
        message = ''
        success_message = ''
        appliedJob_update = self.request.POST['appliedJob_update']
        form_cancel_job = ApplyJobCancelForm()
        if appliedJob_update == 'update':
            form = ApplyJobForm()
            user_apply_job = self.if_job_applied(request.user, job_details)
            form_update = ApplyJobUpdateForm(request.POST, request.FILES, instance=user_apply_job)
            if form_update.is_valid():
                is_resume = request.POST.get('is_resume')
                form_update.save(commit=True, apply_job=user_apply_job, is_resume=is_resume)
                form_update = ApplyJobForm(instance=user_apply_job)
                job_applied = self.if_job_applied(request.user, job_details)
            else:
                print "Errorrrr........."

        elif appliedJob_update == 'cancel_application':
            form = ApplyJobForm()
            form_update = ApplyJobForm()
            form_cancel_job = ApplyJobCancelForm(request.POST)
            if form_cancel_job.is_valid():
                form_cancel_job.save(user, job_details)
                success_message = "You are Canceled your application"
            else:
                print form_cancel_job.errors
            job_applied = self.if_job_applied(request.user, job_details)
        else:
            form = ApplyJobForm(request.POST, request.FILES)
            if request.user.is_authenticated():
                if request.user.active_account == "FLR":
                    if form.is_valid():
                        if is_user == '':
                            job_applied = form.save(user, job_details, request.POST.get('is_resume'))
                            success_message = "You are successfully applied"
                        else:
                            message = "You are already applied for this Job"
                else:
                    message = "Please login as freelancer to apply job"
            else:
                return redirect('/accounts/login/?next=' + request.path)

            user_apply_job = self.if_job_applied(request.user, job_details)
            if user_apply_job:
                form_update = ApplyJobForm(instance=user_apply_job)
            else:
                form_update = ApplyJobForm()
        job_cancel = self.if_job_applied_cancel(request.user, job_details)
        context = {'job_details': job_details, 'login_message': message, 'job_applied': job_applied, 'form': form,
                   'form_update': form_update, 'applied_jobs': applied_jobs, 'form_cancel_job': form_cancel_job,
                   'applied_freelancer': applied_freelancer, 'success_message': success_message,
                   'job_cancel': job_cancel}
        return TemplateResponse(request, self.template_name, context)


class MyJobView(ClientMixin, TemplateView):
    """
    Displaying list of freelancer job list

    """

    @csrf_exempt
    @method_decorator(login_required)
    # @method_decorator(user_passes_test(lambda u: u.has_perm('clients.view_myjobs'), login_url='/permission_error/'))
    @method_decorator(my_decorator('view_myjobs'))
    def dispatch(self, *args, **kwargs):
        return super(MyJobView, self).dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        template_name = 'my_job.html'
        user = self.get_related_freelancers(request.user)
        applied_jobs = self.get_user_applied_jobs().order_by('-id')
        invited_jobs = self.get_user_invited_jobs(user)
        hired_jobs = self.get_user_hired_jobs(user)
        applied_status = self.get_applied_status(user)
        quotes_submit = self.get_quotes_submit_status()
        quote_cancelled = self.get_quotes_cancel_status()
        categories = WorkCategories.objects.all()
        quote_array = []
        quote_cancel_array = []
        for quote in quotes_submit:
            quote_array.append(int(quote.job.id))
        for quote_cancel in quote_cancelled:
            quote_cancel_array.append(int(quote_cancel.job.id))
        message_form = PostMessege()
        messegeDetails_arrray = []
        messegeApplied_arrray = []
        sendQuoteStatus = []
        for userApply in applied_jobs:
            messegeApplied_arrray.append(self.get_messeges(userApply.job.id))
        for userInvite in invited_jobs:
            job_instance = self.get_current_job(userInvite.job.slug)
            messegeDetails_arrray.append(self.get_messeges(userInvite.job.id))
            sendQuoteStatus.append(self.get_user_applied_status(job_instance))
        try:
            job_amt = ClientJobs.objects.earliest('payment')
            amountStart = job_amt.payment
        except:
            amountStart = 0
        try:
            job_amts = ClientJobs.objects.latest('payment')
            amountEnd = job_amts.payment
        except:
            amountEnd = 1000
        amt_array = [int(amountStart), int(amountEnd)]
        context = {'applied_jobs': applied_jobs, 'invited_jobs': invited_jobs,
                   'hired_jobs': hired_jobs, 'messegeDetails': messegeDetails_arrray,
                   'messegeApplied_arrray': messegeApplied_arrray, 'message_form': message_form,
                   'sendQuoteStatus': sendQuoteStatus, "applied_status": applied_status,
                   "quotes_submit": quote_array, "quote_cancel_array": quote_cancel_array, 'categories': categories,
                   'amt_array': amt_array, 'amountEnd': amountEnd}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get('decline-this-job'):
                response_data = {}
                query = self.get_accept_query()
                choose_job = query.get('accept_job_id')
                freelancer_id = query.get('accept_user')
                job_details = self.get_current_job(choose_job)
                freelancers_users = self.get_current_freelancer(freelancer_id)
                if self.if_job_invitation(freelancers_users, job_details):
                    self.decline_invitation(freelancers_users, job_details)
                    response_data = {'status': True}
                else:
                    response_data = {'status': False}
                return HttpResponse(json.dumps(response_data), content_type="application/json")

            elif request.POST.get('accept-this-job'):
                response_data = {}
                query = self.get_accept_query()
                choose_job = query.get('accept_job_id')
                freelancer_id = query.get('accept_user')
                job_details = self.get_current_job(choose_job)
                freelancers_users = self.get_current_freelancer(freelancer_id)
                if self.if_job_invitation(freelancers_users, job_details):
                    self.accept_invitation(freelancers_users, job_details)
                    response_data = {'status': True}
                else:
                    response_data = {'status': False}
                return HttpResponse(json.dumps(response_data), content_type="application/json")
                # elif request.POST.get('post_method'):
                #     if request.POST.get('post_method') == 'message_client_console':
                #         response_data = {}
                #         query = self.get_message_ajax_query()
                #         job_id = query.get('job_id')
                #         freelancer_id = request.POST.get('freelancer_id')
                #         freelancer = FreelancerProfile.objects.get(id=freelancer_id)
                #         job_message = self.get_messages_client_freelancer(job_id, freelancer)
                #         message_form = PostMessege()
                #         jobs = self.get_current_job(job_id)
                #         response_data = {'status': True}
                #
                #         response_data['html_content'] = render_to_string('ajax/job_message_ajax.html',
                #                                                          {'job_message': job_message,
                #                                                           'applied_job_details': jobs,
                #                                                           'message_form': message_form,
                #                                                           'freelancer': freelancer},
                #                                                          context_instance=RequestContext(request))
                #         return HttpResponse(json.dumps(response_data), content_type="application/json")
                #
                #
                #     else:
                #
                #         response_data = {}
                #         query = self.get_message_ajax_query()
                #         job_id = query.get('job_id')
                #         jobs = self.get_current_job(job_id)
                #         if request.user.active_account == "FLR":
                #
                #             freelancer = FreelancerProfile.objects.get(user=request.user)
                #         job_message = self.get_messeges(jobs.id, freelancer.user)
                #         message_form = PostMessege()
                #         freelancer = FreelancerProfile.objects.get(user=request.user)
                #         response_data = {'status': True}
                #
                #         response_data['html_content'] = render_to_string('ajax/job_message_ajax.html',
                #                                                          {'job_message': job_message,
                #                                                           'applied_job_details': jobs,
                #                                                           'message_form': message_form,
                #                                                           'freelancer': freelancer},
                #                                                          context_instance=RequestContext(request))
                #         print response_data
                #         return HttpResponse(json.dumps(response_data), content_type="application/json")
                # #     elif request.POST.get('post_message'):
                #         response_data = {}
                #         user_send = request.user
                #         job = self.get_current_job(request.POST.get('job_id'))
                #         form = PostMessege(request.POST, request.FILES)
                #         query = self.get_messege_query_set()
                #         messege_to = self.get_user_details(query['messege_to'])
                #         if form.is_valid():
                #             form.save(user_send, job, messege_to)
                #         job_message = self.get_messeges(job.id)
                #         message_form = PostMessege()
                #         jobs = self.get_current_job(request.POST.get('job_id'))
                #         response_data = {'status': True}
                #         response_data['html_content'] = render_to_string('ajax/job_message_ajax.html',
                #                                                          {'job_message': job_message,
                #                                                           'applied_job_details': jobs,
                #                                                           'message_form': message_form},
                #                                                          context_instance=RequestContext(request))
                #         return HttpResponse(json.dumps(response_data), content_type="application/json")
                #
                # else:
                #     postFormType = self.request.POST['postFormType']
                #     if postFormType == 'message_send':
                #         form = PostMessege(request.POST, request.FILES)
                #         query = self.get_messege_details_query_set()
                #         user_send = request.user
                #         messege_to = self.get_user_details(query['messege_to'])
                #         job_instance = self.get_current_job(query['job_id'])
                #         appliedStatus = self.get_user_applied_status(job_instance)
                #         # if not appliedStatus:
                #         #     # create_job_apply = self.create_job_apply(job_instance)
                #         if form.is_valid():
                #             form.save(user_send, job_instance, messege_to)
                #             return HttpResponseRedirect(self.request.get_full_path())
                #             template_name = 'my_job.html'
                #             user = self.get_related_freelancers(request.user)
                #             applied_jobs = self.get_user_applied_jobs()
                #             invited_jobs = self.get_user_invited_jobs(user)
                #             hired_jobs = self.get_user_hired_jobs(user)
                #             applied_status = self.get_applied_status(user)
                #             quotes_submit = self.get_quotes_submit_status()
                #             quote_cancelled = self.get_quotes_cancel_status()
                #             quote_array = []
                #             quote_cancel_array = []
                #             for quote in quotes_submit:
                #                 quote_array.append(int(quote.job.id))
                #             for quote_cancel in quote_cancelled:
                #                 quote_cancel_array.append(int(quote_cancel.job.id))
                #             message_form = PostMessege()
                #             messegeDetails_arrray = []
                #             messegeApplied_arrray = []
                #             sendQuoteStatus = []
                #             for userApply in applied_jobs:
                #                 messegeApplied_arrray.append(self.get_messeges(userApply.job.id))
                #             for userInvite in invited_jobs:
                #                 job_instance = self.get_current_job(userInvite.job.id)
                #                 messegeDetails_arrray.append(self.get_messeges(userInvite.job.id))
                #                 sendQuoteStatus.append(self.get_user_applied_status(job_instance))
                #
                #             context = {'applied_jobs': applied_jobs, 'invited_jobs': invited_jobs,
                #                        'hired_jobs': hired_jobs, 'messegeDetails': messegeDetails_arrray,
                #                        'messegeApplied_arrray': messegeApplied_arrray, 'message_form': message_form,
                #                        'sendQuoteStatus': sendQuoteStatus, "applied_status": applied_status,
                #                        "quotes_submit": quotes_submit, "quote_cancel_array": quote_cancel_array}
                #             return TemplateResponse(request, template_name, context)


class MyJobDetails(ClientMixin, TemplateView):
    """
    Displaying  job details

    """
    template_name = 'my_job_details.html'

    @csrf_exempt
    @method_decorator(login_required)
    @method_decorator(my_decorator('add_applyjob'))
    def dispatch(self, *args, **kwargs):
        return super(MyJobDetails, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        slug = self.kwargs['slug']
        job = self.get_current_job(slug)
        jobs_id = job.id
        no_hired_freelancer = self.get_hires_user(jobs_id)
        no_applicants = self.get_applied_user(jobs_id)
        applied_freelancer = self.get_all_Freelancers()
        messege = self.get_messeges(jobs_id)
        message_form = PostMessege()
        is_freelancer = ''
        user = request.user
        username = user.username
        group = self.get_freelancer_group()
        users = group.user_set.all()
        for user1 in users:
            if user1.username == username:
                is_freelancer = True
        is_user = self.if_job_applied(user, job)
        applied_jobs = self.get_related_applyjob(job)
        message = ''
        success_message = ''
        postFormType = self.request.POST['postFormType']
        form_cancel_job = ApplyJobCancelForm()
        try:
            job_applied = self.if_job_applied(request.user, job)
        except:
            job_applied = []
        if postFormType == 'message_send':
            form = PostMessege(request.POST, request.FILES)
            query = self.get_messege_query_set()
            if form.is_valid():
                messege_to = self.get_user_details(query['messege_to'])
                saved_id = form.save(user, job, messege_to)
                return HttpResponseRedirect(self.request.get_full_path())

        if postFormType == 'update':
            form = ApplyJobForm()
            user_apply_job = self.if_job_applied(request.user, job)
            form_update = ApplyJobUpdateForm(request.POST, request.FILES, instance=user_apply_job)

            if form_update.is_valid():
                form_update.save(commit=True, apply_job=user_apply_job, is_resume=request.POST.get('is_resume'))
                return HttpResponseRedirect(self.request.get_full_path())
            else:
                print "Errorrrr........."
        elif postFormType == 'delete_withdrawn':
            user_cancel_quote = self.if_job_withdrawn(request.user, job)
            if user_cancel_quote:
                user_cancel_quote.delete()
                return HttpResponseRedirect(self.request.get_full_path())
        elif postFormType == 'update_cancel_quote':
            job_withdrawn = self.if_job_withdrawn(request.user, job)
            form_withdrawn = UpdateJobWithdrawnForm(request.POST)
            if form_withdrawn.is_valid():
                form_withdrawn.save(user, job, job_withdrawn)
                return HttpResponseRedirect(self.request.get_full_path())
            else:
                print form_withdrawn.errors

        elif postFormType == 'cancel_application':
            form = ApplyJobForm()
            form_update = ApplyJobForm()
            form_cancel_job = ApplyJobCancelForm(request.POST)
            if form_cancel_job.is_valid():
                form_cancel_job.save(user, job)
                success_message = "You are Canceled your application"
                return HttpResponseRedirect(self.request.get_full_path())
            else:
                print form_cancel_job.errors
        else:
            form = ApplyJobForm(request.POST, request.FILES)
            if request.user.is_authenticated():
                if request.user.active_account == "FLR":
                    if form.is_valid():
                        if is_user == '':
                            form.save(user, job)
                            success_message = "You are successfully applied"
                            return HttpResponseRedirect(self.request.get_full_path())
                        else:
                            message = "You are already applied for this Job"
                else:
                    message = "Please login as freelancer to apply job"
            else:
                return redirect('/accounts/login/')

            user_apply_job = self.if_job_applied(request.user, job)
            if user_apply_job:
                form_update = ApplyJobForm(instance=user_apply_job)
            else:
                form_update = ApplyJobForm()
        job_cancel = self.if_job_applied_cancel(request.user, job)

        if job_cancel:
            form_withdrawn = UpdateJobWithdrawnForm(instance=job_cancel)
        else:
            form_withdrawn = UpdateJobWithdrawnForm()
        context = {'job_details': job, 'login_message': message, 'job_applied': job_applied, 'form': form,
                   'form_update': form_update, 'form_withdrawn': form_withdrawn, 'applied_jobs': applied_jobs,
                   'form_cancel_job': form_cancel_job,
                   'applied_freelancer': applied_freelancer, 'success_message': success_message,
                   'job_cancel': job_cancel, 'no_hired_freelancer': len(no_hired_freelancer),
                   'no_applicants': len(no_applicants), 'messegeDetails': messege, 'message_form': message_form,
                   'tabactive': 'messege'}

        return TemplateResponse(request, self.template_name, context)

    def get(self, request, *args, **kwargs):
        slug = self.kwargs['slug']
        message_form = PostMessege()
        job_details = self.get_current_job(slug)
        jobs_id = job_details.id
        no_hired_freelancer = self.get_hires_user(jobs_id)
        no_applicants = self.get_applied_user(jobs_id)
        messege = self.get_messeges(jobs_id)
        appliedJobDetails = self.get_applied_job(jobs_id)
        form = ApplyJobForm()
        user_apply_job = self.if_job_applied(request.user, job_details)
        form_cancel_job = ApplyJobCancelForm()
        if user_apply_job:
            form_update = ApplyJobForm(instance=user_apply_job)
        else:
            form_update = ApplyJobForm()
        applied_freelancer = self.get_all_Freelancers()

        try:
            applied_jobs = self.get_related_applyjob(job_details)
        except:
            applied_jobs = []

        try:
            job_applied = self.if_job_applied(request.user, job_details)
        except:
            job_applied = []
        job_cancel = self.if_job_applied_cancel(request.user, job_details)
        if job_cancel:
            form_withdrawn = UpdateJobWithdrawnForm(instance=job_cancel)
        else:
            form_withdrawn = UpdateJobWithdrawnForm()
        context = {'job_details': job_details, 'no_hired_freelancer': len(no_hired_freelancer),
                   'no_applicants': len(no_applicants), 'messegeDetails': messege, 'message_form': message_form,
                   'tabactive': 'jobdetails',
                   'form': form, 'applied_jobs': applied_jobs, 'form_withdrawn': form_withdrawn,
                   'form_cancel_job': form_cancel_job, 'applied_freelancer': applied_freelancer,
                   'job_applied': job_applied, 'form_update': form_update, 'job_cancel': job_cancel,
                   'appliedJobDetails': appliedJobDetails}
        return TemplateResponse(request, self.template_name, context)


class InviteValidation(ClientMixin, TemplateView):
    template_name = 'invitemodal.html'

    def get(self, request, *args, **kwargs):
        response_data = {}
        is_invited = self.get_invited_job(request.GET.get('job'), request.GET.get('freelancer'))
        if is_invited:
            response_data['is_invited'] = True
        else:
            response_data['is_invited'] = False
        return HttpResponse(json.dumps(response_data), content_type="application/json")


class ChatView(ClientMixin, TemplateView):
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get('postFormType') == 'Savemessage':
                print request.POST.get('type')
                if request.POST.get('type') == 'client':
                    freelancers = FreelancerProfile.objects.get(id=request.POST.get('to'))
                    freelancer = freelancers.user.id

                else:
                    freelancer = request.POST.get('to')
                print "gg"

                messages = JobMesseges()

                messages.user = self.get_user_details(request.POST.get('from'))

                messages.messege_to = self.get_user_details(freelancer)
                messages.job = self.get_current_job(request.POST.get('jobid'))

                messages.messege = request.POST.get('message')

                messages = messages.save()

                payload = {'status': 'success', 'message': 'Successfully Register', 'data': ''}
            elif request.POST.get('postFormType') == 'GetMessages':
                if request.POST.get('type') == 'client':
                    freelancers = FreelancerProfile.objects.get(id=request.POST.get('to'))
                    freelancer = freelancers.user.id
                    clientimage = ClientProfile.objects.get(user=self.get_user_details(request.POST.get('from')))
                    freelancerimage = FreelancerProfile.objects.get(user=self.get_user_details(freelancer))
                else:
                    freelancer = request.POST.get('to')
                    clientimage = ClientProfile.objects.get(user=self.get_user_details(request.POST.get('to')))
                    freelancerimage = FreelancerProfile.objects.get(user=self.get_user_details(request.POST.get('from')))

                details = JobMesseges.objects.filter(Q(user=self.get_user_details(request.POST.get('from')),
                                                       messege_to=self.get_user_details(freelancer)) | Q(
                    user=self.get_user_details(freelancer), messege_to=request.POST.get('from')),
                                                     job=self.get_current_job(request.POST.get('jobid')))
                if clientimage.profile_pic:
                    clientimages =clientimage.profile_pic.url
                else:
                    clientimages = '/static/images/no_image.png'
                if freelancerimage.profile_pic:
                    freelancerimages =freelancerimage.profile_pic.url
                else:
                    freelancerimages = '/static/images/no_image.png'

                print freelancerimages
                print clientimages
                if details:
                    html = render_to_string('getchat.html',
                                            {'object_list': details, 'user': request.user.username,'clientimage':clientimages,'freelancerimage':freelancerimages})
                else:
                    html = ''

                payload = {'status': 'success', 'message': 'Successfully Register', 'data': html}

        return HttpResponse(json.dumps(payload), content_type="application/json")
