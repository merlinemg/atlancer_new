import json
import threading

from django.contrib.auth.models import Group
from django.core.mail import EmailMessage, send_mail
from django.template.loader import render_to_string

from apps.clients.models import ClientJobs, ApplyJob, JobStatus, JobInvitation, ApplyJobCancel, JobMesseges
from apps.freelancer.models import WorkCategories, HourlyRate, WorkSubCategories, Availability
from apps.hire.models import HiredJobsContract
from apps.profile.models import FreelancerProfile, ClientProfile
from apps.knowledgebase.models import Skills
from cities_light.models import Country, Region, City
from django.contrib.auth import *
from django.db.models import Q
from django.template import Context, Template
from apps.knowledgebase.models import Skills
from datetime import datetime, timedelta
from apps.sendbox.models import MailSet

User = get_user_model()


class ClientMixin(object):


    def get_job_search_query_list(self):
        """
        Get Invitation .
        :return: Invitation
        """
        closed_status = ['DRAFT', 'CLOSE']
        all_jobs = self.get_all_jobs()
        visible_jobs = all_jobs.exclude(status__in=closed_status)
        key_words = []
        for skill in Skills.objects.all():
            if skill.skill_name not in key_words:
                key_words.append(skill.skill_name)
        for main_category in WorkCategories.objects.all():
            if main_category.category_title not in key_words:
                key_words.append(main_category.category_title)
        for sub_categories in WorkSubCategories.objects.all():
            if sub_categories.sub_category_title not in key_words:
                key_words.append(sub_categories.sub_category_title)
        for jobs in visible_jobs:
            if jobs.job_title not in key_words:
                key_words.append(jobs.job_title)

        return key_words

    def check_job_status(self):
        """
        Get all invtitaion reject .
        :return: invtitaion reject query
        """
        closed_status = ['DRAFT', 'CLOSE']
        for job in ClientJobs.objects.all().exclude(status__in=closed_status):
            now = datetime.today().date()
            if job.job_close_date <= now:
                job.status = 'CLOSE'
                mail_set_client = MailSet.objects.get(module_type='admin_2_JobExpired_Client')
                template = Template(mail_set_client.html_content)
                job_url_c = settings.SERVER_URL + 'job-list-detail/' + str(job.slug)
                context = Context(
                    {'first_name': job.user.first_name, 'job_title': job.job_title,
                     'expired_date': now,'job_url':job_url_c})
                html_content = template.render(context)
                recipient_client = job.user.email

                def Email():
                    send_mail(mail_set_client.subject, mail_set_client.subject, 'tectdjango@gmail.com',
                              [str(recipient_client)], html_message=html_content)

                multyEmail = threading.Thread(name='Email', target=Email)
                multyEmail.start()
                job.save()

    def get_invited_job(self, job, freelancer):
        """
        Get Invitation .
        :return: Invitation
        """

        try:
            is_invited = JobInvitation.objects.get(user_id=freelancer, job__slug=job)
        except:
            is_invited = None
        return is_invited

    def get_message_ajax_query(self):
        """
        Get user list message .
        :return: job message query
        """
        job_id = self.request.POST.get('job_id')
        query = {}
        query['job_id'] = job_id
        return query

    def get_cancelled_applyjob(self, job_id):
        """
        Get all  cancelled ApplyJobs .
        :return:ApplyJobCancel objects
        """
        cancelled_jobs = ApplyJobCancel.objects.filter(job=job_id)
        return cancelled_jobs

    def get_cancelled_applyjob_slug(self, slug):
        """
        Get all  cancelled ApplyJobs .
        :return:ApplyJobCancel objects
        """
        cancelled_jobs = ApplyJobCancel.objects.filter(slug=slug)
        return cancelled_jobs

    get_cancelled_applyjob

    def get_availability_hour(self, user):
        """
        Get availabilty hour.
        :return:
        """
        availability_hour = Availability.objects.filter(user=user)
        return availability_hour

    def get_freelancer_hourly_rate(self, user):
        """
        Get hourly rate.
        :return:
        """
        hourly_rate = HourlyRate.objects.filter(user=user)
        return hourly_rate

    def get_quotes_cancel_status(self):
        """
        Get quotes cancel status.
        :return:
        """
        quotes_cancel = ApplyJobCancel.objects.filter(user=self.request.user)
        return quotes_cancel

    def get_quotes_submit_status(self):
        """
        Get quotes status.
        :return:
        """
        quotes_submit = ApplyJob.objects.filter(user=self.request.user)
        return quotes_submit

    def get_applied_status(self, user):
        """
        Get applied status.
        :return:
        """
        applied_status = JobStatus.objects.filter(user=user)
        return applied_status

    def get_invited_message(self, job):
        """
        Get Messages jobs.
        :return:
        """
        job_messeges = JobMesseges.objects.filter(job=job)
        return job_messeges

    def reject_invitation(self, invitation_id, reject_message):
        """
        reject invitation.
        :return: reject invitation object
        """
        JobInvitation.objects.filter(id=invitation_id).update(is_rejected_by_client=True, reject_messege=reject_message)
        job_invitation = JobInvitation.objects.filter(id=invitation_id)
        # Email to client and freelancer
        mail_set_client = MailSet.objects.get(module_type='admin_2_Rejected_ByClient_Client')
        mail_set_freelancer = MailSet.objects.get(module_type='admin_2_Rejected_ByClient_Freelancer')
        for job_invitation in job_invitation:
            recipient_freelancer = job_invitation.user.user.email
            recipient_client = job_invitation.job.user.email

            job_url_f = settings.SERVER_URL + 'my-jobsDetails/' + str(job_invitation.job.slug) + '/'
            job_url_c = settings.SERVER_URL + str(job_invitation.job.slug) + '/job-list-detail/'
            freelancer_template = Template(mail_set_freelancer.html_content)
            freelancer_context = Context(
                {'first_name': job_invitation.user.user.first_name, 'job_title': job_invitation.job.job_title,
                 'job_url': job_url_f})
            freelancer_html = freelancer_template.render(freelancer_context)

            client_template = Template(mail_set_client.html_content)
            client_context = Context(
                {'first_name': job_invitation.job.user.first_name,
                 'freelancer_name': job_invitation.user.user.first_name,
                 'job_title': job_invitation.job.job_title, 'job_url': job_url_c})
            client_html = client_template.render(client_context)

            def Email():
                send_mail(mail_set_freelancer.subject, mail_set_freelancer.subject,
                          'tectdjango@gmail.com',
                          [str(recipient_freelancer)], html_message=freelancer_html)

                send_mail(mail_set_client.subject, mail_set_client.subject, 'tectdjango@gmail.com',
                          [str(recipient_client)], html_message=client_html)

            multyEmail = threading.Thread(name='Email', target=Email)
            multyEmail.start()
        return job_invitation

    def get_invitation_reject_query(self):
        """
        Get all invtitaion reject .
        :return: invtitaion reject query
        """
        invitaion_id = self.request.POST.get('invitaion_id')
        invitaion_messege = self.request.POST.get('invitaion_messege')
        query = {}
        query['invitaion_id'] = invitaion_id
        query['invitaion_messege'] = invitaion_messege
        return query

    def get_client_hired_jobs(self, job):
        """
        Get hired jobs.
        :return:
        """
        hired = JobStatus.objects.filter(job=job, status='Hire')
        return hired

    def get_client_invited_jobs(self, job):
        """
        Get invited jobs.
        :return:
        """
        invited_jobs = JobInvitation.objects.filter(job=job)
        return invited_jobs

    def get_selected_skill(self, skills):
        """
        Get related skills.
        :return: skills object
        """
        skills_objs = Skills.objects.filter(pk__in=skills)
        return skills_objs

    def decline_invitation(self, freelancers_users, job_details):
        """
        decline invitation.
        :return: decline invitation object
        """
        JobInvitation.objects.filter(user=freelancers_users, job=job_details).update(is_accepted=False)
        job_invitation = JobInvitation.objects.filter(user=freelancers_users, job=job_details)
        job_reject = JobInvitation.objects.filter(user=freelancers_users, job=job_details).update(is_rejected=True)

        mail_set_client = MailSet.objects.get(module_type='admin_2_declineInvitation_Client')
        mail_set_freelancer = MailSet.objects.get(module_type='admin_2_declineInvitation_Freelancer')
        for job_invitation in job_invitation:
            recipient_freelancer = job_invitation.user.user.email
            recipient_client = job_invitation.job.user.email

            job_url_f = settings.SERVER_URL + 'my-jobsDetails/' + str(job_invitation.job.slug) + '/'
            job_url_c = settings.SERVER_URL + 'job-list-detail/'+ str(job_invitation.job.slug)
            freelancer_template = Template(mail_set_freelancer.html_content)
            freelancer_context = Context(
                {'first_name': job_invitation.user.user.first_name, 'job_title': job_invitation.job.job_title,
                 'job_url': job_url_f})
            freelancer_html = freelancer_template.render(freelancer_context)

            client_template = Template(mail_set_client.html_content)
            client_context = Context(
                {'first_name': job_invitation.job.user.first_name,
                 'freelancer_name': job_invitation.user.user.first_name,
                 'job_title': job_invitation.job.job_title, 'job_url': job_url_c})
            client_html = client_template.render(client_context)

            def Email():
                send_mail(mail_set_freelancer.subject, mail_set_freelancer.subject,
                          'tectdjango@gmail.com',
                          [str(recipient_freelancer)], html_message=freelancer_html)

                send_mail(mail_set_client.subject, mail_set_client.subject, 'tectdjango@gmail.com',
                          [str(recipient_client)], html_message=client_html)

            multyEmail = threading.Thread(name='Email', target=Email)
            multyEmail.start()

        return job_reject

    def accept_invitation(self, freelancers_users, job_details):
        """
        Accept Invitation.
        :return: Accept Invitation object

        """
        JobInvitation.objects.filter(user=freelancers_users, job=job_details).update(is_rejected=False)
        JobInvitation.objects.filter(user=freelancers_users, job=job_details).update(is_accepted=True)
        job_invitation = JobInvitation.objects.filter(user=freelancers_users, job=job_details)
        mail_set_client = MailSet.objects.get(module_type='admin_2_acceptInvitation_Client')
        mail_set_freelancer = MailSet.objects.get(module_type='admin_2_acceptInvitation_Freelancer')
        for job_invitation in job_invitation:
            recipient_freelancer = job_invitation.user.user.email
            recipient_client = job_invitation.job.user.email

            job_url_f = settings.SERVER_URL + 'my-jobsDetails/' + str(job_invitation.job.slug) + '/'
            job_url_c = settings.SERVER_URL + 'job-list-detail/'+ str(job_invitation.job.slug)
            freelancer_template = Template(mail_set_freelancer.html_content)
            freelancer_context = Context(
                {'first_name': job_invitation.user.user.first_name, 'job_title': job_invitation.job.job_title,
                 'job_url': job_url_f})
            freelancer_html = freelancer_template.render(freelancer_context)

            client_template = Template(mail_set_client.html_content)
            client_context = Context(
                {'first_name': job_invitation.job.user.first_name,
                 'freelancer_name': job_invitation.user.user.first_name,
                 'job_title': job_invitation.job.job_title, 'job_url': job_url_c})
            client_html = client_template.render(client_context)

            def Email():
                send_mail(mail_set_freelancer.subject, mail_set_freelancer.subject,
                          'tectdjango@gmail.com',
                          [str(recipient_freelancer)], html_message=freelancer_html)

                send_mail(mail_set_client.subject, mail_set_client.subject, 'tectdjango@gmail.com',
                          [str(recipient_client)], html_message=client_html)

            multyEmail = threading.Thread(name='Email', target=Email)
            multyEmail.start()
        return job_invitation

    def get_accept_query(self):
        """
        Get all accept set   JobStatus .
        :return: JobStatus query
        """
        accept_job_id = self.request.POST.get('accept_job_id')
        accept_user = self.request.POST.get('accept_user')
        query = {}
        query['accept_job_id'] = accept_job_id
        query['accept_user'] = accept_user
        return query

    def get_user_applied_status(self, job):
        """
        Get applied jobs.
        :return:
        """
        applied_jobs = ApplyJob.objects.filter(user=self.request.user.id, job=job)
        return applied_jobs

    def create_job_apply(self, job):
        """
        create   JobStatus .
        :return: JobStatus objects
        """
        user = User.objects.get(id=self.request.user.id)
        job_apply = ApplyJob(user=user, job=job)
        job_apply = job_apply.save()
        return job_apply

    def get_messege_details_query_set(self):
        """
        Get all query set   JobStatus .
        :return: JobStatus query
        """

        messege_to = self.request.POST.get('messege_to')
        job_id = self.request.POST.get('job_id')
        query = {}
        query['messege_to'] = messege_to
        query['job_id'] = job_id
        return query

    def get_applied_user(self, job):
        """
        Get applied jobs.
        :return:
        """
        applied_jobs = ApplyJob.objects.filter(job__pk=job)
        return applied_jobs

    def get_applied_job(self, job):
        applied_job_details = ApplyJob.objects.filter(job__pk=job).filter(user=self.request.user)
        return applied_job_details

    def get_messeges(self, job, freelancer_id=None):
        """
        Get job related messeges.
        :return:
        """
        jobs = ClientJobs.objects.get(id=job)
        if self.request.user.active_account == "CL":
            if freelancer_id:
                messege = JobMesseges.objects.filter(job=jobs).filter(Q(messege_to=freelancer_id))
        else:
            messege = JobMesseges.objects.filter(job=jobs).filter(
                Q(messege_to=freelancer_id) | Q(user=self.request.user))
        return messege

    def get_messeges_userlist(self, slug, user_id):
        """
        Get job related messeges.
        :return:
        """
        user = User.objects.get(id=user_id)
        messege = JobMesseges.objects.filter(job__slug=slug).filter(Q(user=user) | Q(messege_to=user))
        return messege

    def get_messages_client_freelancer(self, job, freelancer):
        """
        Get job related messeges.
        :return:
        """
        freelancer_user = User.objects.get(id=freelancer.user.id)
        client = self.request.user
        print "job", job, "client", client
        # print "********client", client
        # job_messages = JobMesseges.objects.filter(job__pk=job)
        # print "*job_messages", job_messages, "\n"
        #
        # freelancer_message = JobMesseges.objects.filter(job__pk=job).filter(Q(user=client) & Q(messege_to=freelancer_user))
        # print "*freelancer_message", freelancer_message, "\n"
        #
        # client_message = JobMesseges.objects.filter(job__pk=job).filter(Q(user=freelancer_user) & Q(messege_to=client))
        # print "*client_message", client_message, "\n"
        message = JobMesseges.objects.filter(job__slug=job).filter(
            (Q(user=client) & Q(messege_to=freelancer_user) | (Q(user=freelancer_user) & Q(messege_to=client))))
        print message
        return message

    def get_hires_user(self, job):
        """
        Get hired jobs.
        :return:
        """
        hired_users = JobStatus.objects.filter(job__pk=job, status='Hire')
        return hired_users

    def get_user_hired_jobs(self, user):
        """
        Get hired jobs.
        :return:
        """
        hired = JobStatus.objects.filter(user__in=user, status='Hire')
        return hired

    def get_user_invited_jobs(self, user):
        """
        Get invited jobs.
        :return:
        """
        invited_jobs = JobInvitation.objects.filter(user__in=user).order_by('-id')
        return invited_jobs

    def get_user_applied_jobs(self):
        """
        Get applied jobs.
        :return:
        """
        applied_jobs = ApplyJob.objects.filter(user=self.request.user).order_by('id')
        # changed order_by('-id') to order_by('id') for ordering jobs in  freelancer myjob listing
        return applied_jobs

    def get_active_profile(self):
        """
        Get active account.
        :return:
        """
        active_account = self.request.user.active_account
        print "active_accountactive_accountactive_account", active_account
        return active_account

    def has_freelancer_profile(self):
        try:
            self.request.user.freelancer_profile
            status = True
        except:
            status = False
        return status

    def if_job_applied(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = ApplyJob.objects.get(user=user, job=job_details)
        except:
            is_user = ''

        return is_user

    def if_job_withdrawn(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = ApplyJobCancel.objects.get(user=user, job=job_details)
        except:
            is_user = ''

        return is_user

    def if_job_applied_cancel(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = ApplyJobCancel.objects.get(user=user, job=job_details)
        except:
            is_user = ''
        return is_user

    def get_client_group(self):
        """
        Get all related group
        :return: objects of group
        """
        group = Group.objects.get(name="Clients")
        return group

    def get_freelancer_group(self):
        """
        Get all related group
        :return: objects of group
        """
        group = Group.objects.get(name="Freelancers")
        return group

    def get_address(self):
        """
        Get Country,Region and City
        :return: objects of Country,Region and City
        """
        country = self.request.POST.get('country')
        region = self.request.POST.get('region')
        city = self.request.POST.get('city')
        try:
            region = Region.objects.get(name=region)
        except:
            region = None
        try:
            country = Country.objects.get(name=country)
        except:
            country = None
        try:
            city = City.objects.get(name=city)
        except:
            city = None

        address = {}
        address['region'] = region
        address['country'] = country
        address['city'] = city

        return address

    def get_all_jobs(self):
        """
        Get all jobs
        :return: jobs object
        """
        all_jobs = ClientJobs.objects.select_related().all()
        return all_jobs

    def get_freelancer_applied_jobs(self):
        """
        Get freelancer applied jobs
        :return: jobs object
        """
        all_jobs = ClientJobs.objects.filter(applied_jobs__user=self.request.user).order_by('-id')
        return all_jobs

    def get_freelancer_invited_jobs(self, user):
        """
        Get freelancer invited jobs
        :return: jobs object
        """
        all_jobs = ClientJobs.objects.filter(invited_jobs_name__user=user).order_by('-id')
        return all_jobs

    def get_client_profile(self):
        """
        Get current client profile
        :return: client profile object
        """
        try:
            client = ClientProfile.objects.get(user=self.request.user)
        except:
            client = []
        return client

    def create_message(self, all_jobs, query):
        """
        Serach message
        :return: message string
        """
        count = 0
        for i in all_jobs:
            count = count + 1
        message = str(count) + "  result on  " + query
        return message

    def amount_filter(self, amountendInput, amountstartInput, sort_object):
        """
        Get all jobs related to amount .
        :return: jobs objects
        """
        try:
            sort_object = sort_object.filter(payment__lte=amountendInput).filter(payment__gte=amountstartInput)
        except:
            pass
        return sort_object

    def get_sort_query_set(self):
        """
        Get all query set   Sort .
        :return: Sort query
        """
        categories = self.request.POST.getlist('categories')
        job_type = self.request.POST.getlist('job_type')
        experience = self.request.POST.getlist('experience')
        amountstartInput = self.request.POST.get('amountstartInput')
        amountendInput = self.request.POST.get('amountendInput')
        query = {}
        query['categories'] = categories
        query['job_type'] = job_type
        query['experience'] = experience
        query['amountstartInput'] = amountstartInput
        query['amountendInput'] = amountendInput

        return query

    def get_realated_category(self, categories):
        """
        Get related   category .
        :return: category objects
        """
        categories = WorkCategories.objects.filter(pk__in=categories)
        return categories

    def get_status_query_set(self):
        """
        Get all query set   JobStatus .
        :return: JobStatus query
        """

        job_id = self.request.POST.get('job_id')
        status = self.request.POST.get('status')
        freelancers_id = self.request.POST.getlist('freelancer_user')
        job_main_category = self.request.POST.get('job_main_category')
        sub_category = self.request.POST.getlist('job_sub_category')
        skills_obj = self.request.POST.getlist('skills')
        job_skills_obj = self.request.POST.getlist('job_skills')
        query = {}
        query['job_id'] = job_id
        query['status'] = status
        query['freelancers_id'] = freelancers_id
        query['job_main_category'] = job_main_category
        query['sub_category'] = sub_category
        query['skills_obj'] = skills_obj
        query['job_skills_obj'] = job_skills_obj
        return query

    def get_messege_query_set(self):
        """
        Get all query set   JobStatus .
        :return: JobStatus query
        """

        messege_to = self.request.POST.get('messege_to')
        query = {}
        query['messege_to'] = messege_to
        return query

    def get_user_details(self, user_id):
        """
        Get current user model
        :return:
        """

        try:
            user_details = User.objects.get(id=user_id)
        except:
            user_details = []

        return user_details

    def create_job_status(self, freelancers, status, job_details, job=None):
        """
        create   JobStatus .
        :return: JobStatus objects
        """
        job_status = JobStatus(id=job, user=freelancers, status=status, job=job_details)
        job_status = job_status.save()
        return job_status

    def if_job_status(self, freelancers, job_details):
        """
        Get related   JobStatus .
        :return: JobStatus objects
        """
        try:
            job_status = JobStatus.objects.get(user=freelancers, job=job_details)
            print job_status
        except:
            job_status = []
        return job_status

    def get_related_freelancers(self, user):
        """
        Get related  FreelancerProfile .
        :return:FreelancerProfile objects
        """
        freelancers = FreelancerProfile.objects.filter(user=user)
        return freelancers

    def get_related_applyjob(self, job_id):
        """
        Get all  related ApplyJobs .
        :return:ApplyJobs objects
        """
        jobs = ApplyJob.objects.filter(job=job_id)
        return jobs

    def get_related_applyjob_slug(self, slug):
        """
        Get all  related ApplyJobs from slug.
        :return:ApplyJobs objects
        """
        jobs = ApplyJob.objects.filter(slug=slug)
        return jobs

    def get_all_countries(self):
        """
        Get all  Country .
        :return:Country objects
        """
        countries = Country.objects.all()
        return countries

    def get_related_regions(self, country):
        """
        Get related regions...
        :return:regions
        """
        regions = Region.objects.filter(country__name=country)
        return regions

    def get_related_city(self, region):
        """
        Get related cities...
        :return:cities
        """
        city = City.objects.filter(region__name=region)
        return city

    def get_all_sub_category(self):
        """
        Get all  WorkSubCategories .
        :return:WorkSubCategories objects
        """
        sub_category = WorkSubCategories.objects.all()
        return sub_category

    def get_all_job_invitation(self):
        """
        Get all  JobInvitation .
        :return:JobInvitation objects
        """
        job_invitation = JobInvitation.objects.select_related().all()
        return job_invitation

    def get_all_job_status(self):
        """
        Get all  JobStatus .
        :return:JobStatus objects
        """
        job_status = JobStatus.objects.select_related().all()
        return job_status

    def send_new_invitation(self, freelancers_users, job_details, messege):
        """
        Create new JobInvitation  .
        :return:JobInvitation objects
        """
        job_invitation = JobInvitation(user=freelancers_users, job=job_details, messege=messege,
                                       invite=True)
        job_invitation = job_invitation.save()
        return job_invitation

    def if_job_invitation(self, freelancers_users, job_details):
        """
        Get JobInvitation objects .
        :return:JobInvitation objects
        """
        is_invitation = JobInvitation.objects.select_related().filter(user=freelancers_users, job=job_details).exists()
        return is_invitation

    def get_current_freelancer(self, freealncer):
        """
        Get Current Freelancer .
        :return:Freelancer object
        """
        try:
            freealncer = FreelancerProfile.objects.get(id=freealncer)
        except:
            freealncer = FreelancerProfile.objects.get(slug=freealncer)
        return freealncer

    def get_current_job(self, slug):
        """
        Get Current Job .
        :return:job object
        """
        try:
            try:
                job = ClientJobs.objects.get(slug=slug)
            except:
                job = ClientJobs.objects.get(id=slug)
        except:
            job = []
        return job

    def get_query_set(self):
        job_main_category = self.request.POST.get('job_main_category')
        sub_category = self.request.POST.getlist('sub_category')
        job_sub_category = self.request.POST.getlist('job_sub_category')
        skills_obj = self.request.POST.getlist('skills')
        job_skills_obj = self.request.POST.getlist('job_skills')
        freelancer_id = self.request.POST.get('freelancer_id')
        messege = self.request.POST.get('messege')
        choose_job = self.request.POST.get('choose-job')
        query = {}
        query['job_main_category'] = job_main_category
        query['sub_category'] = sub_category
        query['skills_obj'] = skills_obj
        query['job_skills_obj'] = job_skills_obj
        query['freelancer_id'] = freelancer_id
        query['messege'] = messege
        query['choose_job'] = choose_job
        query['job_sub_category'] = job_sub_category
        return query

    def category_wise_sub_category(self, category_query):
        """
        Get all sub categories .
        :return:ajax page with sub category objects
        """
        category = WorkCategories.objects.get(pk=category_query)
        sub_categories = WorkSubCategories.objects.filter(work_category=category)
        html_content = render_to_string('ajax/sub_category.html', {'sub_categories': sub_categories})

        return html_content

    def get_category(self, job_main_category):
        """
        Get all  categories .
        :return:categories object
        """
        job_main_category = WorkCategories.objects.get(pk=job_main_category)
        return job_main_category

    def get_selected_sub_catrories(self, sub_categ_list):
        """
        get selected Sub category.
        :return:
        """
        catogs = WorkSubCategories.objects.filter(id__in=sub_categ_list)
        return catogs

    def get_selected_job_skill(self, skill_list):
        """
        get selected skills.
        :return:
        """
        skills = Skills.objects.filter(id__in=skill_list)
        return skills

    def get_all_skills(self):
        """
        Get all skills
        :return:
        """
        skills = Skills.objects.all()
        return skills

    def get_my_jobs(self):
        """
        Get all jobs belongs to authenticated clients.
        :return:
        """
        all_jobs = ClientJobs.objects.select_related().filter(user=self.request.user)
        return all_jobs

    def get_all_workCategoty(self):
        """
        Get all work category.
        :return:
        """
        categories = WorkCategories.objects.all()
        return categories

    def get_all_Freelancers(self):
        """
        Get all Freelancers list.
        :return:
        """
        all_freelancers = FreelancerProfile.objects.select_related().all()
        return all_freelancers

    def get_hourly_rate(self):
        """
        Get all hourly rate.
        :return:
        """
        rates = HourlyRate.objects.select_related().all()
        return rates

    def get_category_wise_subcat(self, template=None):

        response_data = {}
        category_pk = self.request.GET['action']
        if int(category_pk) > 0:
            response_data['status'] = True
            category = WorkCategories.objects.get(pk=int(category_pk))
            skills = Skills.objects.filter(work_category__in=category_pk)
            sub_categories = WorkSubCategories.objects.filter(work_category=category)
            response_data['html_content'] = render_to_string(template,
                                                             {'sub_categories': sub_categories, 'skills': skills})
        else:
            sub_categories = []
            skills = []
            response_data['status'] = False
            response_data['html_content'] = render_to_string(template,
                                                             {'sub_categories': sub_categories, 'skills': skills})
        return json.dumps(response_data)

    def get_status_contractlist(self):
        status = HiredJobsContract.STATUS
        return status

    def hireJobsContract_filter(self,user):
        if user.active_account == 'CL':
            contracts = HiredJobsContract.objects.filter(client_profile=user.client_profile)
        else:
            contracts = HiredJobsContract.objects.filter(freelancer_profile=user.freelancer_profile)
        return contracts

    def hireJobsContract_filter_status(self,user,status):
        if user.active_account == 'CL':
            contracts = HiredJobsContract.objects.filter(client_profile=user.client_profile,status=status)
        else:
            contracts = HiredJobsContract.objects.filter(freelancer_profile=user.freelancer_profile,status=status)
        return contracts

    def hireJobsContract_filter_startdate(self,user,startdate):
        if user.active_account == 'CL':
            contracts = HiredJobsContract.objects.filter(client_profile=user.client_profile,milestone=startdate)
        else:
            contracts = HiredJobsContract.objects.filter(freelancer_profile=user.freelancer_profile,milestone=startdate)
        #print contracts
        return contracts

    def hireJobsContract_filter_search(self,user,search):
        if user.active_account == 'CL':
            contracts = HiredJobsContract.objects.filter(client_profile=user.client_profile,job_title__contains=search)
        else:
            contracts = HiredJobsContract.objects.filter(freelancer_profile=user.freelancer_profile,job_title__contains=search)
        #print contracts
        return contracts


