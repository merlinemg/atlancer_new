import os
import threading

from django.core.mail import EmailMessage, send_mail
from django.core.signals import request_finished
from django.db.models.signals import pre_save, post_save
from django.dispatch import Signal, receiver
from django.template import loader
import random
from django.utils.crypto import get_random_string
from apps.clients.models import JobInvitation

from apps.clients.models import ClientJobs

from apps.sendbox.models import MailSet
from django.template import Context, Template
from apps.clients.models import ApplyJob
from new_djangoteam import settings


@receiver(pre_save, sender=JobInvitation)
def Invited_Job_Email(sender, instance, **kwargs):
    mail_set_client = MailSet.objects.get(module_type='admin_2_InvitingClient')
    mail_set_freelancer = MailSet.objects.get(module_type='admin_2_InvitedFreelancer')
    recipient_freelancer = instance.user.user.email
    recipient_client = instance.job.user.email

    job_url_f = settings.SERVER_URL + 'job-details/' + str(instance.job.slug) + '/'
    job_url_c = settings.SERVER_URL + str(instance.job.slug) + '/job-list-detail/'
    freelancer_template = Template(mail_set_freelancer.html_content)
    freelancer_context = Context(
        {'first_name': instance.user.user.first_name, 'job_title': instance.job.job_title, 'job_url': job_url_f})
    freelancer_html = freelancer_template.render(freelancer_context)

    client_template = Template(mail_set_client.html_content)
    client_context = Context(
        {'first_name': instance.job.user.first_name, 'freelancer_name': instance.user.user.first_name,
         'job_title': instance.job.job_title, 'job_url': job_url_c})
    client_html = client_template.render(client_context)

    def Email():
        send_mail(mail_set_freelancer.subject, mail_set_freelancer.subject, 'tectdjango@gmail.com',
                  [str(recipient_freelancer)], html_message=freelancer_html)

        send_mail(mail_set_client.subject, mail_set_client.subject, 'tectdjango@gmail.com',
                  [str(recipient_client)], html_message=client_html)

    multyEmail = threading.Thread(name='Email', target=Email)
    multyEmail.start()

    return sender


@receiver(pre_save, sender=ApplyJob)
def Apply_Job_Email(sender, instance, **kwargs):
    applied_jobs = ApplyJob.objects.filter(user=instance.user, job=instance.job)
    if not applied_jobs:
        mail_set_client = MailSet.objects.get(module_type='admin_2_Apply_Job_Client')
        mail_set_freelancer = MailSet.objects.get(module_type='admin_2_Apply_Job_Freelancer')
        recipient_freelancer = instance.user.email
        recipient_client = instance.job.user.email

        job_url_f = settings.SERVER_URL + 'client/job-details/' + str(instance.job.id) + '/'
        job_url_c = settings.SERVER_URL + 'client/' + str(instance.job.id) + '/job-list-detail/'
        freelancer_template = Template(mail_set_freelancer.html_content)
        freelancer_context = Context(
            {'first_name': instance.user.first_name, 'job_title': instance.job.job_title, 'job_url': job_url_f})
        freelancer_html = freelancer_template.render(freelancer_context)

        client_template = Template(mail_set_client.html_content)
        client_context = Context(
            {'first_name': instance.job.user.first_name, 'freelancer_name': instance.user.first_name,
             'job_title': instance.job.job_title, 'job_url': job_url_c})
        client_html = client_template.render(client_context)

        def Email():
            send_mail(mail_set_freelancer.subject, mail_set_freelancer.subject, 'tectdjango@gmail.com',
                      [str(recipient_freelancer)], html_message=freelancer_html)

            send_mail(mail_set_client.subject, mail_set_client.subject, 'tectdjango@gmail.com',
                      [str(recipient_client)], html_message=client_html)

        multyEmail = threading.Thread(name='Email', target=Email)
        multyEmail.start()

    return sender


@receiver(pre_save, sender=ClientJobs)
def Client_Job_Id(sender, instance, **kwargs):
    if not instance.job_number:
        lower = 100 * 1000
        upper = lower * 1000
        random_num = random.randint(lower, upper)
        instance.job_number = random_num
        instance.save()
    return sender
