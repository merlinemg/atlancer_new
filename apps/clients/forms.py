from ckeditor.widgets import CKEditorWidget
from django import forms
from django.forms import ModelForm
from django.utils.crypto import get_random_string
from django.utils.text import slugify

from apps.accounts.models import User
from apps.clients.models import ClientJobs
from apps.profile.models import ClientProfile
from django.core.exceptions import ValidationError

from apps.clients.models import ApplyJob, ApplyJobCancel, JobMesseges


class PostJobForm(forms.ModelForm):
    """
    Form to display the post job page details.
    """

    class Meta:
        model = ClientJobs
        exclude = ('user', 'job_skills', 'job_sub_category',)
        fields = ('user', 'job_title', 'job_main_category', 'duration_in_hour', 'duration_in_weak',
                  'duration_in_month', 'job_description', 'payment', 'exp_level', 'no_of_hires',
                  'qualifications', 'payment_type',  'job_close_date', 'status')

    def __init__(self, *args, **kwargs):
        super(PostJobForm, self).__init__(*args, **kwargs)
        self.fields['job_close_date'].widget.attrs.update({'class': 'datepicker'})

        for field in self.fields:
            self.fields['payment'].widget.attrs['class'] = 'pay_type'
            self.fields['duration_in_month'].widget.attrs['class'] = 'duration_input'
            self.fields['duration_in_month'].widget.attrs['rel'] = 'text'
            self.fields['duration_in_weak'].widget.attrs['class'] = 'duration_input'
            self.fields['duration_in_weak'].widget.attrs['rel'] = 'text'
            self.fields['duration_in_hour'].widget.attrs['class'] = 'duration_input'
            self.fields['duration_in_hour'].widget.attrs['rel'] = 'text'

    def clean_job_description(self):
        job_description = self.cleaned_data.get("job_description")
        job_description = job_description.strip()
        if len(job_description) < 300:
            raise forms.ValidationError("This field is required minimum 300 character.")
        return job_description

    def clean_job_skills(self):
        job_skills = self.cleaned_data.get("job_skills")
        job_skills = job_skills.strip()
        if job_skills == "":
            raise forms.ValidationError("This field is required.")
        return job_skills

    def clean_job_sub_category(self):
        job_sub_category = self.cleaned_data.get("job_sub_category")
        job_sub_category = job_sub_category.strip()
        if job_sub_category == "":
            raise forms.ValidationError("This field is required.")
        return job_sub_category

    def clean_duration_in_month(self):
        # clean_duration_in_month
        duration_in_month = self.cleaned_data.get("duration_in_month")
        if duration_in_month:
            if duration_in_month<0:
                raise forms.ValidationError("invalid month duration")
        return duration_in_month

    def clean_duration_in_weak(self):
        # clean_duration_in_weak
        duration_in_weak = self.cleaned_data.get("duration_in_weak")
        if duration_in_weak:
            if  duration_in_weak<0:
                raise forms.ValidationError("invalid weak duration")
        return duration_in_weak

    def clean_duration_in_hour(self):
        # clean_duration_in_hour
        duration_in_hour = self.cleaned_data.get("duration_in_hour")
        if duration_in_hour:
            if duration_in_hour<0:
                raise forms.ValidationError("invalid hour duration")
        return duration_in_hour

    def clean_job_main_category(self):
        job_main_category = self.cleaned_data.get("job_main_category")
        if not job_main_category:
            raise forms.ValidationError("This field is required.")
        return job_main_category



    def clean_job_title(self):
        job_title = self.cleaned_data.get("job_title")
        job_title = job_title.strip()
        if job_title == "":
            raise forms.ValidationError("This field is required.")
        return job_title

    def save(self, user, job_main_category, job_sub_category, skills, commit=True):
        form = super(PostJobForm, self).save(commit=False)

        if commit:
            form.user = user
            form.job_main_category = job_main_category
            unique_id = get_random_string(length=15)
            slug = slugify(form.job_title) + "-" + unique_id
            form.slug = slug
            form.save()
            try:
                form.job_skills.add(skills)
                form.job_sub_category.add(job_sub_category)

            except:
                for sub_category in job_sub_category:
                    form.job_sub_category.add(sub_category)
                for skill in skills:
                    form.job_skills.add(skill)
        return form


class ClientProfileForm(ModelForm):
    """
    Form to display the Client Profile basic details
    """

    profile_form_id = forms.CharField(widget=forms.HiddenInput(), initial="profile_form_id")
    first_name = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."))

    last_name = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(ClientProfileForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields['first_name'].widget.attrs['readonly'] = 'readonly'
            self.fields['last_name'].widget.attrs['readonly'] = 'readonly'
            self.fields['email'].widget.attrs['readonly'] = 'readonly'

        for key in self.fields:
            self.fields[key].required = False

    def save(self, commit=True):
        user = super(ClientProfileForm, self).save(commit=False)
        if commit:
            user.save()
        return user


class ClientProfileSecondForm(ModelForm):
    """
    Form to display the more client profile details
    """
    phone_number = forms.RegexField(regex=r'^\+?1?\d{5,15}$',
                                    error_message=(
                                        "Incorrect number format."),required=False)

    class Meta:
        model = ClientProfile
        exclude = ['user', 'company', 'about_company', 'area_of_work', 'birthday', 'website', 'vat_id',
                   'company_address',
                   'company_phone_number', 'country', 'region', 'city', 'job_skills', ]
        fields = ['country', 'region', 'city', 'phone_number', 'profile_pic']

    def __init__(self, *args, **kwargs):
        super(ClientProfileSecondForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = False

    def save(self, country, region, city, commit=True):
        profile = super(ClientProfileSecondForm, self).save(commit=False)
        if commit:
            profile.country = country
            profile.region = region
            profile.city = city
            profile.save()
        return profile


class CompanyDetailsForm(forms.ModelForm):
    """
    Form to display the more client company details
    """
    company = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."), required=False)

    company_address = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."), required=False)

    area_of_work = forms.RegexField(regex=r'[a-zA-Z]',
                                    error_message=(
                                        "Only alphabets is allowed."), required=False)

    company_phone_number = forms.RegexField(regex=r'^\+?1?\d{5,15}$',
                                    error_message=(
                                        "Incorrect number format."), required=True)
    
    class Meta:
        model = ClientProfile
        exclude = ['user', 'country', 'birthday', 'profile_pic', 'address', 'owner_name', 'phone_number', 'vat_id',
                   'about_company', 'job_skills', ]
        fields = ['company', 'company_address', 'company_phone_number', 'website', 'area_of_work']

    def __init__(self, *args, **kwargs):
        super(CompanyDetailsForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = False
            # self.fields['phone_number'].widget.attrs['type'] = 'text'

    def clean_company(self):
        company = self.cleaned_data.get("company")
        company = company.strip()
        if company == "":
            raise forms.ValidationError("This field is required.")
        return company

    def clean_company_address(self):
        company_address = self.cleaned_data.get("company_address")
        company_address = company_address.strip()
        if company_address == "":
            raise forms.ValidationError("This field is required.")
        return company_address

    def clean_company_phone_number(self):
        company_phone_number = self.cleaned_data.get("company_phone_number")
        company_phone_number = company_phone_number.strip()
        if company_phone_number == "":
            raise forms.ValidationError("This field is required.")
        return company_phone_number

    def clean_area_of_work(self):
        area_of_work = self.cleaned_data.get("area_of_work")
        area_of_work = area_of_work.strip()
        if area_of_work == "":
            raise forms.ValidationError("This field is required.")
        return area_of_work

    def save(self, commit=True):
        profile = super(CompanyDetailsForm, self).save(commit=False)
        if commit:
            profile.save()
        return profile


class CompanyDescriptionForm(forms.ModelForm):
    """
    Form to display the description about company
    """
    # about_company = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = ClientProfile
        exclude = ['company', 'company_address', 'company_phone_number', 'website', 'area_of_work', 'user', 'birthday',
                   'profile_pic', 'country', 'address', 'owner_name', 'phone_number', 'vat_id', 'job_skills']
        fields = ['about_company']

    def save(self, commit=True):
        profile = super(CompanyDescriptionForm, self).save(commit=False)
        if commit:
            profile.save()
        return profile


class ResetPasswordForm(forms.ModelForm):
    """
    Form to reset password of client
    """
    password = forms.CharField(widget=forms.PasswordInput, label="Password", required=True)
    password2 = forms.CharField(widget=forms.PasswordInput, label="Confirm Password", required=True)

    class Meta:
        model = User
        fields = ['password']

    def save(self, password, commit=True):
        user = super(ResetPasswordForm, self).save(commit=False)
        if commit:
            user.set_password(password)
            user.save()
        return user


class ApplyJobForm(forms.ModelForm):
    """
    Form to apply jobs
    """

    class Meta:
        model = ApplyJob
        exclude = ['user', 'job']
        fields = ['description', 'resume', 'estimated_cost']

    def __init__(self, *args, **kwargs):
        super(ApplyJobForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['estimated_cost'].widget.attrs['class'] = 'estimated_cost'

    def save(self, user, job, is_resume=None,commit=True,):
        job_form = super(ApplyJobForm, self).save(commit=False)
        if commit:
            job_form.user = user
            if is_resume == 'False':
                job_form.resume=None
            job_form.job = job
            job_form.save()
        return job_form


class ApplyJobCancelForm(forms.ModelForm):
    """
    Form to apply jobs
    """

    class Meta:
        model = ApplyJobCancel
        exclude = ['user', 'job']
        fields = ['cancel_message']

    def save(self, user, job, commit=True):
        job_cancel_form = super(ApplyJobCancelForm, self).save(commit=False)
        if commit:
            job_cancel_form.user = user
            job_cancel_form.job = job
            job_cancel_form.save()
        return job_cancel_form


class UpdateJobWithdrawnForm(forms.ModelForm):
    """
    Form to apply jobs
    """

    class Meta:
        model = ApplyJobCancel
        exclude = ['user', 'job', 'id']
        fields = ['cancel_message']

    def save(self, user, job, job_id=None, commit=True):
        job_cancel_form = super(UpdateJobWithdrawnForm, self).save(commit=False)
        if commit:
            job_cancel_form.id = job_id.id
            job_cancel_form.user = user
            job_cancel_form.job = job
            job_cancel_form.save()
        return job_cancel_form


class ApplyJobUpdateForm(forms.ModelForm):
    """
    Form to apply jobs update
    """

    class Meta:
        model = ApplyJob
        exclude = ['id']
        fields = ['description', 'resume', 'estimated_cost']

    def __init__(self, *args, **kwargs):
        super(ApplyJobUpdateForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['resume'].widget.attrs['class'] = 'file hiden'

    def save(self, commit=True, apply_job=None,is_resume=None):
        job_form = super(ApplyJobUpdateForm, self).save(commit=False)
        if commit:
            job_form.id = apply_job.id
            if is_resume == 'False':
                job_form.resume=None
            job_form.save()
        return job_form


class PostMessege(forms.ModelForm):
    """
    Post messege related jobs
    """

    class Meta:
        model = JobMesseges
        exclude = ['user', 'job', 'messege_to']
        fields = ['messege', 'attachment_file']

    def __init__(self, *args, **kwargs):
        super(PostMessege, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['messege'].widget.attrs['cols'] = '3'
            self.fields['messege'].widget.attrs['rows'] = '2'

    def save(self, user=None, job=None, messege_to=None, commit=True):
        messege_form = super(PostMessege, self).save(commit=False)
        if commit:
            messege_form.user = user
            messege_form.job = job
            messege_form.messege_to = messege_to
            message_id = messege_form.save()
        return messege_form
