from django.conf.urls import url
from django.views.decorators.gzip import gzip_page

from apps.clients.views import PostJobView,  ClientProfileView, FindJobView, FindDetailsView, ClientDetailList,\
    JobDetailView, FreelancerActiveView, MyJobView, MyJobDetails, FetchJobDetailsView,InviteValidation, ChatView
from apps.freelancer.general import check_group_required


urlpatterns = [
    url(r'^jobpost/', gzip_page(PostJobView.as_view()), name='post-job'),
    url(r'^jobs/', gzip_page(FindJobView.as_view()), name='find_job'),
    url(r'^my-jobs/', gzip_page(MyJobView.as_view()), name='my_jobs'),
    url(r'^my-jobsDetails/(?P<slug>[\w-]+)', gzip_page(check_group_required(MyJobDetails.as_view())), name='my_jobsDetails'),
    url(r'^client-detail/', gzip_page(check_group_required(ClientDetailList.as_view())),name='client-detail'),
    url(r'^job-list-detail/(?P<slug>[\w-]+)', gzip_page(FindDetailsView.as_view()),name='find_job_details'),
    url(r'^client-profile/$', ClientProfileView.as_view(), name='client_profile'),
    url(r'^job-details/(?P<slug>[\w-]+)', gzip_page(JobDetailView.as_view()), name='job_details'),
    url(r'^client-freelancer/', FreelancerActiveView.as_view(), name='freelancer_active'),
    url(r'^fetch_job_details/', gzip_page(FetchJobDetailsView.as_view()), name='fetch_job_details'),
    url(r'^is_invited/', InviteValidation.as_view(), name='is_invited'),
    url(r'^message/', ChatView.as_view(), name='chat_view'),


    ]