import os

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.assignment_tag
def get_send_quote_status(sendQuoteStatus):
    try:
        send_quote_status = "False"
        for sendQuote in sendQuoteStatus:

            if sendQuote:
                send_quote_status = "True"
                print send_quote_status
    except:
        send_quote_status = "False"

    return send_quote_status




@register.filter(name='filename')
def filename(value):
    return os.path.basename(value.file.name)