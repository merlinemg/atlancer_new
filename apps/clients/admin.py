from django.contrib import admin
from apps.clients.models import ClientJobs, MyJobs,ApplyJob, JobStatus, JobInvitation,  JobMesseges,\
    ApplyJobCancel

admin.site.register(ClientJobs)

admin.site.register(MyJobs)
admin.site.register(ApplyJob)
admin.site.register(JobStatus)
admin.site.register(JobInvitation)
admin.site.register(JobMesseges)
admin.site.register(ApplyJobCancel)