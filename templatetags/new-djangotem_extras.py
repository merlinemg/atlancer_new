from django import template
from django.template.defaultfilters import linebreaksbr, urlize

register = template.Library()


@register.assignment_tag
def online_user(value):
    c = 0
    for i in value:
        c =c +1
    return c

