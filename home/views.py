import json
import os
import re

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from django.views.decorators.vary import vary_on_cookie
from django.views.generic import TemplateView
from apps.accounts.models import User
from apps.clients.models import ClientJobs
from apps.freelancer.models import HourlyRate
from apps.knowledgebase.models import Skills
from apps.profile.models import FreelancerProfile, ClientProfile

from .models import HomeBanner, UserReview


class IndexView(TemplateView):
        """
        Displaying contents for banner slide and testimonial slide

        """


        def get(self, request):
            if request.user.is_authenticated():
                if request.user.is_superuser:
                    freelancer_list = FreelancerProfile.objects.all()[:4]
                    return render(request, 'index.html', {'images': HomeBanner.objects.all(),
                                                          'user_quotes': UserReview.objects.all(),
                                                          'freelancer_list': freelancer_list,'string':'sssss'})
                else:
                    if request.user.groups.filter(name='Freelancers').exists() and request.user.groups.filter(name='Clients').exists() and request.user.groups.filter(name='Site_admin').exists():
                        freelancer_list = FreelancerProfile.objects.all()[:4]
                        return render(request, 'index.html', {'images': HomeBanner.objects.all(),
                                                              'user_quotes': UserReview.objects.all(),
                                                              'freelancer_list': freelancer_list, 'string': 'sssss'})

                    else:
                        if request.user.active_account == 'CL':
                            redirect_url = '/client-profile/'
                            return redirect(redirect_url)
                        elif request.user.active_account == 'FLR':
                            redirect_url ='/freelancer-profile/'
                            return redirect(redirect_url)
                        else:
                            redirect_url = '/'
                            return redirect(redirect_url)


                        # for group in request.user.groups.all():
                        #     if group.name == "Clients":
                        #         redirect_url = '/client/client-profile/'
                        #         return redirect(redirect_url)
                        #     elif group.name == "Freelancers":
                        #         redirect_url ='/freelancer-profile/'
                        #         return redirect(redirect_url)
                        #     elif group.name == "Site_admin":
                        #         # redirect_url = '/admin/mail/template-list/'
                        #         freelancer_list = FreelancerProfile.objects.all()[:4]
                        #         return render(request, 'index.html', {'images': HomeBanner.objects.all(),
                        #                                               'user_quotes': UserReview.objects.all(),
                        #                                               'freelancer_list': freelancer_list,'string':'sssss'})
                        #     else:
                        #         redirect_url = '/'
                        #         return redirect(redirect_url)

            else:
                freelancer_list = FreelancerProfile.objects.all()[:4]
                return render(request, 'index.html', {'images': HomeBanner.objects.all(),
                                                      'user_quotes': UserReview.objects.all(),
                                                      'freelancer_list': freelancer_list,'string':'sssss'})