# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0011_auto_20151109_0832'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userreview',
            old_name='auther_name',
            new_name='author_name',
        ),
        migrations.RenameField(
            model_name='userreview',
            old_name='discription',
            new_name='description',
        ),
    ]
