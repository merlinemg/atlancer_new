# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_auto_20151107_0928'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userreviews',
            old_name='Name',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='home',
            name='bannerImage',
        ),
        migrations.AddField(
            model_name='home',
            name='banner_image',
            field=models.FileField(default=1, upload_to=b'', verbose_name=b'Banner-Images'),
            preserve_default=False,
        ),
    ]
