# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_auto_20151107_0911'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userreviews',
            name='discribe',
        ),
        migrations.AddField(
            model_name='userreviews',
            name='Name',
            field=models.CharField(default=1, max_length=200, verbose_name=b'Auther'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userreviews',
            name='discription',
            field=models.CharField(default=1, max_length=500, verbose_name=b'Discription'),
            preserve_default=False,
        ),
    ]
