# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20151107_0923'),
    ]

    operations = [
        migrations.AddField(
            model_name='userreviews',
            name='images',
            field=models.FileField(default=1, upload_to=b'', verbose_name=b'Banner-Images'),
            preserve_default=False,
        ),
    ]
