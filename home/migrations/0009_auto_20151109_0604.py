# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0008_auto_20151109_0553'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userreviews',
            old_name='name',
            new_name='auther_name',
        ),
    ]
