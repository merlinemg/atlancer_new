# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_home_bannerimage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userreviews',
            old_name='discription',
            new_name='discribe',
        ),
    ]
