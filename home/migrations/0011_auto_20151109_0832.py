# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_auto_20151109_0830'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auther_name', models.CharField(max_length=200, verbose_name=b'Auther')),
                ('images', models.FileField(upload_to=b'', verbose_name=b'Auther-Image')),
                ('discription', models.CharField(max_length=500, verbose_name=b'Discription')),
            ],
        ),
        migrations.RenameModel(
            old_name='HomeBanners',
            new_name='HomeBanner',
        ),
        migrations.DeleteModel(
            name='UserReviews',
        ),
    ]
