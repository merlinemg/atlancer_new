# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_userreviews_images'),
    ]

    operations = [
        migrations.AlterField(
            model_name='home',
            name='bannerImage',
            field=models.FileField(upload_to=b'/static/images/', verbose_name=b'Banner-Images'),
        ),
    ]
