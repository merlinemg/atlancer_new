# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0009_auto_20151109_0604'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomeBanners',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name=b'Banner Title')),
                ('banner_image', models.FileField(upload_to=b'', verbose_name=b'Banner-Images')),
            ],
        ),
        migrations.DeleteModel(
            name='Home',
        ),
        migrations.AlterField(
            model_name='userreviews',
            name='images',
            field=models.FileField(upload_to=b'', verbose_name=b'Auther-Image'),
        ),
    ]
