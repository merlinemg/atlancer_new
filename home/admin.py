from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import HomeBanner,UserReview

admin.site.register(HomeBanner),
admin.site.register(UserReview)