from django.db import models

# Create your models here.
from django.db import models


class HomeBanner(models.Model):
    """
    Custom  model for Banners
    title holds banner title
    banner_image holds the images for slides

    """
    title = models.CharField(max_length=100, verbose_name="Banner Title")
    banner_image = models.FileField(verbose_name="Banner-Images")

    def __str__(self):
        return self.title


class UserReview(models.Model):
    """
    Custom  model for User reviews in slides
    auther_name holds name of author of quotes
    images holds the images of auther
    description holds the reviews of author

    """

    author_name = models.CharField(max_length=200, verbose_name="Auther")
    images = models.FileField(verbose_name="Auther-Image")
    description = models.CharField(max_length=500, verbose_name="Discription")

    def _get_FIELD_display(self, field):
        return self.images

    def __str__(self):
        return self.author_name
